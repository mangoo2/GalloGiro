<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
    }
	public function index(){
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Proveedores/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloProveedor->filas();//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 20; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["preveedor"] = $this->ModeloProveedor->total_paginados($pagex,$config['per_page']);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('proveedores/proveedores',$data);
            $this->load->view('templates/footer');
            $this->load->view('proveedores/jsproveedores');
	}
    public function proveedoradd(){
        $data["Estados"] = $this->ModeloProveedor->estados();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('proveedores/proveedoradd',$data);
        $this->load->view('templates/footer');
        $this->load->view('proveedores/jsproveedores');
    }
    function preoveedoraddd(){
        $id = $this->input->post('id');
        $razonSocial = $this->input->post('razonSocial');
        $domicilio = $this->input->post('domicilio');
        $ciudad = $this->input->post('ciudad');
        $cp = $this->input->post('cp');
        $estado = $this->input->post('estado');
        $contacto = $this->input->post('contacto');
        $email = $this->input->post('email');
        $rfc = $this->input->post('rfc');
        $telefonoLocal = $this->input->post('telefonoLocal');
        $telefonoCelular = $this->input->post('telefonoCelular');
        $fax = $this->input->post('fax');
        $obser = $this->input->post('obser');

        // Variables para lectura de código de barras
        // Puede tener hasta tres variables 
        $pesoi = $this->input->post('pesoi');
        $pesof = $this->input->post('pesof');
        $codi = $this->input->post('codi');
        $codf = $this->input->post('codf');
        $longitud_codigo1 = $this->input->post('longitud_codigo1');


        $pesoi_2 = $this->input->post('pesoi_2');
        $pesof_2 = $this->input->post('pesof_2');
        $codi_2 = $this->input->post('codi_2');
        $codf_2 = $this->input->post('codf_2');
        $longitud_codigo2 = $this->input->post('longitud_codigo2');

        $pesoi_3 = $this->input->post('pesoi_3');
        $pesof_3 = $this->input->post('pesof_3');
        $codi_3 = $this->input->post('codi_3');
        $codf_3 = $this->input->post('codf_3');
        $longitud_codigo3 = $this->input->post('longitud_codigo3');

        if ($id>0) {
            $this->ModeloProveedor->proveedorupdate($id,$razonSocial,$domicilio,$ciudad,$cp,$estado,$contacto,$email,$rfc,$telefonoLocal,$telefonoCelular,$fax,$obser,$pesoi,$pesof,$codi,$codf,$longitud_codigo1,$longitud_codigo2,$longitud_codigo3,$pesoi_2,$pesof_2,$codi_2,$codf_2,$pesoi_3,$pesof_3,$codi_3,$codf_3);
        }else{
            $this->ModeloProveedor->proveedorinsert($razonSocial,$domicilio,$ciudad,$cp,$estado,$contacto,$email,$rfc,$telefonoLocal,$telefonoCelular,$fax,$obser,$pesoi,$pesof,$codi,$codf,$longitud_codigo1,$longitud_codigo2,$longitud_codigo3,$pesoi_2,$pesof_2,$codi_2,$codf_2,$pesoi_3,$pesof_3,$codi_3,$codf_3);
        }
        redirect('/Proveedores');
    }
    
    public function deleteproveedor(){
        $id = $this->input->post('id');
        $this->ModeloProveedor->deleteproveedors($id); 
    }
    function buscarprov(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloProveedor->proveedorallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trcli_<?php echo $item->id_proveedor; ?>">
                <td><?php echo $item->razon_social; ?></td>
                <td><?php echo $item->domicilio; ?></td>
                <td><?php echo $item->ciudad; ?></td>
    
                <td><?php echo $item->telefono_local; ?></td>
                <td><?php echo $item->telefono_celular; ?></td>
                <td><?php echo $item->email_contacto; ?></td>
                <td><?php echo $item->rfc; ?></td>
                <td>
                    <div class="btn-group mr-1 mb-1">
                        <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                          <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="sr-only">Toggle Dropdown</span>
                          </button>
                          <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                              <a class="dropdown-item" href="<?php echo base_url();?>Proveedores/proveedoradd?id=<?php echo $item->id_proveedor; ?>">Editar</a>
                              <a class="dropdown-item" onclick="proveedordelete(<?php echo $item->id_proveedor; ?>);"href="#">Eliminar</a>
                          </div>
                    </div>
                </td>
            </tr>
        <?php }
    }
    
    

       
    
}
