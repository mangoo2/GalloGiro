<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marcas extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Modelomarcas');
    }

	public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('marcas/marcas',$data);
        $this->load->view('templates/footer');
        $this->load->view('marcas/jsmarcas');
	}
       
    public function marcasadd()
    {
        
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('marcas/marcasadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('marcas/jsmarcas');
    }
       
    public function getListadoMarcas()
    {
        $marcas = $this->Modelomarcas->getListadoMarcas();
        $json_data = array("data" => $marcas);
        echo json_encode($json_data);
    }

    public function insertUpdateMarca()
    {
        $data = $this->input->post();
        
        // Si MARCAID es 0, entonces es nueva marca
        if($data["marcaid"]==0)
        {
            $data['activo'] = 1;
            unset($data["marcaid"]);
            {
                $idInsertado = $this->Modelomarcas->insertMarca($data);
                if($idInsertado!=0)
                {
                    $imagenInsertada = $this->imgcat($idInsertado,$_FILES);
                    if($imagenInsertada!=0)
                    {
                       $return = 1;                         
                    } 
                    else
                    {
                        $return = 0;
                    }
                    echo $return;
                }       
            }
        }
        else
        {
            $idMarca = $data["marcaid"];
            
            unset($data["marcaid"]);
            unset($data["imagenid"]);

            $marcaActualizada = $this->Modelomarcas->updateMarca($data,$idMarca);
            if($marcaActualizada == true)
            {
                if($_FILES["img"]["name"]!='')
                {
                    $imagenInsertada = $this->imgcat($idMarca,$_FILES);
                }
               $return = 1;                         
            } 
            else
            {
                $return = 0;
            }
            echo $return;
            
        }
    }

    public function imgcat($idCat, $FILES)
    {
        $target_path = 'public/img/marcas';
        $thumb_path = 'public/img/marcas';
        //file name setup
        $filename_err = explode(".",$FILES['img']['name']);
        $filename_err_count = count($filename_err);
        $file_ext = $filename_err[$filename_err_count-1];
        
        $fileName = $FILES['img']['name'];
        
        $fecha=date('ymd-His');
        //upload image path
        $cargaimagen =$fecha.basename($fileName);
        $imagenarray = array('imgm' => $cargaimagen);
        $upload_image = $target_path.'/'.$cargaimagen;
        
        //upload image
        if(move_uploaded_file($FILES['img']['tmp_name'],$upload_image))
        {
            //thumbnail creation
                $marcainsertadaimagen = $this->Modelomarcas->updateMarca($imagenarray,$idCat);
                
                $thumbnail = $thumb_path.'/'.$cargaimagen;
                list($width,$height) = getimagesize($upload_image);
                if ($width>3000) {
                    $thumb_width = $width/10;
                    $thumb_height = $height/10;
                }elseif ($width>2500) {
                    $thumb_width = $width/7.9;
                    $thumb_height = $height/7.9;
                }elseif ($width>2000) {
                    $thumb_width = $width/6.8;
                    $thumb_height = $height/6.8;
                }elseif ($width>1500) {
                    $thumb_width = $width/5.1;
                    $thumb_height = $height/5.1;
                }elseif ($width>1000) {
                    $thumb_width = $width/3.3;
                    $thumb_height = $height/3.3;
                }elseif ($width>900) {
                    $thumb_width = $width/3;
                    $thumb_height = $height/3;
                }elseif ($width>800) {
                    $thumb_width = $width/2.6;
                    $thumb_height = $height/2.6;
                }elseif ($width>700) {
                    $thumb_width = $width/2.3;
                    $thumb_height = $height/2.3;
                }elseif ($width>600) {
                    $thumb_width = $width/2;
                    $thumb_height = $height/2;
                }elseif ($width>500) {
                    $thumb_width = $width/1.9;
                    $thumb_height = $height/1.9;
                }elseif ($width>400) {
                    $thumb_width = $width/1.3;
                    $thumb_height = $height/1.3;
                }else{
                    $thumb_width = $width;
                    $thumb_height = $height;
                }
                $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                switch($file_ext){
                    case 'jpg':
                        $source = imagecreatefromjpeg($upload_image);
                        break;
                    case 'jpeg':
                        $source = imagecreatefromjpeg($upload_image);
                        break;

                    case 'png':
                        $source = imagecreatefrompng($upload_image);
                        break;
                    case 'gif':
                        $source = imagecreatefromgif($upload_image);
                        break;
                    default:
                        $source = imagecreatefromjpeg($upload_image);
                }

                imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                switch($file_ext){
                    case 'jpg' || 'jpeg':
                        imagejpeg($thumb_create,$thumbnail,100);
                        break;
                    case 'png':
                        imagepng($thumb_create,$thumbnail,100);
                        break;

                    case 'gif':
                        imagegif($thumb_create,$thumbnail,100);
                        break;
                    default:
                        imagejpeg($thumb_create,$thumbnail,100);
                }

            

            //$return = Array('ok'=>TRUE,'img'=>'');
            $regreso = 1;
        }
        else
        {
            //$return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
            $regreso = 0;
        }
        //echo json_encode($return);
        return $regreso;
    }
    
}
