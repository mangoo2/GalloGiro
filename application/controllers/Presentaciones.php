
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presentaciones extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		/*$logueo = $this->session->userdata('logeado');
		error_reporting(0);
        if($logueo!=true){
            redirect(base_url(), 'refresh');
        }*/
		$this->load->model('ModeloCatalogos');
	}

	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('presentaciones/lpresentaciones');
        $this->load->view('templates/footer');
        $this->load->view('presentaciones/jslpresentaciones');
	}

	public function Alta(){
		//$data['presentaciones']=$this->ModeloCatalogos->get_record($id,"presentaciones");
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('presentaciones/alta');
        $this->load->view('templates/footer');
        $this->load->view('presentaciones/jslpresentaciones');
	}

	public function Edit($id){
		$data['presentaciones']=$this->ModeloCatalogos->get_record($id,"presentaciones");
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('presentaciones/alta',$data);
        $this->load->view('templates/footer');
        $this->load->view('presentaciones/jslpresentaciones');
	}

	public function submit(){
		$data=$this->input->post();
		if(!isset($data['id'])){ //insert
			$id=$this->ModeloCatalogos->GenInsert($data,'presentaciones');
		}
		else{ //update
			$id=$data["id"]; unset($data["id"]);
			$this->ModeloCatalogos->GenUpdate($data,'presentaciones','presentacionId',$id);
		}
	}

	public function get_records(){
		$data=$this->ModeloCatalogos->GetAllActive('presentaciones');
		$json_data = array("data" => $data); 
   		echo json_encode($json_data);
	}

	public function delete(){
		$id=$this->input->post("id");
		$this->ModeloCatalogos->GenUpdate(array('activo' => 0,),'presentaciones','presentacionId',$id);
	}

}

/* End of file Proveedores.php 
*/
/* Location: ./application/controllers/Proveedores.php */