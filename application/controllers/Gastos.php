<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gastos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloGasto');
        $this->load->model('ModeloCatalogos');
    }

    public function index(){

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('gastos/gastosadd');
        $this->load->view('templates/footer');
        $this->load->view('gastos/jsgastos');
    }
       
    public function insertarGasto()
    {
        $datos = $this->input->post();

        date_default_timezone_set('America/Mexico_City');
        $date = date('Y-m-d');
        $datos['fecha_reg']=$date;
        $datos['id_usuario']=$_SESSION['idpersonal_tz'];
        
        $this->ModeloGasto->insertarGasto($datos);
    }

    public function delete(){
        $id=$this->input->post("id");
        $this->ModeloCatalogos->GenUpdate(array('status' => 0,),'gastos','id',$id);
    }

    public function datatable_gastos(){
        $params=$this->input->post();
        $gastos = $this->ModeloGasto->get_gastos($params);
        $totalRecords=$this->ModeloGasto->get_no_gastos($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $gastos->result(),
            "query"           => $this->db->last_query()   
            );
        echo json_encode($json_data);
    }
}
