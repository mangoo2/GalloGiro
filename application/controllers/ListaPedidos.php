
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaPedidos extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		/*$logueo = $this->session->userdata('logeado');
		error_reporting(0);
        if($logueo!=true){
            redirect(base_url(), 'refresh');
        }*/
		$this->load->model('ModeloCatalogos');
		$this->load->model('ModeloVentas');
	}

	public function index(){
        /*if (isset($_GET['bod'])) {
            $bodega=$_GET['bod'];
        }else{
            $bodega=0;
        }*/
        $tipo="";
        if(isset($_SESSION['bodega_tz'])){
          $bodega = $_SESSION['bodega_tz'];
        }
        else{
          $bodega=0;
        }
        if(isset($_GET['bod'])) {
          $bodega=$_GET['bod'];
        }
        if (isset($_GET['tipo'])) {
            $tipo=$_GET['tipo'];
        }
        $pages=10; //Número de registros mostrados por páginas
        $this->load->library('pagination'); //Cargamos la librería de paginación
        $config['base_url'] = base_url().'ListaPedidos/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
        $config['total_rows'] = $this->ModeloVentas->filasPedido($bodega,$tipo);//calcula el número de filas
        $config['per_page'] = $pages; //Número de registros mostrados por páginas  
        $config['num_links'] = 3; //Número de links mostrados en la paginación
        $config['first_link'] = 'Primera';//primer link
        $config['last_link'] = 'Última';//último link
        $config["uri_segment"] = 3;//el segmento de la paginación
        $config['next_link'] = 'Siguiente';//siguiente link
        $config['prev_link'] = 'Anterior';//anterior link
        $config['reuse_query_string'] = TRUE;// para mantener los parametros del get
        $this->pagination->initialize($config); //inicializamos la paginación 
        $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["ventas"] = $this->ModeloVentas->total_paginadosPedido($pagex,$config['per_page'],$bodega,$tipo);
        $data["personal"] = $this->ModeloCatalogos->getTable('personal','estatus');

        //$data["edo_pedido"] = $this->ModeloCatalogos->getEdoPedido();
        $data["edo_pedido"] = $this->ModeloCatalogos->GetAllActiveEstatus('personal');

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        //$this->load->view('Personal/Personal',$data);
        $this->load->view('pedidos/listapedidos',$data);
        $this->load->view('templates/footer');
    }
    function cancelarPedido(){
      $id = $this->input->post('id');
      $this->ModeloVentas->cancalarventa($id);

      $resultado=$this->ModeloVentas->ventadetalles($id);
      foreach ($resultado->result() as $item){
        $this->ModeloVentas->regresarpro($item->id_producto,$item->cantidad);
      }
        
    }
    function buscarvent(){
        $buscar = $this->input->post('buscar');
        $bode = $this->input->post('bode');
        $resultado=$this->ModeloVentas->pedidosSearch($buscar,$bode);
        $edo_pedido=$this->ModeloCatalogos->GetAllActiveEstatus('personal');
        foreach ($resultado->result() as $item){ ?>
            <tr id="trven_<?php echo $item->id_venta; ?>">
                  <td><?php echo $item->id_venta; ?></td>
                  <td><?php echo $item->_refecha_pedido; ?></td>
                  <!--<td><?php echo $item->fecha_entrega; ?></td>-->
                  <td><?php echo $item->direc; ?></td>
                  <td>$ <?php echo number_format($item->monto_total,2,'.',',') ; ?></td>
                  <td><?php echo $item->metodo; ?></td>
                  <td><?php echo $item->cliente; ?></td>
                  <td><?php if($item->cancelado==1){ echo '<span class="badge badge-danger">Cancelado</span>';} ?></td>
                  <td>
                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                      <i class="fa fa-book"></i>
                    </button>
                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo $item->monto_total; ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                      <i class="fa fa-times"></i>
                    </button>
                    <button class="btn btn-success success-flickr white" onclick="detalles(<?php echo $item->id_venta; ?>)" title="detalles" data-placement="top" data-toggle="tooltip" data-target="#detalles_reparto">
                      <i class="fa fa-eye"></i>
                    </button>
                    <button class="btn btn-success success-flickr white" onclick="pagarParcial(<?php echo $item->id_venta; ?>)" title="Abonar Pago" data-placement="top" data-toggle="tooltip" data-target="#abonar_pago" <?php if($item->cancelado==1 || $item->tipo_metodo!=4){ echo 'disabled';} ?>>
                      <i class="fa fa-usd"></i>
                    </button>
                    <div>
                      <label onclick="entregado(<?php echo $item->id_venta; ?>)">
                        Entregado: 
                        <input type="checkbox" value="1" id="entregado" class="entregado_<?php echo $item->id_venta; ?>" <?php if($item->id_ventaPD == $item->id_venta && $item->entregado==1) echo 'checked'; if($item->cancelado==1)echo 'disabled'; ?>>
                      </label>
                     
                    </div>
                    <div class="">
                      <label >Pagado:</label>
                      <input type="checkbox" value="1" id="pagado_<?php echo $item->id_venta; ?>" onclick="confirmacionPago('<?php echo $item->id_venta; ?>')" <?php if($item->id_venta == $item->id_ventaPD && $item->pagado==1) echo 'checked'; if($item->cancelado==1) echo 'disabled'; ?>>
                    </div>
                    <div class="col-md-8">
                      <select class="form-control" id="repartidorasirnar" <?php if($item->cancelado==1){ echo 'disabled';} ?> onchange="asignarRepartidor(<?php echo $item->id_venta; ?>)">
                        <option value=""></option>
                        <?php 
                        foreach ($edo_pedido as $x){ //echo "item: " .$x->id_venta;
                          $select="";
                          if(isset($item->id_repartidor) && $x->personalId==$item->id_repartidor){ $select="selected"; }
                          //if($x->id_venta == $item->id_venta){ //echo "son iguales"; 

                            echo "<option value='$x->personalId'$select>$x->nombre $x->apellidos</option>";
                            
                          }
                          /*foreach ($personal as $item) { ?>
                             <option value="<?php echo $item->personalId;?>"><?php echo $item->nombre.' '.$item->apellidos;?></option>
                          <?php }*/
                          ?>
                          
                        </select>
                      </div>
                  </td>
          </tr>
        <?php }
    }

    public function detalles_pedido(){
        $params = $this->input->post();
        $records = $this->ModeloVentas->get_pedido_det($params);
        $json_data = array("data" => $records);
        echo json_encode($records);
    }

    public function asignarRepartidor(){
        $params = $this->input->post();
        $records = $this->ModeloCatalogos->edit_record($params['venta'],array("id_repartidor"=>$params['repartidor']),"pedido_detalle");
    }

    public function pagar(){
        $id=$this->input->post("id");
        $pago=$this->input->post("pago");
        $this->ModeloCatalogos->GenUpdate(array('pagado' => $pago),'pedido_detalle','id_venta',$id);
    }
    public function entregado(){
        $id=$this->input->post("id");
        $entrega=$this->input->post("entrega");
        if($entrega==1)
            $this->ModeloCatalogos->GenUpdate(array('entregado' => $entrega, "fecha_entrega"=>date('Y-m-d H:i:s')),'pedido_detalle','id_venta',$id);
        else
            $this->ModeloCatalogos->GenUpdate(array('entregado' => $entrega, "fecha_entrega"=>''),'pedido_detalle','id_venta',$id);
    }

    public function consultaDetallePedido($id)
    {
      $data=$this->ModeloVentas->detallesVentas($id);
      echo "<table id='tabla_temp_detventa' class='table table-bordered table-striped table-hover js-basic-example dataTable'>
               <thead>
                 <tr>
                 <th class=''>Cantidad</th>
                 <th class=''>Producto</th>
                 <th class=''>Precio</th>
                 <th class=''>Cancelar</th>
                 </tr>
               </thead>
               <tbody>";
      foreach ($data->result() as $row){
          echo "<tr>";
          echo "<td class=''>".$row->cantidad." kg</td>";
          echo "<td class=''>".$row->categoria.' '.$row->marca.' - '.$row->presentacion."</td>";
          echo "<td class=''>$ ".number_format($row->cantidad*$row->precio,2,'.',',')."</td>";
          if($row->status_vd==1){
              echo "<td class=''><button class='btn btn-raised gradient-flickr white sidebar-shadow' onclick='cancelaParcial($row->id_detalle_venta)'> Cancelar</button></td>";
          }
          else{
             echo "<td class='btn btn-raised gradient-flickr white sidebar-shadow'>Cancelado</td>";  
          }
          echo "</tr>";
      }
      echo "</tbody>
          </table>";
    }

    public function asignar_pago(){
        $id=$this->input->post('idPed');
        $pagado=$this->ModeloVentas->get_pagosIDV($id); 
        
        $results=$this->ModeloVentas->detallesVentasPorID($id); 
        foreach ($results as $fila) { 
            echo "<p>Venta: ".$fila->id_venta."</p>";
            echo "<p>Fecha: ".$fila->reg."</p>";
            echo "<p>Cliente: ".$fila->cliente."</p>";
            echo "<p>Total de venta: $".number_format($fila->total_vd,2,'.',',')."</p>";
            echo "<p>Pagado: $".number_format($pagado->totalPagado,2,'.',',')."</p>"; 
            echo "<p>Saldo:  $".number_format($fila->total_vd-$pagado->totalPagado,2,'.',',')."</p>"; 
        }

    }

    public function submitPago($id_venta){
        $data = $this->input->post('data');
        //$id_venta = $this->input->post('id_venta');
        $DATA = json_decode($data);
        $total_monto=0;
        $val1=0;
        log_message('error', 'id_venta: '.$id_venta);
        $pagos=$this->ModeloVentas->getselectwheren('ventas_credito_pago',array('id_venta'=>$id_venta));
        $results=$this->ModeloVentas->detallesVentasPorID($id_venta); 
        foreach ($results as $fila) { 
            $monto_pedido=round($fila->monto_total,2);
        }
        //log_message('error', 'monto_pedido: '.$monto_pedido);
        //$data['id_user_reg'] = $this->session->userdata('usuarioid_tz');
        //$id_user_reg = $this->session->userdata('usuarioid_tz');
        $id_user_reg =$_SESSION['usuarioid_tz'];
        for ($i=0; $i<count($DATA); $i++) {
            if($DATA[$i]->id==0 && $DATA[$i]->cantidad!=""){
                $total_monto = $total_monto + $DATA[$i]->cantidad;
                //log_message('error', 'insert: '.$total_monto);
                if($DATA[$i]->id==0){
                    $this->ModeloCatalogos->insertToCatalogop(array('id_venta'=>$id_venta,'fecha_pago'=>$DATA[$i]->fecha_pago,'folio_pago'=>$DATA[$i]->folio_pago,'cantidad'=>$DATA[$i]->cantidad,'observaciones'=>$DATA[$i]->observaciones,"id_user_reg"=>$id_user_reg,"fecha_reg"=>date("Y-m-d H:i:s")),'ventas_credito_pago'); 
                }           
            }
            if($DATA[$i]->id>0){
                $total_monto = $total_monto + $DATA[$i]->cantidad;
                //log_message('error', 'update id: '.$DATA[$i]->id);
                $this->ModeloCatalogos->updateCatalogon(array('id_venta'=>$id_venta,'fecha_pago'=>$DATA[$i]->fecha_pago,'folio_pago'=>$DATA[$i]->folio_pago,'cantidad'=>$DATA[$i]->cantidad,'observaciones'=>$DATA[$i]->observaciones,"id_user_reg"=>$id_user_reg,"fecha_update"=>date("Y-m-d H:i:s")),array('id_venta'=>$DATA[$i]->id),'ventas_credito_pago');              
            }
            //log_message('error', 'total_monto: '.$total_monto);
            //log_message('error', 'para comprar y cambiar estatus a ventas. total_monto: '.$total_monto);
            //log_message('error', 'monto_pedido: '.$monto_pedido);
            if($total_monto==$monto_pedido){
              //$this->ModeloCatalogos->updateCatalogon(array('pagado'=>1),array('id_venta'=>$id_venta),'ventas');
              $this->ModeloCatalogos->GenUpdate(array('pagado' => $pago),'pedido_detalle','id_venta',$id_venta);
            }
        } 

    }

    public function verificar_pago(){
        $id=$this->input->post('idPed');
        $pago = $this->ModeloVentas->verificaPagos($id);

        $html="";
        $html.='<hr><h1>Pagos realizados</h1>';
        foreach ($pago->result() as $k) {
            $html.='<hr>
            <form id="asociar_pago">
            <div class="cont_hijoExist">
                <input type="hidden" class="form-control" name="id" id="id_pago" value="'.$k->id.'">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="label-control">Fecha: </label>
                        <input type="date" class="form-control" name="fecha_pago" id="fecha_pago" value="'.$k->fecha_pago.'">
                    </div>
                </div>
                <div class="">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="label-control">Folio de Pago: </label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="folio_pago" id="folio_pago" value="'.$k->folio_pago.'">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="label-control">Monto: </label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="cantidad" id="cantidad" value="'.$k->cantidad.'">
                            </div>
                        </div>
                    </div>
                    
                </div> 
                <div class="">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class=" label-control">Observaciones: </label>
                            <div class="col-md-12">
                                <textarea id="observaciones" name="observaciones" class="form-control" value="'.$k->observaciones.'">'.$k->observaciones.'</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label style="color: transparent;">agregar pago</label>
                            <button type="button" onclick="remove_pagoE($(this),'.$k->id.','.$id.')" class="btn btn-danger"><i class="fa fa-minus"></i> Pago</button>
                        </div><hr>
                    </div>
                </div> 
            </div>
        </form>';
        }
        
        echo $html;
        
    }

    public function eliminarPago(){
      $id = $this->input->post('id');
      $id_venta = $this->input->post('id_venta');
      $this->ModeloCatalogos->updateCatalogon(array('estatus'=>0),array('id'=>$id),'ventas_credito_pago');
      $this->ModeloCatalogos->updateCatalogon(array('pagado'=>0),array('id_venta'=>$id_venta),'ventas');
    }

    public function exportarPedidos($tipo,$sucur){
      $data['info']=$this->ModeloVentas->dataExportPedido($sucur,$tipo);
      $this->load->view('ventas/ventasExcel',$data);
    }
}

/* End of file Proveedores.php 
*/
/* Location: ./application/controllers/Proveedores.php */