<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mercados extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
    }

    public function index(){

        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mercados/listado');
        $this->load->view('templates/footer');
        $this->load->view('mercados/jslistado');
    }
    
    public function mercadosadd($id=0)
    {
        $data["m"]=$this->ModeloCatalogos->get_tableRow("mercados",array("id"=>$id));
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('mercados/mercadosadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('mercados/jsmercados');
    }

    function registrar(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        if ($id>0) {
            $this->ModeloCatalogos->updateCatalogon($data,array('id'=>$id),'mercados');
        }else{
            $id=$this->ModeloCatalogos->GenInsert($data,"mercados");
        }
        echo $id;
    }

    public function delete(){
        $id=$this->input->post("id");
        $this->ModeloCatalogos->GenUpdate(array('status' => 0,),'mercados','id',$id);
    }

    public function datatable_records(){
        $datas = $this->ModeloCatalogos->get_tableWhere("mercados",array("status"=>1));
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }

    function searchMercado(){
        $merc = $this->input->get('search');
        $results=$this->ModeloCatalogos->search_mercado($merc);
        echo json_encode($results->result());
    }
}