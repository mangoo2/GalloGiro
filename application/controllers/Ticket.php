<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Ticket extends CI_Controller {
	function __construct(){
        parent::__construct();
        //$this->load->model('Solicitud');
        $this->load->model('ModeloVentas');
    }
	public function index(){
		$id=$this->input->get('id');
		$tipo=$this->input->get('tipo');
		//log_message('error', 'tipo: '.$tipo);

		if (isset($_GET['vcambio'])) {
			$data['vcambio']=$_GET['vcambio'];
			$data['vpagacon']=$_GET['vpagacon'];
		}else{
			$data['vcambio']='';
			$data['vpagacon']='';
		}

		/*$data['configticket']=$this->ModeloVentas->configticket();
		$data['getventas']=$this->ModeloVentas->getventas($id);
		$data['getventasd']=$this->ModeloVentas->getventasd($id);*/
		$configticket=$this->ModeloVentas->configticket();
		$getventas=$this->ModeloVentas->getventas($id);
		$data['getventasd']=$this->ModeloVentas->getventasd($id);

		foreach ($configticket->result() as $item){
		    if($_SESSION['bodega_tz'] == $item->sucursal){
		      $data['titulo'] = $item->titulo;
		      $data['mensaje'] = $item->mensaje;
		      $data['mensaje2'] = $item->mensaje2;
		      $data["fuente"] = $item->fuente;
		      $data['tamano'] = $item->tamano;
		      $data['margensup'] = $item->margensup;
		    }
	  	}
		foreach ($getventas->result() as $item){
		    $data['idticket']= $item->id_venta;
		    $id_personal = $item->id_personal;
		    $id_cliente = $item->id_cliente;
		    $data['monto_total'] = $item->monto_total;
		    $data['descuento'] = $item->descuento;
		    $data['cliente'] = $item->Nom;
		    $data['cancelado'] = $item->cancelado;
		    $metodo = $item->metodo;
		    $data["reg"] = $item->reg;
		}
	  	if($metodo==1){
		    $data['metodo'] = "Efectivo";
		}else if($metodo==2){
		    $data['metodo'] = "T. Crédito";
		}else if($metodo==3){
		    $data['metodo'] = "T. Débito";
		}else if($metodo==4){
		    $data['metodo'] = "Crédito";
		}
	  
		if($tipo==2){
			$getDetallePed=$this->ModeloVentas->getvdirecventa($id);
		    //$data['fecha_ped']=$this->ModeloVentas->getvdirecventa($id);
		}
		else{
			$getDetallePed=0;
		}

		$data["tipo"] = $tipo;
		$data['direccion'] = "";
	  	$data['fecha_ped'] = "";
	  	$data['tipo'] = $tipo;
	  	if($tipo==2){
	    	foreach ($getDetallePed->result()as $value) {
	      		$data['direccion'] = $value->direc;
	      		$data['fecha_ped'] = $value->fecha_pedido;
	    	}
	  	}
		$this->load->view('Reportes/ticket',$data);
	        
	}
}