<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('ModeloClientes');
		$this->load->model('ModeloDomicilio_Cliente');


	}

	public function index()
	{
		$pages = 10; //Número de registros mostrados por páginas
		$this->load->library('pagination'); //Cargamos la librería de paginación
		$config['base_url'] = base_url() . 'Clientes/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
		$config['total_rows'] = $this->ModeloClientes->filas();//calcula el número de filas
		$config['per_page'] = $pages; //Número de registros mostrados por páginas
		$config['num_links'] = 3; //Número de links mostrados en la paginación
		$config['first_link'] = 'Primera';//primer link
		$config['last_link'] = 'Última';//último link
		$config["uri_segment"] = 3;//el segmento de la paginación
		$config['next_link'] = 'Siguiente';//siguiente link
		$config['prev_link'] = 'Anterior';//anterior link
		$config['reuse_query_string'] = TRUE;// para mantener los parametros del get
		$this->pagination->initialize($config); //inicializamos la paginación
		$pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data["clientes"] = $this->ModeloClientes->total_paginados($pagex, $config['per_page']);

		$this->load->view('templates/header');
		$this->load->view('templates/navbar');
		//$this->load->view('Personal/Personal',$data);
		$this->load->view('clientes/clientes', $data);
		$this->load->view('templates/footer');
		$this->load->view('clientes/jsclientes');
	}

	public function clientesadd()
	{
		$this->load->view('templates/header');
		$this->load->view('templates/navbar');
		$this->load->view('clientes/clientesadd');
		$this->load->view('templates/footer');
		$this->load->view('clientes/jsclientes');
	}

	public function clientesUpdate($idCliente = 0)
	{
		$dataClientes = $this->ModeloClientes->get_cliente($idCliente);
		$dataDomicilios = $this->ModeloDomicilio_Cliente->get_by_idCliente_activo($idCliente);
		$data = array(
			"cliente" => $dataClientes,
			"domicilios" => $dataDomicilios
		);
		$this->load->view('templates/header');
		$this->load->view('templates/navbar');
		$this->load->view('clientes/clientesUpdate', $data);
		$this->load->view('templates/footer');
	}

	public function clientesEdita(){
		//$listaDomicilios = $this->input->post("listaDirecciones");

		$listDomiciliosBase = $this->input->post("listDomiciliosBase");
		$ClientesId = $this->input->post("ClientesId");
		$dataUpdate = $this->input->post();
		unset($dataUpdate['listaDirecciones']);
		unset($dataUpdate['listDomiciliosBase']);
		unset($dataUpdate['ClientesId']);
		$df = $this->input->post("df");

		if($df=="true"){
			$dataUpdate['df']=1;
		}else{
			$dataUpdate['df']=0;
		}

		//log_message('error', 'clientesEdita: '.$ClientesId);
		$this->ModeloClientes->clientesupdate($ClientesId, $dataUpdate);

		/*foreach ($listaDomicilios as $domicilios) {
			$domicilios['idCliente'] = $ClientesId;
			$this->ModeloDomicilio_Cliente->insert($domicilios);
		}*/

		/*foreach ($listDomiciliosBase as $domiciliosBase) {
			$idDomicilio = $domiciliosBase['idDomicilio'];
			unset($domiciliosBase['idDomicilio']);
			$this->ModeloDomicilio_Cliente->update_by_idDomicilio($idDomicilio, $domiciliosBase);
		}*/
		//redirect('/Clientes');
	}

	public function eliminar_dom()
	{
		$idDomicilio = $this->input->post("idDomicilio");
		$data=array("status"=>0);
		$this->ModeloDomicilio_Cliente->update_by_idDomicilio($idDomicilio, $data);
		echo 1;
	}

	function clienteadd()
	{
		$listaDomicilios = $this->input->post("listaDirecciones");
		$df = $this->input->post("df");
		$dataInsert = $this->input->post();

		if($df=="true"){
			$dataInsert['df']=1;
		}else{
			$dataInsert['df']=0;
		}

		unset($dataInsert['listaDirecciones']);

		$idCliente = $this->ModeloClientes->clientesinsert($dataInsert);
		/*foreach ($listaDomicilios as $domicilios) {
			$domicilios['idCliente'] = $idCliente;
			$this->ModeloDomicilio_Cliente->insert($domicilios);
		}*/
		redirect('/Clientes');
	}

	public function deleteclientes()
	{
		$id = $this->input->post('id');
		$this->ModeloClientes->deleteclientes($id);
	}

	function buscarcli()
	{
		$buscar = $this->input->post('buscar');
		$resultado = $this->ModeloClientes->clientesallsearch($buscar);
		foreach ($resultado->result() as $item) { ?>
			<tr id="trcli_<?php echo $item->ClientesId; ?>">
				<td><?php echo $item->Nom; ?></td>
				<td><?php echo $item->telefonoc; ?></td>
				<td><?php echo $item->Municipio; ?></td>
				<td><?php echo $item->Calle; ?><?php echo $item->noExterior; ?><?php echo $item->Localidad; ?><?php echo $item->Municipio; ?><?php echo $item->Estado; ?></td>
				<td>
					<div class="btn-group mr-1 mb-1">
						<button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i>
						</button>
						<button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown"
								aria-haspopup="true" aria-expanded="false">
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<div class="dropdown-menu" x-placement="bottom-start"
							 style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
							<a class="dropdown-item" href="<?php echo base_url();?>Clientes/clientesUpdate/<?php echo $item->ClientesId; ?>">Editar</a>
							<a class="dropdown-item" onclick="clientesdelete(<?php echo $item->ClientesId; ?>);"
							   href="#">Eliminar</a>
						</div>
					</div>
				</td>
			</tr>
		<?php }
	}


}
