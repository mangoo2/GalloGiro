<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaTurnos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
    }
    public function index(){
            /*if (!isset($_GET['bod'])) {
                $bodega=1;
            }else{
                $bodega=$_GET['bod'];
            }*/
            if(isset($_SESSION['bodega_tz'])){
                $bodega = $_SESSION['bodega_tz'];
            }
            else{
                $bodega=0;
            }
            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'ListaTurnos/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloVentas->filastur($bodega);//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 3; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $config['reuse_query_string'] = TRUE;// para mantener los parametros del get
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["lturnos"] = $this->ModeloVentas->total_paginadosturnos($pagex,$config['per_page'],$bodega);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('turno/listaturnos',$data);
            $this->load->view('templates/footer');
    }
       
}