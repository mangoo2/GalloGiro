<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class Pedidos extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ModeloCatalogos');
		$this->load->model('ModeloClientes');
		$this->load->model('ModeloVentas');
		$this->load->model('ModeloProductos');
		$this->load->model('ModeloSproducto');
		$this->load->model('ModeloDomicilio_Cliente');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('url_helper');
		if (isset($_SESSION['bodega_tz'])) {
			$this->bodega=$_SESSION['bodega_tz'];
		}else{
			$this->bodega=0;
		}
	}

	public function index()
	{
		//echo $this->bodega; die;
		$data['bodegauser']=$this->bodega;
		$data['sturno']=$this->ModeloVentas->turnosactivo($this->bodega);
		$data['clientedefault']=$this->ModeloVentas->clientepordefecto();
		$data['categoriasll']=$this->ModeloCatalogos->categorias_all();
		$data['metodopagos']=$this->ModeloCatalogos->metodopagos();
		$this->load->view('templates/header');
		$this->load->view('templates/navbar');
		if($_SESSION['perfil_personal']==4){ //perfil de ventas 
			$data["title"]="Pedidos";
            $this->load->view('pedidos/pedidos',$data);
        }else{
        	$data["title"]="Pedidos";
        	$data["tipo"]=2;
            $this->load->view('ventas/ventas',$data);
        }
		$this->load->view('templates/footer');
		if($_SESSION['perfil_personal']==4){ //perfil de ventas 
            $this->load->view('pedidos/jspedidos');
        }else{
            $this->load->view('pedidos/jspedidos2');
        }
		unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        unset($_SESSION['kilos']);
        $_SESSION['kilos']=array();
        unset($_SESSION['precio']);
        $_SESSION['precio']=array();
	}

	function cargarmarcas(){
		$id=$this->input->post('id');
		//echo "Id del producto: "+$id;
		$marcasll=$this->ModeloCatalogos->marcashuevo_all($id);
		//var_dump($marcasll->result());
		//$datasproducto = $this->ModeloCatalogos->getsproducto_by_productoId($item->marcaid);

		foreach ($marcasll->result() as $item){
			//echo $item->marcaid;
			$datasproducto = $this->ModeloSproducto->getsproducto_by_productoId($item->marcaid);
			foreach($datasproducto->result() as $sproducto){
				$datasproductosub = $this->ModeloSproducto->getsproductosub_by_productoId($sproducto->productoaddId);
				//$sproducto->sporductosub = $datasproductosub->result();
				//log_message('error', 'productoaddId: '.$sproducto->productoaddId);
				$idpp = $datasproductosub->subId;
				//log_message('error', 'subId: '.$datasproductosub->subId);
			}
			//$item->sproducto = $datasproducto->result();
			//echo "<script>console.log(".var_dump($item).")</script>";
			if($item->imgm==''){
				$imgmar=base_url().'public/img/ops.png';
				$porcentajeimg='75%';
			}else{
				$imgmar=base_url().'public/img/marcas/'.$item->imgm;
				$porcentajeimg='135%';
			}
			//var_dump($item);
			?>
			<div class="col-lg-3 col-md-3 col-sm-12 mb-2 classproductosub" onclick="marcasselec(<?php echo $item->marcaid; ?>,'<?php echo $item->marca; ?>','<?php echo $imgmar; ?>',<?php echo $idpp?>)">
				<div class="card card-inverse bg-info text-center" style="height: 300.906px;">
					<div class="card-body">
						<div class="card-img overlap" style="background: url('<?php echo $imgmar; ?>');height: 220px;background-size: <?php echo $porcentajeimg;?>;background-position: center; background-repeat: no-repeat; ">
							<!--<img src="<?php echo $imgmar; ?>" alt="element 06" width="225" class="mb-1">-->
						</div>
						<div class="card-block">
							<h4 class="card-title"><?php echo $item->marca; ?></h4>
							<!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
						</div>
					</div>
				</div>
			</div>

			<?php
		}
	}

	public function searchContacto(){
        $id_cliente= $this->input->post("cliente");
        $direcc=$this->ModeloDomicilio_Cliente->get_dir_client($id_cliente);
        $result="";
        foreach($direcc as $c){
            $result.="<option value='$c->ClientesId'>$c->Calle $c->noExterior $c->noInterior $c->Colonia</option>";
            $result.="<option value='$c->ClientesId'>$c->calledc $c->noExtdc $c->noIntdc $c->coloniadc</option>";
        }
        echo $result;
    }

    function searchclientes(){
        $cli = $this->input->get('search');
        $results=$this->ModeloClientes->cliente_search($cli);
        echo json_encode($results->result());
    }
    public function searchproducto(){
        $pro = $this->input->get('search');
        $results=$this->ModeloProductos->productoallsearch($pro);
        echo json_encode($results->result());
    }

    function pedidodatos(){ //aqui estoy
        $id = $this->input->post('id');
        $results=$this->ModeloVentas->detalles_pedido($id);
        echo json_encode($results);
    }

}
