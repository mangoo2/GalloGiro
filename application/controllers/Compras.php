<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProveedor');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloVentas');
    }
	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('compras/compras');
        $this->load->view('templates/footer');
        $this->load->view('compras/jscompras');
	}
    public function searchpro(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProveedor->proveedorallsearch($usu);
        echo json_encode($results->result());
    }
    
    
    public function addproducto(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $prec = $this->input->post('prec');
        $prodt = $this->input->post('prodt');
        //$personalv=$this->ModeloProductos->getproducto($prod);
        //foreach ($personalv->result() as $item){
        //      $id = $item->productoid;
        //      $precio = $item->preciocompra;
        //}
        $array = array("id"=>$prod,
                        "cant"=>$cant,
                        "producto"=>$prodt,
                        "precio"=>$prec
                    );
            echo json_encode($array);
    }
    function ingresarcompra(){
        $uss = $this->input->post('uss');
        $prov = $this->input->post('prov');
        $total = $this->input->post('total');
        $id=$this->ModeloVentas->ingresarcompra($uss,$prov,$total);
        echo $id;
    }
    function ingresarcomprapro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idcompra = $DATA[$i]->idcompra;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $kilos = $DATA[$i]->kilos;
            $precio = $DATA[$i]->precio;
            $this->ModeloVentas->ingresarcomprad($idcompra,$producto,$cantidad,$kilos,$precio);
        }
    }

    function extraeInfoBarcode()
    {
        $data = $this->input->post();
        // Medimos la longitud del código de barras
        $longitud_barcode =  strlen($data["barcode_entrada"]);
        
        // Creamos un array para guardar los datos que obtengamos del código de barras
        $datosMarca = array();

        // Buscamos el o los proveedores que correspondan a la longitud del códigio de barras
        $datos = $this->ModeloVentas->getDatosMarcaByLongitud($longitud_barcode);
        
        // Guardamos el ID del Proveedor
        $datosMarca['idMarca'] = $datos[0]->marcaid;
        
        // Obtenemos los datos de posiciones
        $posicionInicialCodigoProducto = $datos[0]->codigoi;
        $longitudCodigoProducto = $datos[0]->codigof;

        $posicionInicialPeso = $datos[0]->pesoi;
        $longitudPeso = $datos[0]->pesof;

        $diferencia = $longitudPeso - $posicionInicialPeso;
        $rangoCodigoProducto = ($longitudCodigoProducto - 1) - ($posicionInicialCodigoProducto - 1);
        
        // Obtenemos la parte del código de Producto que viene en el codigo de barras
        $codigoProducto = substr($data["barcode_entrada"],($posicionInicialCodigoProducto - 1),$rangoCodigoProducto + 1);
        
        // Con este codigo vamos a buscar a la BD los datos del Producto para obtener su ID y lo guardamos en el arreglo
        $datosProducto = $this->ModeloProductos->getDatosProducto($codigoProducto);
        
        $datosMarca['vproducto'] = $datosProducto->productoaddId;
        $datosMarca['vproductonombre'] = $datosProducto->categoria;
        
        // Si la diferencia es de 4, trae un punto en el código de barras
        // Por lo tanto, a la hora de extraer la información del peso, obtendremos 5 digitos
        if($diferencia==4)
        {
            $datosMarca['pesoProducto'] = substr($data["barcode_entrada"],($posicionInicialPeso - 1),5); 
        }
        else
        {
            $pesoProductoAux = substr($data["barcode_entrada"],($posicionInicialPeso - 1),4); 
            $pesoProductoKilos = substr($pesoProductoAux,0,2); 
            $pesoProductoGramos = substr($pesoProductoAux,2,2); 
            $datosMarca['pesoProducto'] = $pesoProductoKilos.".".$pesoProductoGramos;
        }

        // Regresamos el array
        echo json_encode($datosMarca);
    }

}