<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloVentas');
        $this->load->model('ModeloProductos');
        if (isset($_SESSION['bodega_tz'])) {
            $this->bodega=$_SESSION['bodega_tz'];
        }else{
            $this->bodega=0;
        }
    }
    public function index(){
        $data['bodegauser']=$this->bodega;
        $data['sturno']=$this->ModeloVentas->turnosactivo($this->bodega);
        //$data['clientedefault']=$this->ModeloVentas->clientepordefecto();
        $data['categoriasll']=$this->ModeloCatalogos->categorias_all();
        $data['metodopagos']=$this->ModeloCatalogos->metodopagos();
        //$data['marcasll']=$this->ModeloCatalogos->marcashuevo_all();
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        //$this->load->view('ventas/ventas',$data);
        if($_SESSION['perfil_personal']==4){ //perfil de ventas 
            $this->load->view('ventas/ventas2',$data);
        }else{
            $data["title"]="Ventas";
            $data["tipo"]=1;
            $this->load->view('ventas/ventas',$data); 
        }
        //log_message('error', 'perfil_personal: '.$_SESSION['perfil_personal']); 
        $this->load->view('templates/footer');

        if($_SESSION['perfil_personal']==4){ //perfil de ventas 
            $this->load->view('ventas/jsventas');
        }else{
            $this->load->view('ventas/jsventas2');
        }
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        unset($_SESSION['kilos']);
        $_SESSION['kilos']=array();
        unset($_SESSION['precio']);
        $_SESSION['precio']=array();
    }
    public function searchcli(){
        $usu = $this->input->get('search');
        $results=$this->ModeloClientes->clientesallsearch($usu);
        echo json_encode($results->result());
    }
    public function searchproducto(){
        $pro = $this->input->get('search');
        $results=$this->ModeloProductos->productoallsearch($pro);
        echo json_encode($results->result());
    }
    public function addproducto(){

        $cant = $this->input->post('cant');
        $kilos = $this->input->post('kilos');
        $prod = $this->input->post('proc');
        $prodd = $this->input->post('prod');
        $producton=$this->input->post('producton');
        $marcan=$this->input->post('marcan');
        $categorian=$this->input->post('categorian');
        $productoss=$this->ModeloProductos->getsproductosub($prod);
        log_message('error', 'prodd desde addprod: '.$prodd);
        //$oProducto = new Producto();
        foreach ($productoss->result() as $item){
          $id = $item->subId;
          if ($this->bodega==2) {
                $precio = $item->precio2;
                $precio_mm = $item->precio_mm2;
                $cantidad_mm = $item->cantidad_mm2;
                $precio_m = $item->precio_m2;
                $cantidad_m = $item->cantidad_m2;
          }elseif ($this->bodega==3) {
                $precio = $item->precio3;
                $precio_mm = $item->precio_mm3;
                $cantidad_mm = $item->cantidad_mm3;
                $precio_m = $item->precio_m3;
                $cantidad_m = $item->cantidad_m3;
          }else{
                $precio = $item->precio;
                $precio_mm = $item->precio_mm;
                $cantidad_mm = $item->cantidad_mm;
                $precio_m = $item->precio_m;
                $cantidad_m = $item->cantidad_m;
          }
          /*if($cant==$cantidad_mm){
            
          }*/
          $oProducto=array("id"=>$id,"producton"=>$producton,"marcan"=>$marcan,"categorian"=>$categorian,'precio'=>$precio,'mediomayoreo'=>$precio_mm,'canmediomayoreo'=>$cantidad_mm,'mayoreo'=>$precio_m,'canmayoreo'=>$cantidad_m,"prodd"=>$prodd);

          //var_dump($oProducto);

        }
        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
            /*$_SESSION['kilos'][$idx]+=$kilos;
            log_message('error', '$_SESSION["kilos"][$idx] '.$_SESSION['kilos'][$idx]);*/
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
            //array_push($_SESSION['kilos'],$kilos);
        }
        //======================================================================
        $count = 0;
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count];
            $kiloss=$_SESSION['kilos'][$count]; 
            $id=$fila["id"];
            //log_message('error', 'kiloss: '.$kiloss);
        
            if ($Cantidad>=$fila['canmayoreo'] && $fila['canmayoreo']>0) {
                $precio=$fila['mayoreo'];
            }elseif ($Cantidad>=$fila['canmediomayoreo'] && $Cantidad<$fila['canmayoreo'] && $fila['canmayoreo']>0) {
                $precio=$fila['mediomayoreo'];
            }else{
                $precio=$fila['precio'];
            }
            /*if ($kiloss>0) {
                $cantotal=$kiloss*$precio;
            }else{
                $cantotal=$Cantidad*$precio;
            }*/
            //log_message('error', 'desde addproducto2 $_SESSION["precio"][$prodd]: '.$_SESSION['precio'][$prodd]);
            $prodx=$fila['prodd'];
            if ($_SESSION['precio'][$prodx]>0) {
                $precio = $_SESSION['precio'][$prodx];
                log_message('error', 'ppppp: '.$prodd);
                
            }
            $cantotal=$Cantidad*$precio;
            //log_message('error', 'precio en tabla prods: '.$precio);
            ?>
            <tr class="producto_<?php echo $count;?>" style="font-size: 14px;">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $prodx;?>">
                    <?php echo $fila['producton'];?>
                </td>
                <td>
                    <?php echo $fila['marcan'];?>
                </td>
                <td>
                    <?php echo $fila['categorian'];?>
                </td>                                        
                <td>
                    <input type="number" class="<?php echo "cant_".$prodx."" ?>" name="vscanti" id="vscanti<?php echo $count;?>" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 60px;">
                </td>
                <td>
                    <div class="input-group">
                        <input type="hidden" name="vskilos" id="vskilos" value="<?php echo $kiloss;?>" readonly style="background: transparent;border: 0px; width: 45px; <?php   if ($kiloss==0) { echo' color:transparent;'; };?>"><?php   if ($kiloss>0) { echo''; };?>
                    </div>
                </td>                                        
                <!--<td>$ <input type="text" name="vsprecio" id="vsprecio" value="<?php echo $precio;?>" style="background: transparent;border: 0px;width: 60px;"></td>-->    
                <!--<td>$ <input class="<?php echo "prec_".$fila['id']."" ?>" type="text" name="vsprecio" id="vsprecio<?php echo $count;?>" value="<?php echo $precio;?>" style="width:40px;" onchange="cambioPrecio(this.value,<?php echo $count;?>);"></td>-->
                <td>$ <input class="<?php echo "prec_".$prodx."" ?>" type="text" name="vsprecio" id="vsprecio" data-rowprecio="<?php echo $count;?>" oninput="recalcula(<?php echo $prodx;?>)" value="<?php echo $precio;?>" style="background: transparent;border: 0px;width: 100px;"></td>                                    
                <td>$ <input type="text" class="vstotal <?php echo "vstotal_".$prodx."" ?>" name="vstotal" id="vstotal<?php echo $count;?>" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px; width: 61px;"></td>                                        
                <td>                                            
                    <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $count;?>)">
                        <i class="ft-trash font-medium-3"></i>
                    </a>
                </td>
            </tr>
        <?php
        $count++;
        }
    }

    public function addproducto2(){
        $cant = $this->input->post('cant');
        $prod = $this->input->post('prod');
        $band = $this->input->post('band');

        /*log_message('error', 'cant: '.$cant);
        log_message('error', 'prod: '.$prod);
        log_message('error', 'band: '.$band);*/

        $personalv=$this->ModeloProductos->getproducto2($prod);
        //$oProducto = new Producto();
        foreach ($personalv->result() as $item){
            $id = $item->subId;
            if ($this->bodega==2) {
                $precio = $item->precio2;
                $precio_mm = $item->precio_mm2;
                $cantidad_mm = $item->cantidad_mm2;
                $precio_m = $item->precio_m2;
                $cantidad_m = $item->cantidad_m2;
            }elseif ($this->bodega==3) {
                $precio = $item->precio3;
                $precio_mm = $item->precio_mm3;
                $cantidad_mm = $item->cantidad_mm3;
                $precio_m = $item->precio_m3;
                $cantidad_m = $item->cantidad_m3;
            }else{
                $precio = $item->precio;
                $precio_mm = $item->precio_mm;
                $cantidad_mm = $item->cantidad_mm;
                $precio_m = $item->precio_m;
                $cantidad_m = $item->cantidad_m;
            }
            $producton=$item->marca." ".$item->categoria;
            $marcan=$item->marca;
            $categorian=$item->categoria;

            $oProducto=array("id"=>$id,"producton"=>$producton,"marcan"=>$marcan,"categorian"=>$categorian,'precio'=>$precio,'mediomayoreo'=>$precio_mm,'canmediomayoreo'=>$cantidad_mm,'mayoreo'=>$precio_m,'canmayoreo'=>$cantidad_m);

        }

        if(in_array($oProducto, $_SESSION['pro'])){  // si el producto ya se encuentra en la lista de compra suma las cantidades
            $idx = array_search($oProducto, $_SESSION['pro']);
            $_SESSION['can'][$idx]+=$cant;
            //$_SESSION['kilos'][$idx]+=$kilos;
            //$_SESSION['precio'][$idx]=$precio;
        }
        else{ //sino lo agrega
            array_push($_SESSION['pro'],$oProducto);
            array_push($_SESSION['can'],$cant);
            //array_push($_SESSION['kilos'],$kilos);
            //array_push($_SESSION['precio'],$prod);
            //$cont_prods++;
        }
        //======================================================================
        $count = 0;
        
        $n =array_sum($_SESSION['can']);
        foreach ($_SESSION['pro'] as $fila){
            $Cantidad=$_SESSION['can'][$count]; 

            $id=$fila["id"];
            /*
            if($band!=""){
                log_message('error', 'exist producto '.$fila["id"]);
                //$fila['costo']=$band;
                //$precio=$fila['costo'];
                
                $precio = $_SESSION['precio'][$id];
                log_message('error', 'precio de sesion: '.$_SESSION['precio'][$id]);
                $cantotal=$Cantidad*$precio;
            }else if($band==""){
            */
                //log_message('error', 'nuevo producto '.$prod);
                if ($Cantidad>=$fila['canmayoreo'] && $fila['canmayoreo']>0) {
                    $precio=$fila['mayoreo'];
                }elseif ($Cantidad>=$fila['canmediomayoreo'] && $Cantidad<$fila['canmayoreo'] && $fila['canmayoreo']>0) {
                    $precio=$fila['mediomayoreo'];
                }else{
                    $precio=$fila['precio'];
                }
                if ($_SESSION['precio'][$id]>0) {
                    $precio = $_SESSION['precio'][$id];
                }
                //$id=$fila["id"];
                //$precio = $_SESSION['precio'][$prod];
                $cantotal=$Cantidad*$precio;
            //}
            //log_message('error', 'cont_prods: '.$cont_prods);
            //log_message('error', 'fila["id"]: '.$fila["id"]);
            //log_message('error', 'precio en tabla prods: '.$precio);
            ?>
            <tr class="producto_<?php echo $count;?>">                                        
                <td>
                    <input type="hidden" name="vsproid" id="vsproid" value="<?php echo $fila['id'];?>">
                    <input type="hidden" id="band_<?php echo $fila['id'];?>" value="0">
                    <?php echo $fila['marcan'];?>
                </td>                                        
                <td>
                    <input class="<?php echo "cant_".$fila['id']."" ?>" type="number" name="vscanti" id="vscanti" value="<?php echo $Cantidad;?>" readonly style="background: transparent;border: 0px; width: 80px;">
                </td>                                        
                <td><?php echo $fila['categorian'];?></td>                                        
                <td>$ <input class="<?php echo "prec_".$fila['id']."" ?>" type="text" name="vsprecio" id="vsprecio" data-rowprecio="<?php echo $count;?>" oninput="recalcula(<?php echo $fila['id'];?>)" value="<?php echo $precio;?>" style="background: transparent;border: 0px;width: 100px;"></td>                                        
                <td>$ <input type="text" class="vstotal <?php echo "vstotal_".$fila['id']."" ?>" name="vstotal" id="vstotal" value="<?php echo $cantotal;?>" readonly style="background: transparent;border: 0px; width: 100px;"></td>                                        
                <td>                                            
                    <a class="danger" data-original-title="Eliminar" title="Eliminar" onclick="deletepro(<?php echo $count;?>)">
                        <i class="ft-trash font-medium-3"></i>
                    </a>
                </td>
            </tr>
            <?php
            $count++;
        }
    }
    public function precio_prod(){

        $id = $this->input->post('id');
        $precio = $this->input->post('precio');
        log_message('error', 'id para precio prod: '.$id);
        log_message('error', 'precio prod: '.$precio);
        $_SESSION['precio'][$id]=$precio;
        log_message('error', 'desde precio prod $_SESSION["precio"][$id]: '.$_SESSION['precio'][$id]);
        //echo 'x';
        //echo json_encode($_SESSION['pro']);
    }

    function deleteproducto(){
        $idd = $this->input->post('idd');
        unset($_SESSION['pro'][$idd]);
        $_SESSION['pro']=array_values($_SESSION['pro']);
        unset($_SESSION['can'][$idd]);
        $_SESSION['can'] = array_values($_SESSION['can']); 
        unset($_SESSION['kilos'][$idd]);
        $_SESSION['kilos'] = array_values($_SESSION['kilos']); 
        unset($_SESSION['precio'][$idd]);
        $_SESSION['precio'] = array_values($_SESSION['precio']); 
    }
    public function productoclear(){
        unset($_SESSION['pro']);
        $_SESSION['pro']=array();
        unset($_SESSION['can']);
        $_SESSION['can']=array();
        unset($_SESSION['kilos']);
        $_SESSION['kilos']=array();
        unset($_SESSION['precio']);
        $_SESSION['precio']=array();

        //echo "limpiado";
    }
    function searchcliente(){
        $search = $this->input->post('search');
        $clientes=$this->ModeloCatalogos->searchcliente($search);
        foreach ($clientes->result() as $item){ ?>
            <li class="list-group-item" data-dismiss="modal" onclick="addcliente(<?php echo $item->ClientesId;?>,'<?php echo $item->Nom;?>')"><?php echo $item->Nom;?></li>
        <?php }
    }   
    function ingresarventa(){
        $uss = $this->input->post('uss');
        $cli = $this->input->post('cli');
        $total = $this->input->post('total');
        $fac = $this->input->post('fac');
        $des = $this->input->post('desc');
        $metodo = $this->input->post('mpago');//agregar pagado, si pago = a monto total, pagado es = 1
        $pagado=0;
        if($metodo!=4){
            $pagado=1;
        }
        $id=$this->ModeloVentas->ingresarventa($uss,$cli,$total,$fac,$des,$metodo,1,$pagado);
        echo $id;
    }
    function ingresarventa2(){
        $uss = $this->input->post('uss');
        $cli = $this->input->post('cli');
        $mpago = $this->input->post('mpago');
        $desc = $this->input->post('desc');
        $descu = $this->input->post('descu');
        $sbtotal = $this->input->post('sbtotal');
        $total = $this->input->post('total');
        $fac = $this->input->post('fac');
        $efectivo = $this->input->post('efec');
        $tarjeta = $this->input->post('tarjeta');
        $tipo = $this->input->post('tipo');
        $total_pago = floatval($efectivo)+floatval($tarjeta);
        $pagado=0;
        /*log_message('error', 'metodo: '.$mpago); 
        log_message('error', 'total_pago: '.$total_pago); 
        log_message('error', 'efectivo: '.$efectivo); 
        log_message('error', 'tarjeta: '.$tarjeta); */
        if($total_pago>=$total || $mpago!=4){
            $pagado=1;
        }
        //$id=$this->ModeloVentas->ingresarventa2($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta,$tipo,$pagado);
        //echo $id;
    }
    function ingresarpedido(){
        $uss = $this->input->post('uss');
        $cli = $this->input->post('cli');
        $total = $this->input->post('total');
        $fac = $this->input->post('fac');
        $des = $this->input->post('des');
        $metodo = $this->input->post('metodo');//agregar pagado, si pago = a monto total, pagado es = 1
        $pagado=0;
        log_message('error', 'metodo: '.$metodo); 
        if($metodo!=4){
            $pagado=1;
        }
        $id=$this->ModeloVentas->ingresarventa($uss,$cli,$total,$fac,$des,$metodo,2,$pagado);
        echo $id;
        //log_message('error', 'metodo: '.$metodo); 
    }
    function ingresarpedido2(){
        $uss = $this->input->post('uss');
        $cli = $this->input->post('cli');
        $mpago = $this->input->post('mpago');
        $desc = $this->input->post('desc');
        $descu = $this->input->post('descu');
        $sbtotal = $this->input->post('sbtotal');
        $total = $this->input->post('total');
        $efectivo = $this->input->post('efec');
        $tarjeta = $this->input->post('tarjeta');
        $tipo = $this->input->post('tipo');
        $total_pago = floatval($efectivo)+floatval($tarjeta);
        $pagado=0;
        //log_message('error', 'metodo: '.$mpago); 
        if($total_pago>=$total || $mpago!=4){
            $pagado=1;
        }
        /*log_message('error', 'total_pago: '.$total_pago); 
        log_message('error', 'efectivo: '.$efectivo); 
        log_message('error', 'tarjeta: '.$tarjeta); */
        $id=$this->ModeloVentas->ingresarventa2($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta,$tipo,$pagado);
        echo $id;
    }

    function ingresarventapro($tipo){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            $kilos = $DATA[$i]->kilos;
            $precio = $DATA[$i]->precio;
            if($tipo==2){ //falta mandar estos parametros desde la vista de ventas
              $direc = $DATA[$i]->direccion; 
              $fecha_ped = $DATA[$i]->fecha_pedido;  
            }
            $id_dv = $this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$kilos,$precio);
        }
        //$this->ModeloVentas->ingresarpedidod($id_dv,0,date('Y-m-d H:i:s'),''); 
        if($tipo==2)
            $this->ModeloVentas->ingresarpedidod($idventa,'0',$fecha_ped, $direc);//inserta pedidos detalle.. fecha debe ir vacia, hasta que se entregue se actualiza la fecha entrega AQUIIIIIIIIII
    }

    /*function ingresarpedidopro(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idventa = $DATA[$i]->idventa;
            $producto = $DATA[$i]->producto;
            $cantidad = $DATA[$i]->cantidad;
            //$kilos = $DATA[$i]->kilos;
            $precio = $DATA[$i]->precio;
            $this->ModeloVentas->ingresarventad($idventa,$producto,$cantidad,$precio);
        }
    }*/
    function abrirturno(){
        $cantidad = $this->input->post('cantidad');
        $nombre = $this->input->post('nombre');
        $bodega = $this->input->post('bodega');
        $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
        $horaa =date('H:i:s');
        $this->ModeloVentas->abrirturno($cantidad,$nombre,$fecha,$horaa,$bodega);
    }
    function cerrarturno(){
            $id = $this->input->post('id');
            $fecha =date('Y').'-'.date('m'). '-'.date('d'); 
            $horaa =date('H:i:s');
            $this->ModeloVentas->cerrarturno($id,$horaa);
    }   
    function consultarturno(){
        $id = $this->input->post('id');
        $bodega = $this->input->post('bod');
        $vturno=$this->ModeloVentas->consultarturno($id);
        foreach ($vturno->result() as $item){
            $fecha= $item->fecha;
            $fechac= $item->fechacierre;
            $horaa= $item->horaa;
            $horac= $item->horac;
        }
        if ($horac=='00:00:00') {
            $horac =date('H:i:s');
            $fechac =date('Y-m-d');
        }
        $totalventas=$this->ModeloVentas->consultartotalturno($fecha,$horaa,$horac,$fechac,$bodega);
        echo "<p><b>Total: ".number_format($totalventas,2,'.',',')."</b></p>";
        //$totalpreciocompra=$this->ModeloVentas->consultartotalturno2($fecha,$horaa,$horac,$fechac,$bodega);
        //$obed =$totalventas-$totalpreciocompra;
        //echo "<p><b>Utilidad: ".number_format($obed,2,'.',',')."</b></p>";
        $productos =$this->ModeloVentas->consultartotalturnopro($fecha,$horaa,$horac,$fechac,$bodega);
        echo "<div class='panel-body table-responsive' id='table'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables2'>
                            <thead class='vd_bg-green vd_white'>
                                <tr>
                                    <th>Producto</th>
                                    <th>Cantidad</th>
                                    <th></th>
                                    <th>Precio</th>
                                    
                                </tr>
                            </thead>
                            <tbody>";
        foreach ($productos->result() as $item){
            if ($item->kilos>0) {
                $kilosl='Kilos: '.$item->kilos;
            }else{
                $kilosl='';
            }
            echo "<tr><td>".$item->producto."</td><td>".$item->cantidad."</td><td>".$kilosl."</td><td> $".$item->precio."</td></tr>";
        }
        echo "</tbody> </table></div>";
        echo "<div style='    position: absolute; float: left; top: 0; left: 800px; background: white; width: 250px; z-index: 20;'>
                            <table class='table table-striped table-bordered table-hover' id='data-tables'>
                            <thead>
                                <tr>
                                    <td colspan='4' style='text-align: center;'>Productos mas vendido</td>
                                </tr>
                            </thead>
                            <tbody>";
        $productosmas =$this->ModeloVentas->consultartotalturnopromas($fecha,$horaa,$horac,$fechac,$bodega);
        foreach ($productosmas->result() as $item){
            echo "<tr><td colspan='2' style='text-align: center;'>".$item->total." </td><td colspan='2' style='text-align: center;'>".$item->producto." </td></tr>";
        }
        echo "</tbody> </table></div>";
    }
    function cargarmarcas(){
        $id=$this->input->post('id');
        $marcasll=$this->ModeloCatalogos->marcashuevo_all($id);
        foreach ($marcasll->result() as $item){
            if($item->imgm==''){
                $imgmar=base_url().'public/img/ops.png';
                $porcentajeimg='75%';
            }else{
                $imgmar=base_url().'public/img/marcas/'.$item->imgm;
                $porcentajeimg='135%';
            }
            ?>
                <div class="col-lg-3 col-md-3 col-sm-12 mb-2 classproductosub" onclick="marcasselec(<?php echo $item->marcaid; ?>,'<?php echo $item->marca; ?>','<?php echo $imgmar; ?>')">
                    <div class="card card-inverse bg-info text-center" style="height: 300.906px;">
                        <div class="card-body">
                            <div class="card-img overlap" style="background: url('<?php echo $imgmar; ?>');height: 220px;background-size: <?php echo $porcentajeimg;?>;background-position: center; background-repeat: no-repeat; ">
                                <!--<img src="<?php echo $imgmar; ?>" alt="element 06" width="225" class="mb-1">-->
                            </div>
                            <div class="card-block">
                                <h4 class="card-title"><?php echo $item->marca; ?></h4>
                                <!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
                            </div>
                        </div>
                    </div>
                </div>

            <?php
        }
    }
    function cargarpresentacion(){
        $pro=$this->input->post('pro');
        $mar=$this->input->post('mar');
        $presentacion_all=$this->ModeloCatalogos->presentacion_all($pro,$mar);
        $cont=0;
        foreach ($presentacion_all->result() as $item) {
            $cont++;
            if ($this->bodega==2) {
                $precio=$item->precio2;
            }elseif ($this->bodega==3) {
                $precio=$item->precio3;
            }else{
                $precio=$item->precio;
            }
            ?>
            <div class="col-lg-3 col-md-3 col-sm-12 mb-2 classproductotipo" onclick="presentselec(5,'Kilos',<?php echo $precio; ?>)">
                <div class="card card-inverse bg-info text-center" style="height: 300.906px;">
                    <div class="card-body">
                        
                        <div class="card-block">
                            <h4 class="card-title"><?php echo $item->presentacion; ?></h4>
                            <!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
                        </div>
                    </div>
                </div>
            </div>
        <?php
        }
    }
    function productoverificarstok(){
        $proc=$this->input->post('proc');
        $cant=$this->input->post('cant');
        $producto=$this->ModeloProductos->getsproductosub($proc);
        foreach ($producto->result() as $item) {
            switch ($this->bodega) {
                case 3:
                    $stock=$item->stok3;
                    break;
                case 2:
                    $stock=$item->stok2;
                    break;
                default:
                    $stock=$item->stok;
                    break;
            }
        }
        if ($stock>=$cant) {
            $cantn=$cant;
            $cantstatus=1;
        }else{
            $cantn=$stock;
            $cantstatus=0;
        }
        $array = array("canti"=>$cantn,"estatus"=>$cantstatus);
            echo json_encode($array);
    }
    function datosfactura(){
        $idventa=$this->input->post('idventa');
        $datos=$this->input->post('datos');
        echo $this->ModeloVentas->datosfactura($idventa,$datos);
    }

}
