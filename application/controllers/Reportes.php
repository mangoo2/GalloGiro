<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class Reportes extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('ModeloReportes');
        $this->load->model('ModeloClientes');
        $this->load->model('ModeloProductos');
        /*if (!$this->session->userdata('logeado')){
          redirect('/Login');
	    }else{
	        $this->perfilid=$this->session->userdata('perfilid');
	        //ira el permiso del modulo
	        $this->load->model('Login_model');
	        $permiso=$this->Login_model->getviewpermiso($this->perfilid,18);// 18 es el id del submenu
	        if ($permiso==0) {
	            redirect('/Sistema');
	        }
	    }*/
    }

	public function index(){
		$this->load->view('templates/header');
        $this->load->view('templates/navbar');
		$this->load->view('Reportes/edoCuenta');
		$this->load->view('templates/footer');
        $this->load->view('Reportes/edoCuentajs');
	}

	function searchclientes(){
        $cli = $this->input->get('search');
        $results=$this->ModeloClientes->cliente_search($cli);
        echo json_encode($results->result());
    }

	public function reporteEstadoCuenta() {
	    $params = $this->input->post();
        $datas=$this->ModeloReportes->getReporteEdos($params);
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    } 

    public function reporteSumaVenta() {
        $params = $this->input->post();
        $datas=$this->ModeloReportes->getSumaVenta($params);
        foreach ($datas as $key) {
            $total_venta = $key->total_venta;
        }
        echo $total_venta;
    } 

    public function generarReporteEdos($cli,$fi,$ff){
    	$params["cliente"]=$cli;
        //$params["id"]=$cli;
    	$params["fechai"]=$fi;
    	$params["fechaf"]=$ff;
        /*$datas=$this->ModeloReportes->getSumaVenta($params);
        $total_venta=0;
        foreach ($datas as $key) {
            $total_venta = $key->total_venta;
        }*/
    	$data['info']=$this->ModeloReportes->getReporteEdos2($params);
        //$data['total_venta']=$total_venta;
    	$this->load->view('Reportes/excelEdos',$data);
    }

    public function generarReporteProds(){
        $data['info']=$this->ModeloProductos->allProductos();
        $this->load->view('Reportes/excelProds',$data);
    }


    /* *********************************** */
    public function ventas(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Reportes/ventas');
        $this->load->view('templates/footer');
        $this->load->view('Reportes/ventasjs');
    }

    public function datatable_reporteprodsv(){
        $params=$this->input->post();
        $ventprods = $this->ModeloReportes->get_ventas_prods($params);
        $totalRecords=$this->ModeloReportes->get_ventas_prods_tot($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $ventprods,
            "query"           => $this->db->last_query()   
            );
        echo json_encode($json_data);
    }

    public function exportVentaProds($fi,$ff,$id_suc){
        $params["fechai"]=$fi;
        $params["fechaf"]=$ff;
        $params["id_sucursal"]=$id_suc;
        $data["e"] = $this->ModeloReportes->get_ventas_prods($params);
        $this->load->view('Reportes/exportExcelVP',$data);
    }

    public function datatable_reporteprodsPed(){
        $params=$this->input->post();
        $ventprods = $this->ModeloReportes->get_pedidos_prods($params);
        $totalRecords=$this->ModeloReportes->get_pedidos_prods_tot($params);

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval( $totalRecords ),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $ventprods,
            "query"           => $this->db->last_query()   
            );
        echo json_encode($json_data);
    }

    public function exportPedidoProds($fi,$ff,$id_suc,$id_cli){
        $params["fechai"]=$fi;
        $params["fechaf"]=$ff;
        $params["id_sucursal"]=$id_suc;
        $params["id_cliente"]=$id_cli;
        $data["e"] = $this->ModeloReportes->get_pedidos_prods($params);
        $this->load->view('Reportes/exportExcelPP',$data);
    }

}
