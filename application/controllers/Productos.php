<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloProductos');
        $this->load->model('ModeloCatalogos');
    }
	public function index(){
            /*$pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'Productos/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloProductos->filas();//calcula el número de filas
            $config['per_page'] = 10; //Número de registros mostrados por páginas  
            $config['num_links'] = 3; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["productos"] = $this->ModeloProductos->total_paginados($pagex,$config['per_page']);
            //$data["productosp"] = $this->ModeloProductos->total_paginadosp($pagex,$config['per_page']);
            $data['total_rows'] = $this->ModeloProductos->filas();
            //$data['totalexistencia'] = $this->ModeloProductos->totalproductosenexistencia();
            //$data['totalproductopreciocompra'] = $this->ModeloProductos->totalproductopreciocompra();
            //$data['totalproductoporpreciocompra'] = $this->ModeloProductos->totalproductoporpreciocompra(); 
            */

            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('productos/productos');
            $this->load->view('templates/footer');
            $this->load->view('productos/jsproducto');
	}
    public function productosadd(){
        $data["categorias"] = $this->ModeloProductos->categorias();
        $data["marcaall"] = $this->ModeloCatalogos->marcas_all();
        $data["presentacion"] = $this->ModeloCatalogos->GetAllActive('presentaciones');
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('productos/productoadd',$data);
        $this->load->view('templates/footer');
        $this->load->view('productos/jsproducto');
    }
    function productoadd(){
        $id = $this->input->post('id');
        $prod = $this->input->post('prod');
        $marca = $this->input->post('marca');
        $compra = $this->input->post('compra');
        $codigoProducto = $this->input->post('codigoProducto');
    
        if ($id>0) {
            $this->ModeloProductos->productosupdate($id,$prod,$marca,$compra,$codigoProducto);
            echo $id;
        }else{
            $idd=$this->ModeloProductos->productosinsert($prod,$marca,$compra,$codigoProducto);
            echo $idd;
        }
    }
    function productoaddetalle(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idpro = $DATA[$i]->idpro;
            $idp = $DATA[$i]->ppresentacionid;
            $presenId = $DATA[$i]->idPresentacion;
            //$presentacion = 30;
            $tipo = $DATA[$i]->tipo;

            $cantidad = $DATA[$i]->cantidad;
            $precio = $DATA[$i]->precio;
            $apartirmm = $DATA[$i]->apartirmm;
            $prepreciomm = $DATA[$i]->prepreciomm;
            $apartirm = $DATA[$i]->apartirm;
            $prepreciom = $DATA[$i]->prepreciom;

            $cantidad2 = $DATA[$i]->cantidad2;
            $precio2 = $DATA[$i]->precio2;
            $apartirmm2 = $DATA[$i]->apartirmm2;
            $prepreciomm2 = $DATA[$i]->prepreciomm2;
            $apartirm2 = $DATA[$i]->apartirm2;
            $prepreciom2 = $DATA[$i]->prepreciom2;

            $cantidad3 = $DATA[$i]->cantidad3;
            $precio3 = $DATA[$i]->precio3;
            $apartirmm3 = $DATA[$i]->apartirmm3;
            $prepreciomm3 = $DATA[$i]->prepreciomm3;
            $apartirm3 = $DATA[$i]->apartirm3;
            $prepreciom3 = $DATA[$i]->prepreciom3;

            $cantidad4 = $DATA[$i]->cantidad4;
            $precio4 = $DATA[$i]->precio4;
            $apartirmm4 = $DATA[$i]->apartirmm4;
            $prepreciomm4 = $DATA[$i]->prepreciomm4;
            $apartirm4 = $DATA[$i]->apartirm4;
            $prepreciom4 = $DATA[$i]->prepreciom4;

            $cantidad5 = $DATA[$i]->cantidad5;
            $precio5 = $DATA[$i]->precio5;
            $apartirmm5 = $DATA[$i]->apartirmm5;
            $prepreciomm5 = $DATA[$i]->prepreciomm5;
            $apartirm5 = $DATA[$i]->apartirm5;
            $prepreciom5 = $DATA[$i]->prepreciom5;

            $cantidad6 = $DATA[$i]->cantidad6;
            $precio6 = $DATA[$i]->precio6;
            $apartirmm6 = $DATA[$i]->apartirmm6;
            $prepreciomm6 = $DATA[$i]->prepreciomm6;
            $apartirm6 = $DATA[$i]->apartirm6;
            $prepreciom6 = $DATA[$i]->prepreciom6;
            
            if ($idp==0) {
                
                
               echo $this->ModeloProductos->productoaddetalleinser($idpro,$presenId,$cantidad,$precio,$apartirmm,$prepreciomm,$apartirm,$prepreciom,$tipo,$cantidad2,$precio2,$apartirmm2,$prepreciomm2,$apartirm2,$prepreciom2,$cantidad3,$precio3,$apartirmm3,$prepreciomm3,$apartirm3,$prepreciom3,$cantidad4,$precio4,$apartirmm4,$prepreciomm4,$apartirm4,$prepreciom4,$cantidad5,$precio5,$apartirmm5,$prepreciomm5,$apartirm5,$prepreciom5,$cantidad6,$precio6,$apartirmm6,$prepreciomm6,$apartirm6,$prepreciom6);
            }else{
                    $this->ModeloProductos->productoaddetalleupdate($idp,$idpro,$presenId,$cantidad,$precio,$apartirmm,$prepreciomm,$apartirm,$prepreciom,$tipo,$cantidad2,$precio2,$apartirmm2,$prepreciomm2,$apartirm2,$prepreciom2,$cantidad3,$precio3,$apartirmm3,$prepreciomm3,$apartirm3,$prepreciom3,$cantidad4,$precio4,$apartirmm4,$prepreciomm4,$apartirm4,$prepreciom4,$cantidad5,$precio5,$apartirmm5,$prepreciomm5,$apartirm5,$prepreciom5,$cantidad6,$precio6,$apartirmm6,$prepreciomm6,$apartirm6,$prepreciom6);
            }




        }
    }
    public function deleteproductos(){
        $id = $this->input->post('id');
        $this->ModeloProductos->productosdelete($id); 
    }
    function deleterow(){
        $id = $this->input->post('id');
        $row=$this->ModeloProductos->deleterowv($id);
        if ($row==0) {
            $this->ModeloProductos->deleterow($id);
        }
        echo $row;
    }
    
    
    function buscarpro(){
        $buscar = $this->input->post('buscar');
        $resultado=$this->ModeloProductos->productoallsearch($buscar);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trpro_<?php echo $item->productoaddId; ?>">
              <td><?php echo $item->productoaddId; ?></td>
              <td>
                <?php 
                  if ($item->img=='') {
                    $img='public/img/ops.png';
                  }else{
                    $img='public/img/categoriat/'.$item->img;
                  }
                ?>
                <img src="<?php echo base_url(); ?><?php echo $img; ?>" class="imgpro" >
                </td>
              <td><?php echo $item->categoria; ?></td>
              <td><?php echo $item->marca; ?></td>
              <?php if($_SESSION['perfilid_tz']==1){ ?>
                <td><?php echo $item->stok; ?></td>
                <td><?php echo $item->stok2; ?></td>
                <td><?php echo $item->stok3; ?></td>
              <?php } 
              else if($_SESSION['bodega_tz']==1){ ?>
                <td><?php echo $item->stok; ?></td>
              <?php } ?>  
              <?php  if($_SESSION['bodega_tz']==2){ ?>
                <td><?php echo $item->stok2; ?></td>
              <?php } ?>   
              <?php  if($_SESSION['bodega_tz']==3){ ?>
                <td><?php echo $item->stok3; ?></td>
              <?php } ?>   
              <!--<td><?php echo $item->stok4; ?></td>
              <td><?php echo $item->stok5; ?></td>
              <td><?php echo $item->stok6; ?></td>-->
              <td>
                  <div class="btn-group mr-1 mb-1">
                  <button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>
                  <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                    <?php if($_SESSION['perfilid_tz']==1){?>
                      <a class="dropdown-item" href="<?php echo base_url();?>Productos/productosadd?id=<?php echo $item->productoaddId; ?>">Editar</a>
                    <?php } ?>
                      <!--<a class="dropdown-item" onclick="etiquetas(<?php echo $item->productoaddId; ?>,'0','<?php echo $item->categoria; ?>','<?php echo $item->categoria; ?>',<?php echo 0; ?>);"href="#">Etiquetas</a>-->
                      <?php if ($item->categoriaId==1) { ?>
                        <a class="dropdown-item" onclick="mermas(<?php echo $item->productoaddId; ?>);"href="#">Mermas</a>
                      <?php } ?>
                      <a class="dropdown-item" onclick="traspaso(<?php echo $item->productoaddId; ?>,<?php echo $item->stok; ?>,<?php echo $item->stok2; ?>,<?php echo $item->stok3; ?>);"href="#">Traspaso</a>
                    <?php if($_SESSION['perfilid_tz']==1){?>
                      <a class="dropdown-item" onclick="productodelete(<?php echo $item->productoaddId; ?>);"href="#">Eliminar</a>
                    <?php } ?>
                  </div>
              </div>
              </td>
            </tr>
        <?php }
    }
    public function searchpres(){
        $usu = $this->input->get('search');
        $results=$this->ModeloProductos->presentallsearch($usu);
        echo json_encode($results->result());
    }
    public function mermaadd(){
        $id = $this->input->post('id');
        $marca = $this->input->post('marca'); //tipo merma
        $press = $this->input->post('press'); //presentacion
        $cant = $this->input->post('cant');
        echo $this->ModeloProductos->addmerma($id,$marca,$press,$cant);
    }
    public function traspasos(){
        $id = $this->input->post('id');
        $origen = $this->input->post('origen');
        $destino = $this->input->post('destino');
        $cant = $this->input->post('cant');
        $this->ModeloProductos->traspasos3($id,$origen,$cant);//disminulle
        $this->ModeloProductos->traspasos2($id,$destino,$cant);//aumenta
    }
    
    public function productosall() {
        $params = $this->input->post();

        $clt = $this->ModeloProductos->getProd($params);
        $totalRecords=$this->ModeloProductos->total_prod($params); 

        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($totalRecords),  
            "recordsFiltered" => intval($totalRecords),
            "data"            => $clt->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    } 

    public function Reporte(){
        $this->load->view('templates/header');
        $this->load->view('templates/navbar');
        $this->load->view('Reportes/productos');
        $this->load->view('templates/footer');
        $this->load->view('Reportes/productosjs');
    }

    public function get_prod_rep(){
        $datas = $this->ModeloProductos->allProductos();
        $json_data = array("data" => $datas);
        echo json_encode($json_data);
    }
}
