<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('America/Mexico_City');
class ListaVentas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('ModeloVentas');
    }
    public function index(){
            $tipo="";
            if(isset($_SESSION['bodega_tz'])){
                $bodega = $_SESSION['bodega_tz'];
            }
            else{
                $bodega=0;
            }
            if (isset($_GET['bod'])) {
                $bodega=$_GET['bod'];
            }
            if (isset($_GET['tipo'])) {
                $tipo=$_GET['tipo'];
            }

            $pages=10; //Número de registros mostrados por páginas
            $this->load->library('pagination'); //Cargamos la librería de paginación
            $config['base_url'] = base_url().'ListaVentas/view/'; // parametro base de la aplicación, si tenemos un .htaccess nos evitamos el index.php
            $config['total_rows'] = $this->ModeloVentas->filas($bodega,$tipo);//calcula el número de filas
            $config['per_page'] = $pages; //Número de registros mostrados por páginas  
            $config['num_links'] = 3; //Número de links mostrados en la paginación
            $config['first_link'] = 'Primera';//primer link
            $config['last_link'] = 'Última';//último link
            $config["uri_segment"] = 3;//el segmento de la paginación
            $config['next_link'] = 'Siguiente';//siguiente link
            $config['prev_link'] = 'Anterior';//anterior link
            $config['reuse_query_string'] = TRUE;// para mantener los parametros del get
            $this->pagination->initialize($config); //inicializamos la paginación 
            $pagex = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $data["ventas"] = $this->ModeloVentas->total_paginados($pagex,$config['per_page'],$bodega,$tipo);
            
            $this->load->view('templates/header');
            $this->load->view('templates/navbar');
            //$this->load->view('Personal/Personal',$data);
            $this->load->view('ventas/listaventas',$data);
            $this->load->view('templates/footer');
    }
    function cancelarventa(){
        $id = $this->input->post('id');
        $this->ModeloVentas->cancalarventa($id);

        $resultado=$this->ModeloVentas->ventadetalles($id);
        foreach ($resultado->result() as $item){
            $this->ModeloVentas->regresarpro($item->id_producto,$item->cantidad);
        }
    }

    public function consultaDetalleVenta($id)
    {
        $data=$this->ModeloVentas->detallesVentas($id);
        echo "<table id='tabla_temp_detventa' class='table table-bordered table-striped table-hover js-basic-example dataTable'>
                 <thead>
                   <tr>
                   <th class=''>Cantidad</th>
                   <th class=''>Producto</th>
                   <th class=''>Precio</th>
                   <th class=''>Cancelar</th>
                   </tr>
                 </thead>
                 <tbody>";
        foreach ($data->result() as $row){
            echo "<tr>";
            echo "<td class=''>".$row->cantidad." kg</td>";
            echo "<td class=''>".$row->categoria.' '.$row->marca.' - '.$row->presentacion."</td>";
            echo "<td class=''>$ ".number_format($row->cantidad*$row->precio,2,'.',',')."</td>";
            if($row->status_vd==1){
                echo "<td class=''><button class='btn btn-raised gradient-flickr white sidebar-shadow' onclick='cancelaParcial($row->id_detalle_venta)'> Cancelar</button></td>";
            }
            else{
               echo "<td class='btn btn-raised gradient-flickr white sidebar-shadow'>Cancelado</td>";  
            }
            echo "</tr>";
        }
        echo "</tbody>
            </table>";
    }

    function buscarvent(){
        $buscar = $this->input->post('buscar');
        $bode = $this->input->post('bode');
        $resultado=$this->ModeloVentas->ventassearch($buscar,$bode);
        foreach ($resultado->result() as $item){ ?>
            <tr id="trven_<?php echo $item->id_venta; ?>">
                  <td><?php echo $item->id_venta; ?></td>
                  <td><?php echo $item->reg; ?></td>
                  <td><?php echo $item->vendedor; ?></td>
                  <td>$ <?php echo number_format($item->monto_total,2,'.',',') ; ?></td>
                  <td><?php echo $item->metodo; ?></td>
                  <td><?php echo $item->cliente; ?></td>
                  <td><?php if($item->cancelado==1){ echo '<span class="badge badge-danger">Cancelado</span>';}
                    if($item->cancelado==2){ echo '<span class="badge badge-danger">Cancelado Parcial</span>';} ?>
                        
                  </td>
                  <td>
                    <button class="btn btn-raised gradient-blackberry white sidebar-shadow" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                      <i class="fa fa-book"></i>
                    </button>
                    <button class="btn btn-raised gradient-flickr white sidebar-shadow" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo $item->monto_total; ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                      <i class="fa fa-times"></i>
                    </button>
                    <button class="btn btn-success success-flickr white" onclick="detalles(<?php echo $item->id_venta; ?>)" title="detalles" data-placement="top" data-toggle="tooltip" data-target="#detalles_reparto" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                      <i class="fa fa-eye"></i>
                    </button>
                    <button class="btn btn-success success-flickr white" onclick="pagarParcial(<?php echo $item->id_venta; ?>)" title="Abonar Pago" data-placement="top" data-toggle="tooltip" data-target="#abonar_pago" <?php if($item->cancelado==1 || $item->tipo_metodo!=4){ echo 'disabled';} ?>>
                      <i class="fa fa-usd"></i>
                    </button>
                  </td>
          </tr>
        <?php }
    }

    public function cancelaParcial($id_vd,$id_v)
    {
        //$this->db->trans_start();
        $this->ModeloVentas->cancelaParcial($id_vd); //cancela la venta detalle status=0 ok 
        $this->ModeloVentas->cancelarventa2($id_v/*,$this->personal*/);//cancela venta parcial ok 

        $obt = $this->ModeloVentas->detallesVentasId($id_vd);
        foreach ($obt->result() as $row){
            $cant = $row->cantidad;
            $bodega = $row->bodega;
            $id_producto = $row->id_producto;
        }
        $this->ModeloVentas->update_stock($id_producto,$cant,$bodega); // modifica el inventario, regresa prod cancelado a stock 

        /*$this->db->trans_complete();
        echo $this->db->trans_status();*/
    }

    public function detalles_pedido(){
      $params = $this->input->post();
      $records = $this->ModeloVentas->get_venta_det($params);
      $json_data = array("data" => $records);
      echo json_encode($records);
    }
    
    public function exportarVentas($tipo,$sucur){
      $data['info']=$this->ModeloVentas->dataExport($sucur,$tipo);
      $this->load->view('ventas/ventasExcel',$data);
    }
}
