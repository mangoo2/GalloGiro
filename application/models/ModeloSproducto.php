<?php

class ModeloSproducto extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function getsproducto_by_productoId($idMarca){
		$strq = "SELECT * FROM sproducto where MarcaId=$idMarca";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}

	public function getsproductosub_by_productoId($productoaddId){
		$strq = "SELECT * FROM sproductosub where productoaddId=$productoaddId";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}


}
