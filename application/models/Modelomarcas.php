<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class Modelomarcas extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function getListadoMarcas()
    {
        $sql = "SELECT * FROM marca";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getMarcaPorId($idMarca)
    {
        $sql = "SELECT * FROM marca WHERE marcaid=".$idMarca;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function insertMarca($data) 
    {
        $this->db->insert('marca', $data);
        return $this->db->insert_id();
    }
    
    public function updateMarca($data, $idMarca) 
    {
        $this->db->set($data);
        $this->db->where('marcaid', $idMarca);
        return $this->db->update('marca');
    }
}