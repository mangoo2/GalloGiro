<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class modeloProductos extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->bodega=$_SESSION['bodega_tz'];
	}

	function getProd($params){
        $columns = array( 
            0=>'spro.productoaddId', 
            1=>"pro.categoria",
            2=>'pro.img',
            3=>'mar.marca',
            4=>'pro.categoriaId',
            5=>'spros.stok',
            6=>'spros.stok2',
            7=>'spros.stok3',
            8=>'spros.stok4',
            9=>'spros.stok5',
            10=>'spros.stok6'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('sproducto spro');
        $this->db->join('sproductosub spros', 'spros.productoaddId=spro.productoaddId and spros.tipo=1',"left");
        $this->db->join('categoria pro', 'pro.categoriaId=spro.productoId');
        $this->db->join('marca mar', 'mar.marcaid=spro.MarcaId');

        $this->db->where("spro.activo",1);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function total_prod($params){
        $columns = array( 
            0=>'spro.productoaddId', 
            1=>"pro.categoria",
            2=>'pro.img',
            3=>'mar.marca',
            4=>'pro.categoriaId',
            5=>'spros.stok',
            6=>'spros.stok2',
            7=>'spros.stok3',
            8=>'spros.stok4',
            9=>'spros.stok5',
            10=>'spros.stok6'
        );
        
        $this->db->select('COUNT(*) as total');
        $this->db->from('sproducto spro');
        $this->db->join('sproductosub spros', 'spros.productoaddId=spro.productoaddId and spros.tipo=1',"left");
        $this->db->join('categoria pro', 'pro.categoriaId=spro.productoId');
        $this->db->join('marca mar', 'mar.marcaid=spro.MarcaId');

        $this->db->where("spro.activo",1);

        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            
        //$this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        ///$this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query->row()->total;
    }

	function filas() {
		$strq = "SELECT COUNT(*) as total FROM sproducto where activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		foreach ($query->result() as $row) {
			$total =$row->total;
		}
		return $total;
	}
	function total_paginados($por_pagina,$segmento) {
		//$consulta = $this->db->get('productos',$por_pagina,$segmento);
		//return $consulta;
		if ($segmento!='') {
			$segmento=','.$segmento;
		}else{
			$segmento='';
		}
		$strq = "SELECT spro.productoaddId,pro.categoria,pro.img, mar.marca,pro.categoriaId,spros.stok,spros.stok2,spros.stok3,
                spros.stok4,spros.stok5,spros.stok6
                FROM sproducto as spro
                left join sproductosub as spros on spros.productoaddId=spro.productoaddId and spros.tipo=1
                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
                inner join marca as mar on mar.marcaid=spro.MarcaId
                where spro.activo=1
                LIMIT $por_pagina $segmento";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function productosall() {
		$strq = "SELECT spro.productoaddId,pro.categoria,pro.img, mar.marca,pro.categoriaId,spros.stok,spros.stok2,spros.stok3
                FROM sproducto as spro
                left join sproductosub as spros on spros.productoaddId=spro.productoaddId and spros.tipo=1
                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
                inner join marca as mar on mar.marcaid=spro.MarcaId
                where spro.activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function total_paginadosp($por_pagina,$segmento) {
		//$consulta = $this->db->get('productos',$por_pagina,$segmento);
		//return $consulta;
		if ($segmento!='') {
			$segmento=','.$segmento;
		}else{
			$segmento='';
		}
		$strq = "SELECT pro.productoid,pro.nombre,pro.img,pro.codigo FROM productos as pro
        inner join categoria as cat on cat.categoriaId=pro.categoria 
        where pro.activo=1 LIMIT $por_pagina $segmento";
		return $strq;
	}
	function productoallsearch($pro){
		$strq = "SELECT * FROM(
		    SELECT spro.productoaddId,pro.categoria,pro.img, mar.marca,pro.categoriaId,spros.stok,spros.stok2,spros.stok3,spros.subId
		                FROM sproducto as spro
		                left join sproductosub as spros on spros.productoaddId=spro.productoaddId and spros.tipo=1
		                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
		                inner join marca as mar on mar.marcaid=spro.MarcaId
		                where spro.activo=1
		) as prod
		WHERE prod.categoria LIKE concat('%".$pro."%')";
		//$strq = "CALL SP_GET_SEARCHPRODUCTO('$pro')";
		$query = $this->db->query($strq);
		return $query;
	}
	function categorias() {
		$strq = "SELECT * FROM categoria where activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	public function productosinsert($prod,$marca,$compra,$codigoProducto){
		$strq = "INSERT INTO sproducto(`productoId`, `MarcaId`,precompra,codigoProducto) VALUES ($prod,$marca,'$compra',$codigoProducto)";
		$this->db->query($strq);
		$id=$this->db->insert_id();
		return $id;
	}
	public function productosupdate($id,$prod,$marca,$compra,$codigoProducto){
		$strq = "UPDATE sproducto SET productoId='$prod',MarcaId=$marca,precompra='$compra',codigoProducto='$codigoProducto' where productoaddId=$id";
		$this->db->query($strq);
	}
	function productoaddetalleinser($idpro,$presentacion,$cantidad,$precio,$apartirmm,$prepreciomm,$apartirm,$prepreciom,$tipo,$cantidad2,$precio2,$apartirmm2,$prepreciomm2,$apartirm2,$prepreciom2,$cantidad3,$precio3,$apartirmm3,$prepreciomm3,$apartirm3,$prepreciom3,$cantidad4,$precio4,$apartirmm4,$prepreciomm4,$apartirm4,$prepreciom4,$cantidad5,$precio5,$apartirmm5,$prepreciomm5,$apartirm5,$prepreciom5,$cantidad6,$precio6,$apartirmm6,$prepreciomm6,$apartirm6,$prepreciom6)
	{
		$strq = "INSERT INTO sproductosub(productoaddId, PresentacionId, tipo, precio, stok, precio_mm, cantidad_mm, precio_m, cantidad_m,precio2,stok2, precio_mm2, cantidad_mm2, precio_m2, cantidad_m2,precio3,stok3, precio_mm3, cantidad_mm3, precio_m3, cantidad_m3,precio4,stok4, precio_mm4, cantidad_mm4, precio_m4, cantidad_m4,precio5,stok5, precio_mm5, cantidad_mm5, precio_m5, cantidad_m5, precio6,stok6, precio_mm6, cantidad_mm6, precio_m6, cantidad_m6) 
        VALUES ($idpro,$presentacion,$tipo,'$precio','$cantidad','$prepreciomm','$apartirmm','$prepreciom','$apartirm','$precio2','$cantidad2','$prepreciomm2','$apartirmm2','$prepreciom2','$apartirm2','$precio3','$cantidad3','$prepreciomm3','$apartirmm3','$prepreciom3','$apartirm3','$precio4','$cantidad4','$prepreciomm4','$apartirmm4','$prepreciom4','$apartirm4','$precio5','$cantidad5','$prepreciomm5','$apartirmm5','$prepreciom5','$apartirm5','$precio6','$cantidad6','$prepreciomm6','$apartirmm6','$prepreciom6','$apartirm6')";

        /*$strq = "INSERT INTO sproductosub(productoaddId, PresentacionId, tipo, precio, stok, precio_mm, cantidad_mm, precio_m, cantidad_m,precio2,stok2, precio_mm2, cantidad_mm2, precio_m2, cantidad_m2,precio3,stok3, precio_mm3, cantidad_mm3, precio_m3, cantidad_m3) 
        VALUES ($idpro,$presentacion,$tipo,'$precio','$cantidad','$prepreciomm','$apartirmm','$prepreciom','$apartirm','$precio2','$cantidad2','$prepreciomm2','$apartirmm2','$prepreciom2','$apartirm2','$precio3','$cantidad3','$prepreciomm3','$apartirmm3','$prepreciom3','$apartirm3')";*/
		$this->db->query($strq);
		return $strq;
	}
	function productoaddetalleupdate($idp,$idpro,$presentacion,$cantidad,$precio,$apartirmm,$prepreciomm,$apartirm,$prepreciom,$tipo,$cantidad2,$precio2,$apartirmm2,$prepreciomm2,$apartirm2,$prepreciom2,$cantidad3,$precio3,$apartirmm3,$prepreciomm3,$apartirm3,$prepreciom3,$cantidad4,$precio4,$apartirmm4,$prepreciomm4,$apartirm4,$prepreciom4,$cantidad5,$precio5,$apartirmm5,$prepreciomm5,$apartirm5,$prepreciom5,$cantidad6,$precio6,$apartirmm6,$prepreciomm6,$apartirm6,$prepreciom6){
		$strq = "UPDATE sproductosub 
                SET productoaddId=$idpro,PresentacionId=$presentacion,tipo=$tipo,
                    precio='$precio',stok='$cantidad',precio_mm='$prepreciomm',cantidad_mm='$apartirmm',precio_m='$prepreciom',cantidad_m='$apartirm',
                    precio2='$precio2',stok2='$cantidad2',precio_mm2='$prepreciomm2',cantidad_mm2='$apartirmm2',precio_m2='$prepreciom2',cantidad_m2='$apartirm2',
                    precio3='$precio3',stok3='$cantidad3',precio_mm3='$prepreciomm3',cantidad_mm3='$apartirmm3',precio_m3='$prepreciom3',cantidad_m3='$apartirm3',
                    precio4='$precio4',stok4='$cantidad4',precio_mm4='$prepreciomm4',cantidad_mm4='$apartirmm4',precio_m4='$prepreciom4',cantidad_m4='$apartirm4',
                    precio5='$precio5',stok5='$cantidad5',precio_mm5='$prepreciomm5',cantidad_mm5='$apartirmm5',precio_m5='$prepreciom5',cantidad_m5='$apartirm5',
                    precio6='$precio6',stok6='$cantidad6',precio_mm6='$prepreciomm6',cantidad_mm6='$apartirmm6',precio_m6='$prepreciom6',cantidad_m6='$apartirm6'
                    WHERE subId=$idp";
        /*$strq = "UPDATE sproductosub 
                SET productoaddId=$idpro,PresentacionId=$presentacion,tipo=$tipo,
                    precio='$precio',stok='$cantidad',precio_mm='$prepreciomm',cantidad_mm='$apartirmm',precio_m='$prepreciom',cantidad_m='$apartirm',
                    precio2='$precio2',stok2='$cantidad2',precio_mm2='$prepreciomm2',cantidad_mm2='$apartirmm2',precio_m2='$prepreciom2',cantidad_m2='$apartirm2',
                    precio3='$precio3',stok3='$cantidad3',precio_mm3='$prepreciomm3',cantidad_mm3='$apartirmm3',precio_m3='$prepreciom3',cantidad_m3='$apartirm3'
                    WHERE subId=$idp";*/
		$this->db->query($strq);
		return $strq;
	}
	public function imgpro($file,$pro){
		$strq = "UPDATE productos SET img='$file' WHERE productoid=$pro";
		$this->db->query($strq);
	}
	function totalproductosenexistencia() {
		$strq = "SELECT ROUND(sum(spro.stok+spro.stok2+spro.stok3),2) as total 
                 FROM sproductosub as spro
                 inner join sproducto as pro on spro.productoaddId=pro.productoaddId and pro.activo=1
                 where spro.tipo=1
                 ";
		$query = $this->db->query($strq);
		$this->db->close();
		foreach ($query->result() as $row) {
			$total =$row->total;
		}
		return $total;
	}
	function totalproductopreciocompra() {
		$strq = "SELECT ROUND(sum(preciocompra),2) as total FROM `productos` where activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		foreach ($query->result() as $row) {
			$total =$row->total;
		}
		return $total;
	}
	function totalproductoporpreciocompra(){
		$strq = "SELECT ROUND(sum(preciocompra*stock),2) as total FROM `productos` where activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		foreach ($query->result() as $row) {
			$total =$row->total;
		}
		return $total;
	}
	function getproducto($id){
		$strq = "SELECT * FROM sproducto where productoaddId=$id";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function getproducto2($id){
		$strq = "SELECT spro.*, pro.categoria, spros.*, mar.marca, pro.categoria
                FROM sproducto as spro
                left join sproductosub as spros on spros.productoaddId=spro.productoaddId
                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
                inner join marca as mar on mar.marcaid=spro.MarcaId
                where spro.productoaddId=$id";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	
	function getproductodll($id,$tipo){
		$strq = "SELECT * 
                FROM sproductosub as spro
                inner join presentaciones as pre on pre.presentacionId=spro.PresentacionId
                where spro.productoaddId=$id and spro.tipo=$tipo";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function productosdelete($id){
		$strq = "UPDATE sproducto SET activo=0 WHERE productoaddId=$id";
		$this->db->query($strq);
	}
	function presentallsearch($text){
		$strq = "SELECT * FROM presentaciones where activo=1 and presentacion like '%".$text."%' ORDER BY presentacion ASC";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function getsproductosub($id){
		$strq = "SELECT * FROM sproductosub where subId=$id";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	//                               tipo merma, presentacion
	function addmerma($productoaddId,$marca,$PresentacionId,$cantidad){
		//==============================================================
		$strq5 = "SELECT * FROM sproducto where productoId=25 and MarcaId=$marca";
		$query5 = $this->db->query($strq5);
		$this->db->close();
		$vid=0;
		foreach ($query5->result() as $row) {
			$vid =$row->productoaddId;
			$strq8 = "UPDATE sproducto SET activo=1 WHERE productoaddId=$vid";
			$query8 = $this->db->query($strq8);
			$this->db->close();
		}
		if ($vid==0) {
			$strq = "INSERT INTO sproducto(`productoId`, `MarcaId`) VALUES (25,$marca)";
			$this->db->query($strq);
			$idmerma=$this->db->insert_id();
			$this->db->close();
			//=============================================
			$strqx = "INSERT INTO sproductosub(productoaddId, PresentacionId, tipo, precio, stok, precio_mm, cantidad_mm, precio_m, cantidad_m) VALUES ($idmerma,$PresentacionId,1,0,$cantidad,0,0,0,0)";
			$this->db->query($strqx);
			$this->db->close();
		}else{
			$idmerma=$vid;
			//==============================================
			$strqz = "SELECT * FROM sproductosub where productoaddId=$idmerma and PresentacionId=$PresentacionId";
			$queryz = $this->db->query($strqz);
			$this->db->close();
			$vids=0;
			foreach ($queryz->result() as $row) {
				$vids =$row->subId;
			}
			if ($vids==0) {
				$strq = "INSERT INTO sproductosub(productoaddId, PresentacionId, tipo, precio, stok, precio_mm, cantidad_mm, precio_m, cantidad_m) VALUES ($idmerma,$PresentacionId,1,0,$cantidad,0,0,0,0)";
				$this->db->query($strq);
			}else{
				$strq = "UPDATE sproductosub SET stok=stok+$cantidad WHERE subId=$vids";
				$this->db->query($strq);


			}

		}











		//====================================
		$strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
		$query2 = $this->db->query($strq2);
		$this->db->close();
		$unidad=1;
		foreach ($query2->result() as $row) {
			$unidad =$row->unidad;
		}
		//===================================
		$tounidad=$unidad*$cantidad;
		//================================
		$strq4 = "UPDATE sproductosub SET stok=stok-$tounidad WHERE productoaddId=$productoaddId and tipo=1";
		$query4 = $this->db->query($strq4);
		$this->db->close();
		//===============================================


		$strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=1";
		$query3 = $this->db->query($strq3);
		$this->db->close();
		foreach ($query3->result() as $row) {
			$stok =$row->stok;
		}

		$strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=0";
		$query3 = $this->db->query($strq3);
		$this->db->close();
		foreach ($query3->result() as $row) {
			$subId =$row->subId;
			$PresentacionId =$row->PresentacionId;

			$strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
			$query2 = $this->db->query($strq2);
			$this->db->close();
			$unidad=1;
			foreach ($query2->result() as $row) {
				$unidad =$row->unidad;
			}
			$newstok=$stok/$unidad;

			$strq4 = "UPDATE sproductosub SET stok=$newstok WHERE subId=$subId and tipo=0";
			$query4 = $this->db->query($strq4);
			$this->db->close();
		}

	}
	function traspasos($producto,$origen,$cantidad){
		if ($origen=='2') {
			$stock='stok2';
		}elseif($origen=='3') {
			$stock='stok3';
		}else{
			$stock='stok';
		}
		//===================================
		$strq1 = "SELECT * FROM sproductosub where subId=$producto";
		$query1 = $this->db->query($strq1);
		$this->db->close();
		$PresentacionId=0;
		$productoaddId=0;
		foreach ($query1->result() as $row) {
			$PresentacionId =$row->PresentacionId;
			$productoaddId =$row->productoaddId;
		}
		//====================================
		$strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
		$query2 = $this->db->query($strq2);
		$this->db->close();
		$unidad=1;
		foreach ($query2->result() as $row) {
			$unidad =$row->unidad;
		}
		//===================================
		$tounidad=$unidad*$cantidad;
		//================================
		$strq4 = "UPDATE sproductosub SET $stock=$stock-$tounidad WHERE productoaddId=$productoaddId and tipo=1";
		$query4 = $this->db->query($strq4);
		$this->db->close();
		//===============================================


		$strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=1";
		$query3 = $this->db->query($strq3);
		$this->db->close();
		foreach ($query3->result() as $row) {
			if ($origen=='2') {
				$stok =$row->stok2;
			}elseif ($origen=='3') {
				$stok =$row->stok3;
			}else{
				$stok =$row->stok;
			}
		}

		$strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=0";
		$query3 = $this->db->query($strq3);
		$this->db->close();
		foreach ($query3->result() as $row) {
			$subId =$row->subId;
			$PresentacionId =$row->PresentacionId;

			$strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
			$query2 = $this->db->query($strq2);
			$this->db->close();
			$unidad=1;
			foreach ($query2->result() as $row) {
				$unidad =$row->unidad;
			}
			$newstok=$stok/$unidad;

			$strq4 = "UPDATE sproductosub SET $stock=$newstok WHERE subId=$subId and tipo=0";
			$query4 = $this->db->query($strq4);
			$this->db->close();
		}
		return $strq4;
	}
	function traspasos2($producto,$destino,$cantidad){
		if ($destino=='2') {
			$stock='stok2';
		}elseif($destino=='3') {
			$stock='stok3';
		}elseif($destino=='4') {
			$stock='stok4';
		}elseif($destino=='5') {
			$stock='stok5';}
			elseif($destino=='6') {
				$stock='stok6';
		}else{
			$stock='stok';
		}
		$strq = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		$subId=0;
		foreach ($query->result() as $row) {
			$subId =$row->subId;
			$PresentacionId = $row->PresentacionId;
		}
		if ($subId>0) {
			//============================================================================
			$strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
			$query2 = $this->db->query($strq2);
			$this->db->close();
			$unidad=1;
			foreach ($query2->result() as $row) {
				$unidad =$row->unidad;
			}
			//===================================
			$tounidad=$unidad*$cantidad;
			//================================
			$strq4 = "UPDATE sproductosub SET $stock=$stock+$tounidad WHERE productoaddId=$producto and tipo=1";
			$query4 = $this->db->query($strq4);
			$this->db->close();
			//===============================================
			//===============================================
			$strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
			$query3 = $this->db->query($strq3);
			$this->db->close();
			foreach ($query3->result() as $row) {
				if ($destino=='2') {
					$stok =$row->stok2;
				}elseif ($destino=='3') {
					$stok =$row->stok3;
				}elseif ($destino=='4') {
					$stok =$row->stok4;
				}elseif ($destino=='5') {
					$stok =$row->stok5;
				}elseif ($destino=='6') {
					$stok =$row->stok6;
				}else{
					$stok =$row->stok;
				}
			}

			$strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=0";
			$query3 = $this->db->query($strq3);
			$this->db->close();
			foreach ($query3->result() as $row) {
				$subId =$row->subId;
				$PresentacionId =$row->PresentacionId;

				$strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
				$query2 = $this->db->query($strq2);
				$this->db->close();
				$unidad=1;
				foreach ($query2->result() as $row) {
					$unidad =$row->unidad;
				}
				$newstok=$stok/$unidad;

				$strq4 = "UPDATE sproductosub SET $stock=$newstok WHERE subId=$subId and tipo=0";
				$query4 = $this->db->query($strq4);
				$this->db->close();
			}
		}
	}
	function traspasos3($producto,$destino,$cantidad){
		if ($destino=='2') {
			$stock='stok2';
		}elseif($destino=='3') {
			$stock='stok3';
		}elseif($destino=='4') {
			$stock='stok4';
		}elseif($destino=='5') {
			$stock='stok5';
		}elseif($destino=='6') {
			$stock='stok6';
		}else{
			$stock='stok';
		}
		$strq = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
		$query = $this->db->query($strq);
		//$this->db->close();
		$subId=0;
		foreach ($query->result() as $row) {
			$subId =$row->subId;
			$PresentacionId = $row->PresentacionId;
		}
		if ($subId>0) {
			//============================================================================
			$strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
			$query2 = $this->db->query($strq2);
			//$this->db->close();
			$unidad=1;
			foreach ($query2->result() as $row) {
				$unidad =$row->unidad;
			}
			//===================================
			$tounidad=$unidad*$cantidad;
			//================================
			$strq4 = "UPDATE sproductosub SET $stock=$stock-$tounidad WHERE productoaddId=$producto and tipo=1";
			$query4 = $this->db->query($strq4);
			//$this->db->close();
			//===============================================
			//===============================================
			$strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1";
			$query3 = $this->db->query($strq3);
			//$this->db->close();
			foreach ($query3->result() as $row) {
				if ($destino=='2') {
					$stok =$row->stok2;
				}elseif ($destino=='3') {
					$stok =$row->stok3;
				}elseif ($destino=='4') {
					$stok =$row->stok4;
				}elseif ($destino=='5') {
					$stok =$row->stok5;
				}elseif ($destino=='6') {
					$stok =$row->stok6;
				}else{
					$stok =$row->stok;
				}
			}

			$strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=0";
			$query3 = $this->db->query($strq3);
			//$this->db->close();
			foreach ($query3->result() as $row) {
				$subId =$row->subId;
				$PresentacionId =$row->PresentacionId;

				$strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId";
				$query2 = $this->db->query($strq2);
				//$this->db->close();
				$unidad=1;
				foreach ($query2->result() as $row) {
					$unidad =$row->unidad;
				}
				$newstok=$stok/$unidad;

				$strq4 = "UPDATE sproductosub SET $stock=$newstok WHERE subId=$subId and tipo=0";
				$query4 = $this->db->query($strq4);
				//$this->db->close();
			}
		}
	}
	function deleterowv($id){
		$strq2 = "SELECT * FROM venta_detalle where id_producto=$id";
		$query2 = $this->db->query($strq2);
		$this->db->close();
		$row=0;
		foreach ($query2->result() as $row) {
			$row=1;
		}
		return $row;
	}
	function deleterow($id){
		$strq2 = "DELETE FROM sproductosub WHERE subId=$id";
		$query2 = $this->db->query($strq2);
		$this->db->close();
	}


	// Obtiene los datos del producto en base al codigo de Producto
	function getDatosProducto($codigoProducto)
	{
		$strq = "SELECT
                spro.*,
                pro.categoria
                FROM sproducto as spro
                left join sproductosub as spros on spros.productoaddId=spro.productoaddId and spros.tipo=1
                inner JOIN categoria as pro on pro.categoriaId=spro.productoId
                inner join marca as mar on mar.marcaid=spro.MarcaId
                WHERE codigoProducto = '".$codigoProducto."'";
		$query = $this->db->query($strq);
		return $query->row();
	}

	function allProductos(){
        $columns = array( 
            0=>'spro.productoaddId', 
            1=>"pro.categoria",
            2=>'pro.img',
            3=>'mar.marca',
            4=>'pro.categoriaId',
            5=>'spros.stok',
            6=>'spros.stok2',
            7=>'spros.stok3',
            8=>'spros.stok4',
            9=>'spros.stok5',
            10=>'spros.stok6'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('sproducto spro');
        $this->db->join('sproductosub spros', 'spros.productoaddId=spro.productoaddId and spros.tipo=1',"left");
        $this->db->join('categoria pro', 'pro.categoriaId=spro.productoId');
        $this->db->join('marca mar', 'mar.marcaid=spro.MarcaId');

        $this->db->where("spro.activo",1);
        $query=$this->db->get();
        return $query->result();
    }
}
