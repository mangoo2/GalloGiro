<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloReportes extends CI_Model {
    public function __construct(){
        parent::__construct();
    }

    function getReporteEdos($params){
        $this->db->select("v.*,vcp.id, vcp.fecha_pago, vcp.folio_pago, vcp.cantidad, vcp.observaciones, vcp.fecha_reg, vcp.estatus, cli.Nom,
            (select sum(vcp.cantidad) from ventas_credito_pago vcp where vcp.estatus=1 and id_venta=v.id_venta) as total_pagos");
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd', 'vd.id_venta=v.id_venta');
        $this->db->join('ventas_credito_pago vcp', 'vcp.id_venta=v.id_venta',"left");
        $this->db->join('clientes cli', 'cli.ClientesId=v.id_cliente');
        if($params["cliente"]>0){
            $this->db->where('v.id_cliente',$params["cliente"]);
        }
        if($params["mercado"]>0){
            $this->db->where('cli.id_mercado',$params["mercado"]);
        }
        $this->db->where('v.reg BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
        $this->db->where('v.cancelado!=',1);
        //$this->db->where('v.tipo',1);
        $this->db->where('v.metodo',4);
        $this->db->where('vd.status',1);
        //$this->db->where('vcp.estatus',1);
        $this->db->order_by('v.id_venta','desc');
        $this->db->group_by('v.id_venta');
        $query=$this->db->get(); 
        return $query->result();
    }

    function getReporteEdos2($params){
        $this->db->select("v.*,vcp.id, vcp.fecha_pago, vcp.folio_pago, vcp.cantidad, vcp.observaciones, vcp.fecha_reg, vcp.estatus, cli.Nom");
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd', 'vd.id_venta=v.id_venta');
        $this->db->join('ventas_credito_pago vcp', 'vcp.id_venta=v.id_venta',"left");
        $this->db->join('clientes cli', 'cli.ClientesId=v.id_cliente');
        if($params["cliente"]>0){
            $this->db->where('v.id_cliente',$params["cliente"]);
        }
        $this->db->where('v.reg BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
        $this->db->where('v.cancelado!=',1);
        //$this->db->where('v.tipo',1);
        $this->db->where('v.metodo',4);
        $this->db->where('vd.status',1);
        //$this->db->where('vcp.estatus',1);
        $this->db->order_by('v.id_venta','desc');
        $this->db->group_by('v.id_venta');
        $this->db->group_by('vcp.id');
        $query=$this->db->get(); 
        return $query->result();
    }

    function getSumaVenta($params){
        $this->db->select("sum(vd.cantidad*vd.precio) as total_venta");
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd', 'vd.id_venta=v.id_venta');
        $this->db->join('clientes cli', 'cli.ClientesId=v.id_cliente');
        /*$this->db->where('v.id_cliente',$params["cliente"]);
        $this->db->where('v.reg BETWEEN '.'"'.$params["fechai"].' 00:00:00"'.' AND '.'"'.$params["fechaf"].' 23:59:59"');
        $this->db->where('v.cancelado!=',1);
        $this->db->where('v.metodo',4);*/
        $this->db->where('vd.status',1);
        $this->db->where('v.id_venta',$params["id"]);

        $query=$this->db->get(); 
        return $query->result();
    }

    public function get_ventas_prods($params){
        $columns = array( 
            0 => 'v.id_venta',
            1 => 'cat.categoria',
            2 => 'mar.marca',
            3 => 'vd.cantidad',
            4 => "vd.precio",
            5 => "v.metodo",
            6 => "sum(vd.cantidad) as totalc",
            7 => "sum(vd.cantidad*vd.precio) as total_prods"
        );
        $columns_s = array( 
            0 => 'v.id_venta',
            1 => 'cat.categoria',
            2 => 'mar.marca',
            3 => 'vd.cantidad',
            4 => "vd.precio",
            5 => "v.metodo",
            //6 => "sum(vd.cantidad*vd.precio)"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd','vd.id_venta=v.id_venta and vd.status=1');
        $this->db->join('sproductosub sprob','sprob.subId=vd.id_producto');
        $this->db->join('sproducto pro','pro.productoaddId=sprob.productoaddId');
        $this->db->join('categoria cat','cat.categoriaId=pro.productoId');
        $this->db->join('marca mar','mar.marcaid=pro.MarcaId');

        if($params["id_sucursal"]!="0"){
            $this->db->where('v.bodega',$params["id_sucursal"]);
        }
        $this->db->where('v.cancelado!=',1);
        $this->db->where("v.reg between '{$params['fechai']} 00:00:00' AND '{$params['fechaf']} 23:59:59' ");

        $this->db->group_by('vd.id_producto');
        $this->db->order_by('categoria','ASC');

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns_s as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns_s[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query->result();
    }

    public function get_ventas_prods_tot($params){ 
        $columns = array( 
            0 => 'v.id_venta',
            1 => 'cat.categoria',
            2 => 'mar.marca',
            3 => 'vd.cantidad',
            4 => "vd.precio",
            5 => "v.metodo",
        );
        $columns_s = array( 
            0 => 'v.id_venta',
            1 => 'cat.categoria',
            2 => 'mar.marca',
            3 => 'vd.cantidad',
            4 => "vd.precio",
            5 => "v.metodo",
            //6 => "sum(vd.cantidad*vd.precio)"
        );
        $this->db->select('count(*) as total');
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd','vd.id_venta=v.id_venta and vd.status=1');
        $this->db->join('sproductosub sprob','sprob.subId=vd.id_producto');
        $this->db->join('sproducto pro','pro.productoaddId=sprob.productoaddId');
        $this->db->join('categoria cat','cat.categoriaId=pro.productoId');
        $this->db->join('marca mar','mar.marcaid=pro.MarcaId');

        if($params["id_sucursal"]!="0"){
            $this->db->where('v.bodega',$params["id_sucursal"]);
        }
        $this->db->where('v.cancelado!=',1);
        $this->db->where("v.reg between '{$params['fechai']} 00:00:00' AND '{$params['fechaf']} 23:59:59' ");

        $this->db->group_by('vd.id_producto');

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns_s as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        return $this->db->count_all_results();
        /*$query=$this->db->get();
        return $query->row()->total;*/
    }

    /* ************************************************/
    public function get_pedidos_prods($params){
        $columns = array( 
            0 => 'v.id_venta',
            1 => 'cat.categoria',
            2 => 'mar.marca',
            3 => 'vd.cantidad',
            4 => "vd.precio",
            5 => "v.metodo",
            6 => "sum(vd.cantidad) as totalc",
            7 => "sum(vd.cantidad*vd.precio) as total_prods",
            8 => "c.Nom"
        );
        $columns_s = array( 
            0 => 'v.id_venta',
            1 => 'cat.categoria',
            2 => 'mar.marca',
            3 => 'vd.cantidad',
            4 => "vd.precio",
            5 => "v.metodo",
            //6 => "sum(vd.cantidad*vd.precio)"
            8 => "c.Nom"
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd','vd.id_venta=v.id_venta and vd.status=1');
        $this->db->join('sproductosub sprob','sprob.subId=vd.id_producto');
        $this->db->join('sproducto pro','pro.productoaddId=sprob.productoaddId');
        $this->db->join('categoria cat','cat.categoriaId=pro.productoId');
        $this->db->join('marca mar','mar.marcaid=pro.MarcaId');
        $this->db->join('clientes c','c.ClientesId=v.id_cliente');

        if($params["id_sucursal"]!="0"){
            $this->db->where('v.bodega',$params["id_sucursal"]);
        }
        $this->db->where('v.cancelado!=',1);
        if($params["id_cliente"]!="0"){
            $this->db->where('v.id_cliente',$params["id_cliente"]);
        }
        $this->db->where('v.tipo',2);
        $this->db->where("v.reg between '{$params['fechai']} 00:00:00' AND '{$params['fechaf']} 23:59:59' ");

        $this->db->group_by('vd.id_producto');
        $this->db->order_by('categoria','ASC');

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns_s as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns_s[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query->result();
    }

    public function get_pedidos_prods_tot($params){ 
        $columns = array( 
            0 => 'v.id_venta',
            1 => 'cat.categoria',
            2 => 'mar.marca',
            3 => 'vd.cantidad',
            4 => "vd.precio",
            5 => "v.metodo",
            6 => "sum(vd.cantidad) as totalc",
            8 => "c.Nom"
        );
        $columns_s = array( 
            0 => 'v.id_venta',
            1 => 'cat.categoria',
            2 => 'mar.marca',
            3 => 'vd.cantidad',
            4 => "vd.precio",
            5 => "v.metodo",
            //6 => "sum(vd.cantidad)",
            //7 => "sum(vd.cantidad*vd.precio)",
            8 => "c.Nom"
        );
        $this->db->select('count(*) as total');
        $this->db->from('ventas v');
        $this->db->join('venta_detalle vd','vd.id_venta=v.id_venta and vd.status=1');
        $this->db->join('sproductosub sprob','sprob.subId=vd.id_producto');
        $this->db->join('sproducto pro','pro.productoaddId=sprob.productoaddId');
        $this->db->join('categoria cat','cat.categoriaId=pro.productoId');
        $this->db->join('marca mar','mar.marcaid=pro.MarcaId');
        $this->db->join('clientes c','c.ClientesId=v.id_cliente');

        if($params["id_sucursal"]!="0"){
            $this->db->where('v.bodega',$params["id_sucursal"]);
        }
        $this->db->where('v.cancelado!=',1);
        if($params["id_cliente"]!="0"){
            $this->db->where('v.id_cliente',$params["id_cliente"]);
        }
        
        $this->db->where('v.tipo',2);
        $this->db->where("v.reg between '{$params['fechai']} 00:00:00' AND '{$params['fechaf']} 23:59:59' ");

        $this->db->group_by('vd.id_producto');

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns_s as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        return $this->db->count_all_results();
        /*$query=$this->db->get();
        return $query->row()->total;*/
    }


}  