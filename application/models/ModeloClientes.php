<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class Modeloclientes extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM clientes where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM clientes where activo=1 LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function clientesallsearch($usu){
        $strq = "SELECT * FROM clientes where activo=1 and Nom like '%".$usu."%' ORDER BY Nom ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    
    public function clientesinsert($data){
		if($this->db->insert("clientes", $data))
			return $this->db->insert_id();
		else
			return null;
    }
	public function clientesupdate($clientesId, $data){
		$this->db->where('ClientesId', $clientesId);
		$this->db->update("clientes", $data);
		return 1;
	}
    function getcliente($id){
        $strq = "SELECT * FROM clientes where ClientesId=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }

	function get_cliente($id){
		$this->db->select('clientes.*,m.nombre as mercado');
		$this->db->from("clientes");
        $this->db->join("mercados m","m.id=clientes.id_mercado","left");
		$this->db->where("ClientesId", $id);
		$consulta = $this->db->get();
		return $consulta->result();
	}
    function deleteclientes($id){
        $strq = "UPDATE clientes SET activo=0 WHERE ClientesId=$id";
        $this->db->query($strq);
    }

    function cliente_search($cli){
        $strq = "SELECT cli.*, cli.Nom as ciente 
        FROM clientes as cli
        where activo=1 and cli.Nom like '%".$cli."%'";
        $query = $this->db->query($strq);
        return $query;
    }
}
