<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGasto extends CI_Model 
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function insertarGasto($data) 
    {
        $this->db->insert('gastos', $data);
        return $this->db->insert_id();
    }

    public function VerTodasGastos()
    {
        $this->db->select('*');
        $this->db->from('gastos');
        $this->db->where("gastos.status",1);

        $query = $this->db->get();
        if($query->num_rows()> 0)
        {
          return $query;
        }
        else
        {
          return $query;  
        }
  }

    public function get_gastos($params){
        //$bodega = $this->session->userdata('bodega_tz');
        $bodega= $_SESSION['bodega_tz'];

        $columns = array( 
            0 => 'gastos.id',
            1 => 'gastos.descrip',
            2 => 'gastos.monto_gasto',
            3 => 'gastos.fecha_gasto'
        );
        $select="gastos.*";
        $this->db->select($select);
        $this->db->from('gastos');
        $this->db->join('usuarios','usuarios.UsuarioID=id_usuario');
        $this->db->join('personal','personal.personalId=usuarios.UsuarioID','left');  
        $this->db->where('personal.bodega',$bodega);
        $this->db->where('gastos.status',1);

        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        
        $query=$this->db->get();
        return $query;
    }

    public function get_no_gastos($params){ 
        $bodega= $_SESSION['bodega_tz'];
        $this->db->select('count(1)');
        $this->db->from('gastos');
        $this->db->join('usuarios','usuarios.UsuarioID=id_usuario');
        $this->db->join('personal','personal.personalId=usuarios.UsuarioID','left');  
        $this->db->where('personal.bodega',$bodega);
        $this->db->where('gastos.status',1);
        $columns = array( 
            0 => 'gastos.id',
            1 => 'gastos.descrip',
            2 => 'gastos.monto_gasto',
            3 => 'gastos.fecha_gasto'
        );
        //si hay busqueda con el campo de busqueda
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach ($columns as $c) {
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
        }
        return $this->db->count_all_results();
    }
    

}