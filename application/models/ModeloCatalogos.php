<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
	public function __construct() {
		parent::__construct();
		if (isset($_SESSION['bodega_tz'])) {
			$this->bodega=$_SESSION['bodega_tz'];
		}else{
			$this->bodega=0;
		}

	}
	//====================== categoria===================================
	function categorias_all() {
		/*if($_SESSION['perfilid_tz']!=1){
			if($_SESSION['bodega_tz']==1){
				$where = "and ";
			}
		}*/
		$strq = "SELECT * FROM categoria where activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function categoriadell($id){
		$strq = "UPDATE categoria SET activo=0 WHERE categoriaId=$id";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function categoriaadd($nom){
		$strq = "INSERT INTO categoria(categoria) VALUES ('$nom')";
		$query = $this->db->query($strq);
		$id=$this->db->insert_id();
		$this->db->close();

		return $id;
	}
	function categoriupdate($nom,$id){
		$strq = "UPDATE categoria SET categoria='$nom' WHERE categoriaId=$id";
		$query = $this->db->query($strq);
		$this->db->close();
	}
	function categoriaaddimg($img,$id){
		$strq = "UPDATE categoria SET img='$img' WHERE categoriaId=$id";
		$query = $this->db->query($strq);
		$this->db->close();
	}
	//=======================fin categoria================
	//====================== marcas===================================
	function marcas_all() {
		$strq = "SELECT * FROM marca where activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function marcadell($id){
		$strq = "UPDATE marca SET activo=0 WHERE marcaid=$id";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function marcaadd($nom){
		$strq = "INSERT INTO marca(marca) VALUES ('$nom')";
		$query = $this->db->query($strq);
		$id=$this->db->insert_id();
		$this->db->close();

		return $id;
	}
	function marcaupdate($nom,$id){
		$strq = "UPDATE marca SET marca='$nom' WHERE marcaid=$id";
		$query = $this->db->query($strq);
		$this->db->close();
	}
	function marcaaddimg($img,$id){
		$strq = "UPDATE marca SET imgm='$img' WHERE marcaid=$id";
		$query = $this->db->query($strq);
		$this->db->close();
	}
	//=======================fin marcas================
	function updatenota($use,$nota){
		$bodega= $_SESSION['bodega_tz'];
		$strq = "UPDATE notas SET mensaje='$nota',usuario='$use',reg=NOW() where bodega=$bodega";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function configticket(){
		$strq = "SELECT * FROM ticket";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function configticketupdate($titulo,$mensaje1,$mensaje2,$fuente,$tamano,$margsup,$sucursal){
		$strq = "UPDATE ticket SET titulo='$titulo',mensaje='$mensaje1',mensaje2='$mensaje2',fuente='$fuente',tamano='$tamano',margensup='$margsup' WHERE sucursal = $sucursal";
		$query = $this->db->query($strq);
		$this->db->close();
	}
	function marcashuevo_all($idp){
		$strq = "SELECT mar.imgm, mar.marca,mar.marcaid 
                FROM sproducto as pro 
                inner join marca as mar on mar.marcaid=pro.MarcaId 
                where pro.productoid=$idp and pro.activo=1
                group by mar.marcaid";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function presentacion_all($pro,$mar){
		if ($this->bodega=='2') {
			$stock='pros.stok2';
		}elseif($this->bodega=='3') {
			$stock='pros.stok3';
		}else{
			$stock='pros.stok';
		}
		$strq ="SELECT pre.presentacionId,pre.presentacion, pros.subId,pros.precio, 
                pros.precio2,pros.precio3 
                FROM sproducto as pro 
                /*inner join sproductosub as pros on pros.productoaddId=pro.productoaddId and $stock>=1 */
                inner join sproductosub as pros on pros.productoaddId=pro.productoaddId
                left join presentaciones as pre on pre.presentacionId=pros.PresentacionId 
                WHERE pro.productoId=$pro AND pro.MarcaId=$mar and pro.activo=1 
                ";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function searchcliente($search){
		$strq ="SELECT * FROM clientes WHERE activo=1 and Nom like '%$search%'";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}
	function getdescuento($id){
		$strq = "SELECT * FROM descuentos where bodega=$id";
		$query = $this->db->query($strq);
		$this->db->close();
		foreach ($query->result() as $row) {
			$activado =$row->activado;
		}
		return $activado;
	}
	function getdescuentoup($sta,$bod){
		$strq = "UPDATE descuentos set activado=$sta where bodega=$bod";
		$query = $this->db->query($strq);
		$this->db->close();
	}
	function metodopagos() {
		$strq = "SELECT * FROM metodopago where activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}


	/********************************* */
	public function GenInsert($data,$tabla){
		$this->db->insert($tabla,$data);
		return $this->db->insert_id();
	}
	public function insertToCatalogop($data, $catalogo) {
        $this->db->insert('' . $catalogo, $data);
        return $this->db->insert_id();
    }
	function GenUpdate($data,$table,$campo,$dato){
		$this->db->where($campo, $dato);
        $this->db->update($table,$data);
        return $this->db->affected_rows();
	}

	public function get_record($id,$table){
    	$this->db->select('*');
        $this->db->from($table);
        $this->db->where('presentacionId',$id);
		$query=$this->db->get();
		return $query->row();	
	}

	function GetAllActive($tabla){
		$sql="SELECT * FROM $tabla where activo=1";
		$query=$this->db->query($sql);
		return $query->result();	
	}

	function GetAllActiveEstatus($tabla){
		$sql="SELECT * FROM $tabla where estatus=1";
		$query=$this->db->query($sql);
		return $query->result();	
	}

	function getEdoPedido(){
		$sql="SELECT * FROM pedido_detalle left join personal on personal.personalId = id_repartidor where status=1";
		$query=$this->db->query($sql);
		return $query->result();	
	}

	function getTable($tabla,$sta){
		$this->db->select('*');
        $this->db->from($tabla);
        $this->db->where($sta,1);
		$query=$this->db->get();
		return $query->result();	
	}
	/*function table_all($table) {
		$strq = "SELECT * FROM $table where activo=1";
		$query = $this->db->query($strq);
		$this->db->close();
		return $query;
	}*/

	public function edit_record($id,$data,$table){
    	$this->db->set($data);
        $this->db->where('id_venta', $id);
        return $this->db->update($table);
    }

    public function updateCatalogon($data,$where,$table){
        $this->db->set($data);
        $this->db->where($where);/// Se ocupa para n select
        $this->db->update($table);
        
    }

    public function get_tableWhere($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_tableRow($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        return $query->row();
    }

    function search_mercado($merc){
        $strq = "SELECT *
        FROM mercados as m
        where m.status=1 and (m.nombre like '%".$merc."%' or 
        m.direc like '%".$merc."%')";
        $query = $this->db->query($strq);
        return $query;
    }
}
