<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloDomicilio_Cliente extends CI_Model
{
	public $tabla;

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->tabla = "domicilios_clietes";
	}

	public function insert($data)
	{
		if ($this->db->insert($this->tabla, $data))
			return true;
		else
			return null;
	}
	public function get_by_idCliente_activo($idCliente)
	{
		$this->db->select('*');
		$this->db->from($this->tabla);
		$this->db->where("idCliente", $idCliente);
		$this->db->where("status", 1);
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function get_dir_client($idCliente)
	{
		$this->db->select('c.*, dc.Calle as calledc, dc.noExterior as noExtdc,dc.noInterior as noIntdc, dc.Colonia as coloniadc');
		//$this->db->select('c.*');
		$this->db->from('clientes c');
		$this->db->join('domicilios_clietes dc','dc.idCliente=ClientesId');
		$this->db->where("c.ClientesId", $idCliente);
		//$this->db->where("status", 1);
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function eliminar_byId($idDomicilio){
		$this->db->delete('domicilios_clietes', array('idDomicilio' => $idDomicilio));
		return true;
	}

	public function update_by_idDomicilio($idDomicilio, $data){
		$this->db->where('idDomicilio', $idDomicilio);
		$this->db->update($this->tabla, $data);
		return 1;
	}
}
