<?php
$a=session_id();
if(empty($a)) session_start();
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProveedor extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function filas() {
        $strq = "SELECT COUNT(*) as total FROM proveedores where activo=1";
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function total_paginados($por_pagina,$segmento) {
        //$consulta = $this->db->get('productos',$por_pagina,$segmento);
        //return $consulta;
        if ($segmento!='') {
            $segmento=','.$segmento;
        }else{
            $segmento='';
        }
        $strq = "SELECT * FROM proveedores 
        where activo=1 LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    
    public function proveedorinsert($razonSocial,$domicilio,$ciudad,$cp,$estado,$contacto,$email,$rfc,$tel,$telcel,$fax,$obser,$pesoi,$pesof,$codi,$codf,$longitud_codigo1,$longitud_codigo2,$longitud_codigo3,$pesoi_2,$pesof_2,$codi_2,$codf_2,$pesoi_3,$pesof_3,$codi_3,$codf_3){
            $strq = "INSERT INTO proveedores(razon_social, domicilio, ciudad, cp,id_estado, telefono_local, telefono_celular, contacto, email_contacto, rfc, fax, obser,pesoi,pesof,codigoi,codigof,longitud_codigo1,longitud_codigo2,longitud_codigo3,pesoi_2,pesof_2,codigoi_2,codigof_2,pesoi_3,pesof_3,codigoi_3,codigof_3) VALUES ('$razonSocial','$domicilio','$ciudad','$cp','$estado','$tel','$telcel','$contacto','$email','$rfc','$fax','$obser','$pesoi','$pesof','$codi','$codf','$longitud_codigo1','$longitud_codigo2','$longitud_codigo3','$pesoi_2','$pesof_2','$codi_2','$codf_2','$pesoi_3','$pesof_3','$codi_3','$codf_3')";
            $this->db->query($strq);
            $id=$this->db->insert_id();
            return $id;
    }
    public function proveedorupdate($id,$razonSocial,$domicilio,$ciudad,$cp,$estado,$contacto,$email,$rfc,$tel,$telcel,$fax,$obser,$pesoi,$pesof,$codi,$codf,$longitud_codigo1,$longitud_codigo2,$longitud_codigo3,$pesoi_2,$pesof_2,$codi_2,$codf_2,$pesoi_3,$pesof_3,$codi_3,$codf_3){
            $strq = "UPDATE proveedores SET razon_social='$razonSocial',domicilio='$domicilio',ciudad='$ciudad',cp='$cp',id_estado='$estado',telefono_local='$tel',telefono_celular='$telcel',contacto='$contacto',email_contacto='$email',rfc='$rfc',fax='$fax',obser='$obser',pesoi='$pesoi',pesof='$pesof',codigoi='$codi',codigof='$codf',longitud_codigo1='$longitud_codigo1',longitud_codigo2='$longitud_codigo2',longitud_codigo3='$longitud_codigo3',pesoi_2='$pesoi_2',pesof_2='$pesof_2',codigoi_2='$codi_2',codigof_2='$codf_2',pesoi_3='$pesoi_3',pesof_3='$pesof_3',codigoi_3='$codi_3',codigof_3='$codf_3' WHERE id_proveedor=$id";
            $this->db->query($strq);
    }
    function getproveedor($id){
        $strq = "SELECT * FROM proveedores where id_proveedor=$id";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function deleteproveedors($id){
        $strq = "UPDATE proveedores SET activo=0 WHERE id_proveedor=$id";
        $this->db->query($strq);
    }
    function estados(){
        $strq = "SELECT * FROM estado";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
    function proveedorallsearch($usu){
        $strq = "SELECT * FROM proveedores where activo=1 and razon_social like '%".$usu."%' ORDER BY razon_social ASC";
        $query = $this->db->query($strq);
        $this->db->close();
        return $query;
    }
}