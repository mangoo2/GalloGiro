<?php 
$a=session_id(); 
if(empty($a)) session_start(); 
defined('BASEPATH') OR exit ('No direct script access allowed'); 
date_default_timezone_set('America/Mexico_City');
class ModeloVentas extends CI_Model { 
    public function __construct() { 
        parent::__construct(); 
        if (isset($_SESSION['bodega_tz'])) { 
            $this->bodega=$_SESSION['bodega_tz']; 
        }else{ 
            $this->bodega=0; 
        }    
    } 
 
    public function getDatosMarcaByLongitud($longitud_barcode) 
    { 
        $strq = "SELECT * FROM marca WHERE longitud_codigo1=".$longitud_barcode; 
        $query = $this->db->query($strq); 
        return $query->result(); 
    } 
     
    function ingresarventa($uss,$cli,$total,$fac,$des,$metodo,$tipo,$pagado){ 
        $strq = "INSERT INTO ventas(id_personal, id_cliente,bodega, metodo, monto_total,facturada,descuento,tipo,pagado) VALUES ($uss,$cli,$this->bodega,$metodo,$total,$fac,$des,$tipo,$pagado)"; 
        $query = $this->db->query($strq); 
        $id=$this->db->insert_id(); 
        return $id; 
    } 
    function ingresarventa2($uss,$cli,$mpago,$sbtotal,$desc,$descu,$total,$efectivo,$tarjeta,$tipo,$pagado){
        $strq = "INSERT INTO ventas(id_personal,id_cliente,bodega, metodo, subtotal, descuento,descuentocant, monto_total,pagotarjeta,efectivo,tipo,pagado) 
                VALUES ($uss,$cli,$this->bodega,$mpago,$sbtotal,$desc,$descu,$total,'$tarjeta','$efectivo','$tipo','$pagado')";
        $query = $this->db->query($strq);
        $id=$this->db->insert_id();
        return $id;
    }
    function ingresapedidod($id_vd,$entregado,$fecha_entrega,$repartidor){
        $strq = "INSERT INTO pedido_detalle(id_detalle_venta,entregado,fecha_entrega,id_repartidor) VALUES ($id_vd,$entregado,$fecha_entrega,$repartidor)"; 
        $query = $this->db->query($strq); 
        return $this->db->insert_id();
    }

    function ingresarventad($idventa,$producto,$cantidad,$kilos,$precio){ 
        if ($this->bodega=='2') { 
            $stock='stok2'; 
        }elseif($this->bodega=='3') { 
            $stock='stok3'; 
        }else{ 
            $stock='stok'; 
        } 
        $strq = "INSERT INTO venta_detalle(id_venta, id_producto, cantidad,kilos, precio) VALUES ($idventa,$producto,'$cantidad','$kilos',$precio)"; 
        $query = $this->db->query($strq); 
        //return $this->db->insert_id();
        //$this->db->close(); 
        //=================================== 
        $strq1 = "SELECT * FROM sproductosub where subId=$producto"; 
        $query1 = $this->db->query($strq1); 
        //$this->db->close(); 
        $PresentacionId=0; 
        $productoaddId=0; 
        foreach ($query1->result() as $row) { 
            $PresentacionId =$row->PresentacionId; 
            $productoaddId =$row->productoaddId; 
        } 
        //==================================== 
        $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId"; 
        $query2 = $this->db->query($strq2); 
        //$this->db->close(); 
        $unidad=1; 
        foreach ($query2->result() as $row) { 
            $unidad =$row->unidad; 
        } 
        //=================================== 
        $tounidad=$unidad*$cantidad; 
        //================================ 
        $strq4 = "UPDATE sproductosub SET $stock=$stock-$tounidad WHERE productoaddId=$productoaddId and tipo=1"; 
        $query4 = $this->db->query($strq4); 
        //$this->db->close(); 
        //=============================================== 
 
 
        $strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=1"; 
        $query3 = $this->db->query($strq3); 
        //$this->db->close(); 
        foreach ($query3->result() as $row) { 
            if ($this->bodega=='2') { 
                $stok =$row->stok2; 
            }elseif ($this->bodega=='3') { 
                $stok =$row->stok3; 
            }else{ 
                $stok =$row->stok; 
            } 
        } 
 
        $strq3 = "SELECT * FROM sproductosub where productoaddId=$productoaddId and tipo=0"; 
        $query3 = $this->db->query($strq3); 
        //$this->db->close(); 
        foreach ($query3->result() as $row) { 
            $subId =$row->subId; 
            $PresentacionId =$row->PresentacionId; 
 
            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId"; 
            $query2 = $this->db->query($strq2); 
            //$this->db->close(); 
            $unidad=1; 
            foreach ($query2->result() as $row) { 
                $unidad =$row->unidad; 
            } 
            $newstok=$stok/$unidad; 
 
            $strq4 = "UPDATE sproductosub SET $stock=$newstok WHERE subId=$subId and tipo=0"; 
            $query4 = $this->db->query($strq4); 
            //$this->db->close(); 
        }  
        //================================== 
    } 
    function configticket(){ 
        $strq = "SELECT * FROM ticket"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function clientepordefecto(){ 
        $strq = "SELECT * FROM  clientes"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    }
    function getventas($id){ 
        $strq = "SELECT ven.id_venta,ven.id_personal,ven.id_cliente,cli.Nom,ven.metodo,ven.bodega,ven.monto_total, ven.facturada,ven.datosfactura,ven.descuento,ven.cancelado,ven.hcancelacion,ven.reg, ven.metodo
         FROM ventas as ven  
         left join clientes as cli on cli.ClientesId=ven.id_cliente 
         where ven.id_venta=$id" 
         ; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function getventasd($id){ 
        //$strq = "CALL SP_GET_DETALLEVENTA($id)"; 
        $strq = "SELECT cat.categoria,mar.marca,pre.presentacion,pre.unidad,vdll.cantidad,vdll.kilos,vdll.precio,(vdll.cantidad*vdll.precio) as total, vdll.status
        FROM venta_detalle as vdll
        INNER JOIN sproductosub as sprob on sprob.subId=vdll.id_producto
        INNER JOIN sproducto as pro on pro.productoaddId=sprob.productoaddId
        INNER JOIN categoria as cat on cat.categoriaId=pro.productoId
        INNER JOIN marca as mar on mar.marcaid=pro.MarcaId
        INNER JOIN presentaciones as pre ON pre.presentacionId=sprob.PresentacionId
        WHERE vdll.id_venta=$id";
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 

    function getvdirecventa($id){ 
        $strq = "SELECT direc, DATE_FORMAT(fecha_pedido,'%d-%m-%Y') as fecha_pedido from pedido_detalle where id_venta=$id"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function ingresarcompra($uss,$prov,$total){ 
        $strq = "INSERT INTO compras(id_proveedor, monto_total) VALUES ($prov,$total)"; 
        $query = $this->db->query($strq); 
        $id=$this->db->insert_id(); 
        //$this->db->close(); 
        return $id; 
    } 
    function ingresarcomprad($idcompra,$producto,$cantidad,$kilos,$precio){ 
        $strq = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        $subId=0; 
        foreach ($query->result() as $row) { 
            $subId =$row->subId; 
            $PresentacionId = $row->PresentacionId; 
        } 
        if ($subId>0) { 
            $strq = "INSERT INTO compra_detalle(id_compra, id_producto, cantidad,kilos, precio_compra) VALUES ($idcompra,$subId,$cantidad,$kilos,$precio)"; 
            $query = $this->db->query($strq); 
            //$this->db->close(); 
            //============================================================================ 
            $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId"; 
            $query2 = $this->db->query($strq2); 
            $this->db->close(); 
            $unidad=1; 
            foreach ($query2->result() as $row) { 
                $unidad =$row->unidad; 
            } 
            //=================================== 
            $tounidad=$unidad*$cantidad; 
            //================================ 
            $strq4 = "UPDATE sproductosub SET stok=stok+$tounidad WHERE productoaddId=$producto and tipo=1"; 
            $query4 = $this->db->query($strq4); 
            //$this->db->close(); 
            //===================================================== 
            $strq41 = "UPDATE sproducto SET precompra=$precio WHERE productoaddId=$producto"; 
            $query41 = $this->db->query($strq41); 
            //$this->db->close(); 
            //=============================================== 
            //=============================================== 
            $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=1"; 
            $query3 = $this->db->query($strq3); 
            //$this->db->close(); 
            foreach ($query3->result() as $row) { 
                $stok =$row->stok; 
            } 
 
            $strq3 = "SELECT * FROM sproductosub where productoaddId=$producto and tipo=0"; 
            $query3 = $this->db->query($strq3); 
            //$this->db->close(); 
            foreach ($query3->result() as $row) { 
                $subId =$row->subId; 
                $PresentacionId =$row->PresentacionId; 
 
                $strq2 = "SELECT * FROM presentaciones where presentacionId=$PresentacionId"; 
                $query2 = $this->db->query($strq2); 
                $this->db->close(); 
                $unidad=1; 
                foreach ($query2->result() as $row) { 
                    $unidad =$row->unidad; 
                } 
                $newstok=$stok/$unidad; 
 
                $strq4 = "UPDATE sproductosub SET stok=$newstok WHERE subId=$subId and tipo=0"; 
                $query4 = $this->db->query($strq4); 
                //$this->db->close(); 
            } 
        } 
        //=================================================================== 
         
        /* 
        $strq = "UPDATE productos SET stock=stock+$cantidad WHERE productoid=$producto"; 
        $query = $this->db->query($strq); 
        $this->db->close(); 
        */ 
    } 
    function turnos(){ 
        $strq = "SELECT * FROM turno ORDER BY id DESC LIMIT 1"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        $status='cerrado'; 
        foreach ($query->result() as $row) { 
            $status =$row->status; 
        } 
        return $status; 
    } 
    function turnoss($bodega){ 
        $strq = "SELECT * FROM turno where bodega=$bodega ORDER BY id DESC LIMIT 1"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function turnosactivo($bodega){ 
        $strq = "SELECT * FROM turno where bodega=$bodega ORDER BY id DESC LIMIT 1"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        $status=0; 
        foreach ($query->result() as $row) { 
            if ($row->status=='abierto') { 
                $status=1; 
            } 
        } 
        return $status; 
    } 
    function abrirturno($cantidad,$nombre,$fecha,$horaa,$bodega){ 
        $strq = "INSERT INTO turno(fecha, horaa, cantidad, nombre, status, user,bodega) VALUES ('$fecha','$horaa','$cantidad','$nombre','abierto','user','$bodega')"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
    } 
    function cerrarturno($id,$horaa){ 
        $fechac=date('Y-m-d'); 
        $strq = "UPDATE turno SET horac='$horaa',fechacierre='$fechac', status='cerrado' WHERE id=$id"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
    } 
    function corte($inicio,$fin,$bodega){ 
        if ($bodega==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect=' and v.bodega='.$bodega; 
        } 
        $strq = "SELECT cl.ClientesId,cl.Nom, v.reg, IFNULL(v.id_venta,'0') as id_venta, v.id_cliente, v.id_personal, v.monto_total, p.personalId, concat(p.nombre,p.apellidos) as vendedor, v.bodega, vd.cantidad,vd.precio, sum(vd.cantidad*vd.precio) as totalvd
                FROM ventas as v  
                inner join usuarios as usu on usu.UsuarioID=v.id_personal  
                inner join personal as p on p.personalId=usu.personalId 
                join clientes as cl on cl.ClientesId=v.id_cliente 
                join venta_detalle as vd on vd.id_venta=v.id_venta
                WHERE v.reg between '$inicio 00:00:00' and '$fin 23:59:59' and v.cancelado!=1 and vd.status=1 $bodegaselect"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function cortecompras($inicio,$fin){ 
        $strq="SELECT compdll.id_detalle_compra,comp.reg,concat(cat.categoria,' ',mar.marca) as producto,prov.razon_social,compdll.cantidad,compdll.precio_compra  
                FROM compra_detalle as compdll  
                inner join compras as comp on comp.id_compra=compdll.id_compra  
                inner join sproductosub as pro on pro.subId=compdll.id_producto 
                inner join sproducto as pr on pr.productoaddId =pro.productoaddId 
                inner join categoria as cat on cat.categoriaId=pr.productoId 
                inner join marca as mar on mar.marcaid=pr.MarcaId 
                inner join proveedores as prov on prov.id_proveedor=comp.id_proveedor 
                where comp.reg between '$inicio 00:00:00' and '$fin 23:59:59' 
                ORDER BY compdll.id_detalle_compra DESC"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function cortenew($inicio,$fin,$bodega){ 
        if ($bodega==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect=' and ven.bodega='.$bodega; 
        } 
        $strq = "SELECT categoria,presentacion, cantidad, kilos,importe, tprecioc,descuento,precompra,(importe-tprecioc) as utilidad, id_venta, id_detalle_venta, metodo from( 
                SELECT categoria,presentacion,sum(cantidad) as cantidad,sum(kilos) as kilos,sum(total) as importe,sum(totalc) as tprecioc, sum(descuento) as descuento,precompra, id_venta, id_detalle_venta, metodo FROM( 
                    SELECT categoria,presentacion,cantidad,kilos,(cantidad2*precio) as total,(cantidad2*(precompra*unidad)) as totalc,((cantidad2*precio)*descuento) as descuento,precompra, id_venta, id_detalle_venta, metodo FROM ( 
                        SELECT cat.categoria,pre.presentacion, vend.cantidad,if(vend.kilos=0,vend.cantidad,vend.kilos) as cantidad2,vend.kilos, if(vend.kilos=0,1,vend.kilos) as kilos2,vend.precio,ven.descuento,pro.precompra,pre.unidad,ven.id_venta,vend.id_detalle_venta, metodo
                        FROM venta_detalle as vend 
                        inner JOIN ventas as ven on ven.id_venta=vend.id_venta 
                        inner JOIN sproductosub as pros on pros.subId=vend.id_producto 
                        inner JOIN sproducto as pro on pro.productoaddId=pros.productoaddId 
                        inner JOIN categoria as cat on cat.categoriaId=pro.productoId 
                        inner JOIN presentaciones as pre on pre.presentacionId=pros.PresentacionId 
                        WHERE ven.cancelado!=1 and vend.status=1 $bodegaselect AND ven.reg BETWEEN '$inicio 00:00:00' and '$fin 23:59:59' 
                        ) as con 
                    ) as conn 
                    /*GROUP BY categoria,presentacion */
                    GROUP BY id_detalle_venta
                ) as connn"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function consultarturnoname($inicio,$fin){ 
        $strq = "SELECT * FROM turno where fecha>='$inicio' and fechacierre<='$fin'"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        //$nombre=$cfecha.'/'.$chora; 
        return $query; 
    } 
    function cortesum($inicio,$fin,$bodega){ 
        if ($bodega==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect=' and bodega='.$bodega; 
        } 
        $strq = "SELECT sum(monto_total) as total , 0 as subtotal, 0 as descuento  
                FROM ventas  
                where reg between '$inicio 00:00:00' and '$fin 23:59:59' and cancelado=0 $bodegaselect"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function filas($bode,$tipo) { 
        if ($bode==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and bodega='.$bode; 
        } 
        if($tipo==0){
            $tipowhere="";
        }else if($tipo==1){ //credito
            $tipowhere=" and ventas.metodo=4";
        }else if($tipo==2){ //contado
            $tipowhere=" and ventas.metodo!=4";
        }
        $strq = "SELECT COUNT(*) as total FROM ventas 
                 where tipo=1 $bodegaselect $tipowhere 
                "; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->total; 
        }  
        return $total; 
    } 
    function total_paginados($por_pagina,$segmento,$bodega,$tipo) { 
        //$consulta = $this->db->get('productos',$por_pagina,$segmento); 
        //return $consulta; 
        if ($segmento!='') { 
            $segmento=','.$segmento; 
        }else{ 
            $segmento=''; 
        } 
        if ($bodega==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bodega; 
        } 
        if($tipo==0){
            $tipowhere="";
        }else if($tipo==1){ //credito
            $tipowhere=" and ven.metodo=4";
        }else if($tipo==2){ //contado
          $tipowhere=" and ven.metodo!=4";
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado, ven.metodo as tipo_metodo
                FROM ventas as ven  
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal  
                inner join personal as per on per.personalId=usu.personalId  
                inner join clientes as cli on cli.ClientesId=ven.id_cliente 
                left join metodopago as met on met.metodoId=ven.metodo 
                where ven.tipo = 1   $bodegaselect $tipowhere 
                /*and ven.cancelado=0*/
                ORDER BY ven.id_venta DESC 
                LIMIT $por_pagina $segmento"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 

    function dataExport($bodega,$tipo) { //ventas
        if ($bodega==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bodega; 
        } 
        if($tipo==0){
            $tipowhere="";
        }else if($tipo==1){ //credito
            $tipowhere=" and ven.metodo=4";
        }else if($tipo==2){ //contado
          $tipowhere=" and ven.metodo!=4";
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado 
                FROM ventas as ven  
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal  
                inner join personal as per on per.personalId=usu.personalId  
                inner join clientes as cli on cli.ClientesId=ven.id_cliente 
                left join metodopago as met on met.metodoId=ven.metodo 
                where ven.tipo=1 $bodegaselect $tipowhere 
                ORDER BY ven.id_venta DESC"; 
        $query = $this->db->query($strq);
        return $query; 
    } 

    function filasPedido($bode,$tipo) { 
        if ($bode==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and bodega='.$bode; 
        } 
        if($tipo==0){
            $tipowhere="";
        }else if($tipo==1){ //credito
            $tipowhere=" and ventas.metodo=4";
        }else if($tipo==2){ //contado
          $tipowhere=" and ventas.metodo!=4";
        }
        $strq = "SELECT COUNT(*) as total FROM ventas 
                 where tipo=2 $bodegaselect $tipowhere"; 
        $query = $this->db->query($strq); 
        $this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->total; 
        }  
        return $total; 
    } 
    function total_paginadosPedido($por_pagina,$segmento,$bodega,$tipo) { 
        //$consulta = $this->db->get('productos',$por_pagina,$segmento); 
        //return $consulta; 
        if ($segmento!='') { 
            $segmento=','.$segmento; 
        }else{ 
            $segmento=''; 
        } 
        if ($bodega==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bodega; 
        } 
        if($tipo==0){
            $tipowhere="";
        }else if($tipo==1){ //credito
            $tipowhere=" and ven.metodo=4";
        }else if($tipo==2){ //contado
          $tipowhere=" and ven.metodo!=4";
        }
        /*$strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado, cli.Calle, cli.noExterior, cli.noInterior, cli.Colonia, pd.direc
                FROM ventas as ven  
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal  
                left join personal as per on per.personalId=usu.personalId  
                inner join clientes as cli on cli.ClientesId=ven.id_cliente 
                inner join pedido_detalle as pd on pd.id_venta=ven.id_venta 
                left join metodopago as met on met.metodoId=ven.metodo 
                where ven.facturada=0 $bodegaselect
                and ven.tipo = 2  
                ORDER BY ven.id_venta DESC 
                LIMIT $por_pagina $segmento"; */
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado, cli.Calle, cli.noExterior, cli.noInterior, cli.Colonia, pedido_detalle.*, pedido_detalle.id_venta as id_ventaPD,
            DATE_FORMAT(pedido_detalle.fecha_pedido,'%d/%m/%Y') as re_fecha_pedido,ven.metodo as tipo_metodo
            FROM pedido_detalle 
            inner join ventas as ven on ven.id_venta=pedido_detalle.id_venta 
            left join usuarios as usu on usu.UsuarioID=ven.id_personal 
            left join personal as per on per.personalId=usu.personalId 
            left join clientes as cli on cli.ClientesId=ven.id_cliente 
            left join metodopago as met on met.metodoId=ven.metodo 
            where ven.tipo = 2 $bodegaselect $tipowhere  
            ORDER BY ven.id_venta DESC 
            LIMIT $por_pagina $segmento";
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 

    function dataExportPedido($bodega,$tipo) { 
        if ($bodega==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bodega; 
        } 
        if($tipo==0){
            $tipowhere="";
        }else if($tipo==1){ //credito
            $tipowhere=" and ven.metodo=4";
        }else if($tipo==1){ //contado
          $tipowhere=" and ven.metodo!=4";
        }
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado 
                FROM ventas as ven  
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal  
                inner join personal as per on per.personalId=usu.personalId  
                inner join clientes as cli on cli.ClientesId=ven.id_cliente 
                left join metodopago as met on met.metodoId=ven.metodo 
                where ven.tipo=2 $bodegaselect $tipowhere 
                ORDER BY ven.id_venta DESC"; 
        $query = $this->db->query($strq);
        return $query; 
    } 

    function filastur($bodega) { 
        $strq = "SELECT COUNT(*) as total FROM turno where bodega=$bodega"; 
        $query = $this->db->query($strq); 
        $this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->total; 
        }  
        return $total; 
    } 
    function cancalarventa($id){ 
        $fecha = date('Y-m-d'); 
        $strq = "UPDATE ventas SET cancelado=1,hcancelacion='$fecha' WHERE `id_venta`=$id"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
    } 

    function cancelarventa2($id/*,$personal*/){
        $fecha = date('Y-m-d H:i:s');
        //$strq = "UPDATE ventas SET cancelado=2,cancela_personal='$personal',hcancelacion='$fecha' WHERE `id_venta`=$id";
        $strq = "UPDATE ventas SET cancelado=2,hcancelacion='$fecha' WHERE id_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
    }

    function ventadetalles($id){ 
        $strq = "SELECT * FROM venta_detalle where id_venta=$id"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    /*function regresarpro($id,$can){ 
        $strq = "UPDATE productos set stock=stock+$can where productoid=$id"; 
        $query = $this->db->query($strq); 
        $this->db->close(); 
    } */
    function regresarpro($id,$can){ 
        $strq = "UPDATE sproductosub set stok=stok+$can where subId=$id"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
    } 
    function filasturnos() { 
        $strq = "SELECT COUNT(*) as total FROM turno"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->total; 
        }  
        return $total; 
    } 
    function total_paginadosturnos($por_pagina,$segmento,$bodega) { 
        //$consulta = $this->db->get('productos',$por_pagina,$segmento); 
        //return $consulta; 
        if ($segmento!='') { 
            $segmento=','.$segmento; 
        }else{ 
            $segmento=''; 
        } 
        $strq = "SELECT * FROM turno where bodega=$bodega ORDER BY id DESC 
                LIMIT $por_pagina $segmento"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function consultarturno($id){ 
        $strq = "SELECT * FROM turno where id=$id"; 
        $query = $this->db->query($strq); 
        $this->db->close(); 
        return $query; 
    } 
    function consultartotalturno($fecha,$horaa,$horac,$fechac,$bodega){ 
        $strq = "SELECT sum(monto_total) as total FROM ventas  
                where bodega=$bodega and 
                      cancelado=0 and  
                      reg between '$fecha $horaa' and '$fechac $horac' "; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->total; 
        } 
        return $total; 
    } 
    function consultartotalturno2($fecha,$horaa,$horac,$fechac){ 
        $strq = "SELECT sum(p.preciocompra*vd.cantidad) as preciocompra  
                from venta_detalle as vd 
                inner join productos as p on vd.id_producto=p.productoid 
                inner join ventas as v on vd.id_venta=v.id_venta 
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->preciocompra; 
        } 
        return $total; 
    } 
    function consultartotalturnopro($fecha,$horaa,$horac,$fechac,$bodega){ 
        /*$strq = "SELECT concat(cat.categoria,' ',mar.marca,' ',pre.presentacion)as producto,vd.cantidad, vd.precio 
                from venta_detalle as vd 
                inner join sproductosub as p on vd.id_producto=p.subId 
                inner join presentaciones as pre on pre.presentacionId=p.PresentacionId 
                inner join sproducto as pro on pro.productoaddId=p.productoaddId 
                inner join categoria as cat on cat.categoriaId=pro.productoId 
                inner join marca as mar on mar.marcaid=pro.MarcaId 
                inner join ventas as v on vd.id_venta=v.id_venta 
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac'";*/ 
        $strq="CALL SP_GET_PRODUCTOS_TURNO('$fecha $horaa','$fechac $horac',$bodega)"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function consultartotalturnopromas($fecha,$horaa,$horac,$fechac,$bodega){ 
        /*$strq = "SELECT p.nombre as producto, sum(vd.cantidad) as total  
                from venta_detalle as vd  
                inner join productos as p on vd.id_producto=p.productoid  
                inner join ventas as v on vd.id_venta=v.id_venta  
                where v.cancelado=0 and v.reg>='$fecha $horaa' and v.reg<='$fechac $horac' GROUP BY producto ORDER BY `total` DESC LIMIT 1"; */ 
        $strq ="CALL SP_GET_PRODUCTOMAS_TURNO('$fecha $horaa','$fechac $horac',$bodega)"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function filaslcompras(){ 
        $strq = "SELECT COUNT(*) as total FROM compra_detalle"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->total; 
        }  
        return $total; 
    } 
    function total_paginadoslcompras($por_pagina,$segmento) { 
        //$consulta = $this->db->get('productos',$por_pagina,$segmento); 
        //return $consulta; 
        if ($segmento!='') { 
            $segmento=','.$segmento; 
        }else{ 
            $segmento=''; 
        } 
        $strq = "SELECT comd.id_detalle_compra,com.reg,concat(cat.categoria,' ',mar.marca) as producto,prov.razon_social,comd.cantidad,comd.kilos,comd.precio_compra 
                    FROM compra_detalle as comd 
                    left join compras as com on com.id_compra=comd.id_compra 
                    left join sproductosub as pros on pros.subId=comd.id_producto 
                    left join sproducto as pro on pro.productoaddId=pros.productoaddId 
                    left join categoria as cat on cat.categoriaId=pro.productoId 
                    left join marca as mar on mar.marcaid=pro.MarcaId 
                    left join proveedores as prov on prov.id_proveedor=com.id_proveedor  
                ORDER BY comd.id_detalle_compra DESC 
                LIMIT $por_pagina $segmento"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function lcomprasconsultar($inicio,$fin) { 
         
        $strq = "SELECT comd.id_detalle_compra,com.reg,concat(cat.categoria,' ',mar.marca) as producto,prov.razon_social,comd.cantidad,comd.kilos,comd.precio_compra 
                    FROM compra_detalle as comd 
                    left join compras as com on com.id_compra=comd.id_compra 
                    left join sproductosub as pros on pros.subId=comd.id_producto 
                    left join sproducto as pro on pro.productoaddId=pros.productoaddId 
                    left join categoria as cat on cat.categoriaId=pro.productoId 
                    left join marca as mar on mar.marcaid=pro.MarcaId 
                    left join proveedores as prov on prov.id_proveedor=com.id_proveedor   
                where com.reg between '$inicio 00:00:00' and '$fin 23:59:59' 
                ORDER BY comd.id_detalle_compra DESC"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function ventassearch($search,$bode){ 
        if ($bode==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bode; 
        } 
        $strq="SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado 
                FROM ventas as ven  
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal  
                inner join personal as per on per.personalId=usu.personalId  
                inner join clientes as cli on cli.ClientesId=ven.id_cliente 
                left join metodopago as met on met.metodoId=ven.metodo 
                where ven.facturada=0 $bodegaselect and ven.id_venta like '%".$search."%' or  
                      ven.facturada=0 $bodegaselect and ven.reg like '%".$search."%' or 
                      ven.facturada=0 $bodegaselect and cli.Nom like '%".$search."%' or 
                      ven.facturada=0 $bodegaselect and per.nombre like '%".$search."%' or 
                      ven.facturada=0 $bodegaselect and ven.monto_total like '%".$search."%' 
                "; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    }

    function pedidosSearch($search,$bode){ 
        if ($bode==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bode; 
        } 
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado, cli.Calle, cli.noExterior, cli.noInterior, cli.Colonia, pedido_detalle.*, pedido_detalle.id_venta as id_ventaPD,
            DATE_FORMAT(pedido_detalle.fecha_pedido,'%d/%m/%Y') as re_fecha_pedido
            FROM pedido_detalle 
            inner join ventas as ven on ven.id_venta=pedido_detalle.id_venta 
            left join usuarios as usu on usu.UsuarioID=ven.id_personal 
            left join personal as per on per.personalId=usu.personalId 
            left join clientes as cli on cli.ClientesId=ven.id_cliente 
            left join metodopago as met on met.metodoId=ven.metodo 
            where ven.facturada=0 and ven.tipo = 2
            $bodegaselect and ven.id_venta like '%".$search."%' or  
                      ven.facturada=0 $bodegaselect and ven.reg like '%".$search."%' or 
                      ven.facturada=0 $bodegaselect and cli.Nom like '%".$search."%' or 
                      ven.facturada=0 $bodegaselect and per.nombre like '%".$search."%' or 
                      ven.facturada=0 $bodegaselect and ven.monto_total like '%".$search."%' or
                      ven.facturada=0 $bodegaselect and pedido_detalle.fecha_pedido like '%".$search."%' or
                      ven.facturada=0 $bodegaselect and pedido_detalle.fecha_entrega like '%".$search."%' or
                      ven.facturada=0 $bodegaselect and pedido_detalle.direc like '%".$search."%'
                "; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    //==================================facturadas 
    function filasf($bode) { 
         if ($bode==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bode; 
        } 
        $strq = "SELECT COUNT(*) as total FROM ventas as ven 
                where ven.facturada=1 $bodegaselect 
                "; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->total; 
        }  
        return $total; 
    } 
    function total_paginadosf($por_pagina,$segmento,$bode) { 
        if ($bode==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bode; 
        } 
        if ($segmento!='') { 
            $segmento=','.$segmento; 
        }else{ 
            $segmento=''; 
        } 
        $strq = "SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado, ven.bodega
                FROM ventas as ven  
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal  
                inner join personal as per on per.personalId=usu.personalId  
                inner join clientes as cli on cli.ClientesId=ven.id_cliente 
                left join metodopago as met on met.metodoId=ven.metodo 
                where ven.facturada=1 $bodegaselect 
                ORDER BY ven.id_venta DESC 
                LIMIT $por_pagina $segmento"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    function ventassearchf($search,$bodega){ 
        if ($bodega==0) { 
            $bodegaselect=''; 
        }else{ 
            $bodegaselect='and ven.bodega='.$bode; 
        } 
        $strq="SELECT ven.id_venta,ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,met.metodo, ven.monto_total,ven.cancelado, ven.bodega
                FROM ventas as ven  
                inner join usuarios as usu on usu.UsuarioID=ven.id_personal  
                inner join personal as per on per.personalId=usu.personalId  
                inner join clientes as cli on cli.ClientesId=ven.id_cliente 
                left join metodopago as met on met.metodoId=ven.metodo 
                where ven.facturada=1 $bodegaselect and ven.id_venta like '%".$search."%' or  
                      ven.facturada=1 $bodegaselect and ven.reg like '%".$search."%' or 
                      ven.facturada=1 $bodegaselect and cli.Nom like '%".$search."%' or 
                      ven.facturada=1 $bodegaselect and per.nombre like '%".$search."%' or 
                      ven.facturada=1 $bodegaselect and ven.monto_total like '%".$search."%' 
                "; 
        $query = $this->db->query($strq); 
        //*$this->db->close(); 
        return $query; 
    } 
    function datosfactura($idventa,$datos){ 
        $strq="CALL SP_ADD_DATOSFACTURA($idventa,'$datos')"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $strq; 
    } 
    function datosfacturaget($id){ 
        $strq="SELECT datosfactura FROM ventas where id_venta=$id"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        return $query; 
    } 
    // para corte 
 
    function totalcajas($id){ 
         $strq = "SELECT pre.presentacion,sum(vend.cantidad) as cantidad 
                    from venta_detalle as vend 
                    inner join sproductosub as spro on spro.subId=vend.id_producto 
                    inner join presentaciones as pre on pre.presentacionId=spro.PresentacionId 
                    WHERE vend.id_venta=$id 
                    GROUP BY pre.presentacion"; 
        $query = $this->db->query($strq); 
        $this->db->close(); 
        $presentacionestotal=''; 
        foreach ($query->result() as $pres) { 
           $presentacionestotal.='<p>'.$pres->presentacion.' : '.$pres->cantidad.'</p>';  
        } 
        return $presentacionestotal;  
    } 
    function totalkilos($id){ 
         $strq = "SELECT sum(kilos) as total FROM venta_detalle where id_venta=$id"; 
        $query = $this->db->query($strq); 
        //$this->db->close(); 
        foreach ($query->result() as $row) { 
            $total =$row->total; 
        }  
        return $total; 
    } 
 
    function ingresarpedidod($id_dv,$entregado,$fecha_ped,$direc){ 
        $strq = "INSERT INTO pedido_detalle(id_venta,entregado,fecha_pedido,direc) VALUES ($id_dv,$entregado,'$fecha_ped','$direc')"; 
        $query = $this->db->query($strq); 
        $id=$this->db->insert_id(); 
        return $id; 
    } 

    public function get_pedido_det($params){
        $this->db->select("pd.*, (
                CASE 
                    WHEN pd.entregado = '0' THEN 'Sin Repartir'
                    WHEN pd.entregado = '1' THEN 'Entregado'
                    WHEN pd.entregado = '2' THEN 'En curso'
                    ELSE 3
                END) AS esdo, concat(personal.nombre, ,personal.apellidos) as repartidor");
        $this->db->from('pedido_detalle pd');
        $this->db->join("personal","personal.personalId=id_repartidor",'left');
        $this->db->join("venta_detalle","venta_detalle.id_venta=pd.id_venta",'left'); 
        $this->db->join("sproductosub","sproductosub.subId=venta_detalle.id_producto",'left');
        $this->db->where("venta_detalle.id_venta","{$params['venta']}"); 
        $this->db->group_by("venta_detalle.id_venta");
        $query = $this->db->get();
        if($query->num_rows()>0)
            return $query->result();
        else
           return 0;
    }

    function detallesVentas($id) {
        $strq = "SELECT ven.id_venta, ven.bodega, ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo, ven.monto_total,ven.cancelado, vt.id_detalle_venta, vt.cantidad, vt.precio, vt.id_producto as id_productovd, vt.status as status_vd,
            cat.categoria,mar.marca,pre.presentacion,pre.unidad
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                /*inner join sucursal as suc on suc.idsucursal=ven.sucursalid*/
                inner join venta_detalle as vt on vt.id_venta=ven.id_venta
                INNER JOIN sproductosub as sprob on sprob.subId=vt.id_producto
                INNER JOIN sproducto as pro on pro.productoaddId=sprob.productoaddId
                INNER JOIN categoria as cat on cat.categoriaId=pro.productoId
                INNER JOIN marca as mar on mar.marcaid=pro.MarcaId
                INNER JOIN presentaciones as pre ON pre.presentacionId=sprob.PresentacionId
                where ven.id_venta=$id
                ORDER BY ven.id_venta DESC";
        $query = $this->db->query($strq);
        return $query;
    }

    function detallesVentasId($id) {
        $strq = "SELECT *, ven.bodega
                FROM venta_detalle 
                inner join ventas as ven on ven.id_venta=venta_detalle.id_venta
                where venta_detalle.id_detalle_venta=$id";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query;
    }

    public function cancelaParcial($id)
    {
        $this->db->where('id_detalle_venta', $id);
        $this->db->set('status',0);
        $this->db->update('venta_detalle');
    }

    public function cancelaParcial2($id)
    {
        $this->db->where('id_venta', $id);
        $this->db->set('status',0);
        $this->db->update('venta_detalle');
    }

    public function update_stock($id_prod,$cantidad,$id_suc){ 
        if($id_suc==1) $namestock = "stok";
        if($id_suc==2) $namestock = "stok2";
        if($id_suc==3) $namestock = "stok3";
        //if($id_suc==4) $namestock = "stok";

        $sql = "UPDATE sproductosub SET $namestock=$namestock+$cantidad WHERE productoaddId=$id_prod";
        return $this->db->query($sql);
    }

    public function detalles_pedido($id){
        $this->db->select('*, sum(cantidad) as total_pagado');
        $this->db->from("ventas_credito_pago");
        $this->db->where("id_venta",$id);
        $query=$this->db->get();
        return $query->result();
    }

    /*function detallesVentasPorID($id) {
        $strq = "SELECT ven.id_venta, ven.bodega, ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.monto_total,ven.cancelado, vt.id_detalle_venta, vt.cantidad, vt.precio, vt.id_producto as id_productovd, vt.status as status_vd,
            prods.codigo, prods.descripcion, prods.nombre, IFNULL(sum(vp.cantidad),'0') as totalPagado, sum(vt.cantidad*vt.precio) as total_vd
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                inner join ventas_credito_pago as vp on vp.id_venta=ven.id_venta
                inner join venta_detalle as vt on vt.id_venta=ven.id_venta
                left join productos as prods on prods.productoid=vt.id_producto
                where ven.id_venta=$id
                and vp.estatus=1 and vt.status=1
                ORDER BY ven.id_venta DESC";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query->result();
    }*/

    function detallesVentasPorID($id) {
        $strq = "SELECT ven.id_venta, ven.bodega, ven.reg, cli.Nom as cliente, concat(per.nombre,' ',per.apellidos) as vendedor,ven.metodo,ven.monto_total,ven.cancelado, 
            IFNULL(sum(vp.cantidad),'0') as totalPagado, (select sum(vd.cantidad*vd.precio) from venta_detalle as vd where vd.id_venta=ven.id_venta
                and vd.status=1) as total_vd
                FROM ventas as ven 
                inner join personal as per on per.personalId=ven.id_personal
                inner join clientes as cli on cli.ClientesId=ven.id_cliente
                inner join ventas_credito_pago as vp on vp.id_venta=ven.id_venta
                where ven.id_venta=$id
                and vp.estatus=1
                ORDER BY ven.id_venta DESC";
        $query = $this->db->query($strq);
        //$this->db->close();
        return $query->result();
    }

    function get_pagosIDV($id) {
        $this->db->select("IFNULL(sum(vp.cantidad),'0') as totalPagado");
        $this->db->from("ventas_credito_pago vp");
        $this->db->where("id_venta",$id);
        $this->db->where("estatus",1);
        $query=$this->db->get();
        return $query->row();
    }
    
    public function verificaPagos($id){
        $this->db->select("*");
        $this->db->from('ventas_credito_pago');
        $this->db->where('id_venta',$id);
        $this->db->where('estatus',1); 
        $query=$this->db->get();
        return $query;
    }

    public function getselectwheren($table,$where){
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where($where);/// Se puede ocupar un array para n condiciones
        $query=$this->db->get();
        return $query;
    }

    public function get_venta_det($params){
        $this->db->select("vd.*, concat(cat.categoria,' ',mar.marca) as producto");
        $this->db->from('venta_detalle vd');
        $this->db->join("sproductosub pro","pro.subId=vd.id_producto",'left');
        $this->db->join("sproducto pr","pr.productoaddId=pro.productoaddId");
        $this->db->join("categoria cat","cat.categoriaId=pr.productoId");
        $this->db->join("marca mar","mar.marcaid=pr.MarcaId");

        $this->db->where("vd.id_venta","{$params['venta']}"); 
        $this->db->group_by("vd.id_venta");
        $query = $this->db->get();
        if($query->num_rows()>0)
            return $query->result();
        else
           return 0;
    }
}