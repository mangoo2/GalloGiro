<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<div class="row">
    <div class="col-md-12">
      <h2>Corte de caja </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Corte de caja</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row inputbusquedas">
                    <div class="col-md-12">
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-1">Desde:</label>
                                <div class="col-md-3">
                                    <input id="txtInicio" name="txtInicio" class="form-control date-picker" size="16" type="date" />
                                </div>
                                <label class="control-label col-md-1">Hasta:</label>
                                <div class="col-md-3">
                                    <input id="txtFin" name="txtFin" class="form-control date-picker" size="16" type="date"/>
                                </div>
                                <div class="col-md-2">                      
                                    <div class="checkbox-list">
                                        <label><input type="checkbox" id="chkFecha" value="1"> Fecha actual </label>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="control-label col-md-1">Sucursal</label>
                        <div class="col-md-3">
                            <select id="selectbodega" class="form-control">
                                <option value="0">Todos</option>
                                <option value="1">Matriz</option>
                                <option value="2">La Fragua</option>
                                <option value="3">16 de Septiembre</option>
                                <option value="4">Acocota</option>
                                <option value="5">Morillotla</option>
                                <option value="6">Sonata</option>
                            </select>
                        </div>
                        <label class="control-label col-md-1">Informe</label>
                        <div class="col-md-3">
                            <select id="selecinforme" class="form-control">
                                <option value="0">Productos</option>
                                <option value="1">Ventas</option>
                                <option value="2">Compras</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <a href="#" class="btn btn-raised gradient-purple-bliss white" id="btnBuscar" style="background: #2e58a6;">Buscar</a>
                            <a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"  style="background: #2e58a6;" ><i class="fa fa-print"></i></button></a>
                        </div>
                    </div>
                </div>
                <div class="row" id="imprimir">
                    <div class="col-md-12" id="tbCorte">
                        
                    </div>
                    <div class="col-md-12" id="tbCorte2">
                        
                    </div>
                    <div class="col-md-6" >
                        <p style="font-size: 20px;display: none;">
                            <span class="col-md-6 text-warning">SUBTOTAL:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="dSubtotal">0.00</span>
                            </span>
                        </p>
                        
                        <p style="font-size: 20px;display: none;">
                            <span class="col-md-6 text-warning">TOTAL:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="dTotal">0.00</span>
                            </span>
                        </p>
                        
                        
                        <p style="font-size: 20px;display: none;">
                            <span class="col-md-6 text-warning">Total de ventas:</span>
                            <span class="col-md-4" >
                                <span class="text-warning"> </span>
                                <span id="rowventas">0</span>
                            </span>
                        </p>
                        <p style="font-size: 20px;display: none;">
                            <span class="col-md-6 text-warning">Total de utilidad:</span>
                            <span class="col-md-4" >
                                <span class="text-warning">$</span>
                                <span id="totalutilidades">0</span>
                            </span>
                        </p>
                    </div>
                </div>
               
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function imprimir(){
      window.print();
    }
    function ticket(id){
        $("#iframeri").modal();
        $('#iframereporte').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+id+'"></iframe>');
      }
</script>