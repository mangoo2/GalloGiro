<?php 
/*if (isset($_GET['id'])) {
    $id=$_GET['id'];
    $gastos=$this->ModeloGasto->VerTodasGastos();
    foreach ($gastos->result() as $item){
      $label='Editar Gasto';
      $id = $item->id;
      $descrip = $item->descrip;
      $monto_gasto = $item->monto_gasto;
      $fecha_gasto = $item->fecha_gasto;
  }
}else{
  $label='Nuevo Gasto';
  $id = 0;
  $id = $item->id;
  $descrip = "";
  $monto_gasto = "";
  $fecha_gasto = ""; 
} */
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
  }
  .vd_green{
    color: #009688; 
  }
</style>
<div class="row">
  <div class="col-md-12">
    <h2>Registro de Gastos</h2>
  </div>
</div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Nuevo Gasto</h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal">
                                <!--------//////////////-------->
                                <div class="row">
                                  <form data-parsley-validate class="form-horizontal form-label-left" required id="form-insert-gasto">
                                    <div class="col-md-12">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="col-md-2 control-label">Descripción:</label>
                                          <div class=" col-md-10 input-group">
                                            <input type="text" class="form-control" id="descrip" name="descrip" value="">
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-12">
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label class="col-md-4 control-label">Monto:</label>
                                            <div class=" col-md-8 input-group">
                                              <input type="number" class="form-control" id="monto_gasto" name="monto_gasto" value="">
                                            </div>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <div class="form-group">
                                            <label class="col-md-3 control-label">Fecha:</label>
                                            <div class=" col-md-9 input-group">
                                              <input type="date" class="form-control" id="fecha_gasto" name="fecha_gasto" value="">
                                            </div>
                                          </div>
                                        </div>
                                        <br/><br/><br/><br/>
                                      </div>
                                        
                                      <div class="col-md-4" >
                                        <div class="form-group" align="right">
                                          <button type="submit" id="btn_submit" class="form-control btn-success">Guardar</button>
                                        </div>
                                        <br/><br/>
                                      </div>
                                      
                                  </form>


                                  </div>

                                  <div class="col-md-12">
                                    <h4>Historial de Gastos</h4>
                                   <table cellpadding="0" cellspacing="0" border="0" class="dataTable table table-striped" id="tablaGastos">
                                        <thead>
                                          <tr>
                                            <th>#</th>
                                            <th>Descripción</th>
                                            <th>Monto</th>
                                            <th>Fecha</th>
                                            <th>Delete</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                  </div>


                             
                                  
                                </div>
                                
                                
                                <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>