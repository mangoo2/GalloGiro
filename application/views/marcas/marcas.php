<style type="text/css">
  .imgpro{
    width: 80px;
  }
  .imgpro:hover {
    transform: scale(2.6); 
    filter: drop-shadow(5px 9px 12px #444);
}
</style>
<div class="row">
    <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
                <div class="col-md-12">
                  <h2>Marcas</h2>
                </div>
                
                <div class="col-md-12">
                  <div class="col-md-11"></div>
                  <div class="col-md-1">
                    <a href="<?php echo base_url(); ?>index.php/Marcas/marcasadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nueva Marca</a>
                  </div>
                </div>
                
                
              </div>
              <!--Statistics cards Ends-->

              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Listado de Marcas</h4>
                        </div>
                        <div class="col-md-12">
                          <div class="col-md-8">
                            
                          </div>
                        </div>
                        <div class="card-body">
                            <div class="card-block">
                                <!--------//////////////-------->
                                <table class="table table-striped" id="datatables_marcas" style="width: 100%">
                                      <thead>
                                        <tr>
                                          <th>#</th>
                                          <th>Imagen</th>
                                          <th>Nombre</th>
                                          <th>Acciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>

                                       
                                            
                                      </tbody>
                                    </table>
                                    
                                
                                    
                                    
                        <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>
