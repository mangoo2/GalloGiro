<?php 
if (isset($_GET['id'])) 
{
    $id=$_GET['id'];
    
    $marca=$this->Modelomarcas->getMarcaPorId($id);
    
    foreach ($marca as $item)
    {
      $label='Editar Marca';
      $id = $item->marcaid;
      $marca = $item->marca;
      $pesoi = $item->pesoi;
      $pesof = $item->pesof;
      $codigoi = $item->codigoi;
      $codigof = $item->codigof;
      $longitud_codigo1 = $item->longitud_codigo1;
      $img = '<img src="'.base_url().'public/img/marcas/'.$item->imgm.'">' ;
    }

    

}
else
{
  $label='Nueva Marca';
  $marca = '';
  $pesoi = '';
  $pesof = '';
  $codigoi = '';
  $codigof = '';
  $longitud_codigo1 = '';
  $img = '';
} 
?>
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
  }
  .vd_green{
    color: #009688; 
  }
  .controls{
    margin-bottom: 10px;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <h2>Marca</h2>
  </div>
</div>
              <!--Statistics cards Ends-->
              <!--Line with Area Chart 1 Starts-->
              <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"><?php echo $label;?></h4>
                        </div>
                        <div class="card-body">
                            <div class="card-block form-horizontal col-sm-12">
                                <!--------//////////////-------->
                                <div class="row">

                                  <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">

                                  <form method="post" id="form_marca" class="form-horizontal">

                                    <input type="hidden" name="marcaid" id="marcaid" value="<?php echo $id;?>">
                                    
                                     <div class="col-md-12">

                                      <div class="col-md-12">
                                        <h3>Datos generales</h3>
                                        <hr />
                                      </div>
                                      
                                      <br><br><br>

                                      
                                      <div class="col-md-6">
                                        <label class="col-sm-3 control-label">Imagen:</label>
                                        <div class="col-sm-8 fileinput fileinput-new" data-provides="fileinput" style="width: 100%">
                                            <div class="fileinput-preview thumbnail previewmar" data-trigger="fileinput" style="width: 100%; height: 150px; border: solid #a6a9ae 1px; border-radius: 12px;">
                                              <?php echo $img;?>
                                            </div>
                                            <div>
                                                <span class="btn btn-primary btn-file">
                                                    <span class="fileinput-new">Foto</span>
                                                    <span class="fileinput-exists">Cambiar</span>
                                                    <input type="file" name="img" id="img" data-allowed-file-extensions='["jpg", "png"]'>
                                                </span>
                                                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Quitar</a>
                                            </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Nombre de la Marca:</label>
                                            <div class="col-sm-8 controls">
                                              <input type="text" name="marca" class="form-control" id="marca" value="<?php echo $marca;?>">
                                          </div>
                                        </div>
                                      </div>
                                      

<!------------------------------------------------------------------->
<!------------------------------------------------------------------->



                                  <div class="col-md-12">
                                    
                                        <hr />
                                        <div class="col-md-12">
                                          <h3>Lectura de Código de Barras <i class="fa fa-barcode"></i></h3>
                                          <hr />
                                        </div>
                                        
                                    <div class="col-md-12">
                                        <div class="card">
                                          <div class="card-body">
                                            <div class="card-block">
                                              <p>* Puedes ingresar hasta un máximo de 3 configuraciones, cada una deberá llevar la lóngitud del código de barras, la longitud y ubicación del peso así como la información del código de producto. Si no es necesario solo deje los campos en blanco </p>
                                              <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                  <a class="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="tab1" href="#tab1" aria-expanded="true">Configuración 1</a>
                                                </li>
                                              </ul>
                                              <!--------------------------TAB 1---------------------------->
                                              <div class="tab-content px-1 pt-1">
                                                <br>
                                                <div role="tabpanel" class="tab-pane active" id="tab1" aria-expanded="true" aria-labelledby="base-tab1">
                                                  
                                                    <div class="col-md-12">
                                                      <p>Configuración 1</p>
                                                      <div class="form-group"> 
                                                      <label class="col-sm-2 control-label">Lóngitud del código de barras:</label> 
                                                        <div class="col-sm-3 controls">
                                                          <input type="text" id="longitud_codigo1" name="longitud_codigo1" value="<?php echo $longitud_codigo1;?>" class="form-control"/>
                                                            </div>
                                                          </div>
                                                      </div>
                                                      <div class="col-md-12">
                                                        <div class="form-group">
                                                          <label class="col-sm-2 control-label">Peso del Producto Del:</label>
                                                          <div class="col-sm-3 controls">
                                                            <input type="text" id="pesoi" name="pesoi" value="<?php echo $pesoi;?>" class="form-control"/>
                                                          </div>
                                                          <label class="col-sm-1 control-label">al:</label>
                                                          <div class="col-sm-3 controls">
                                                             <input type="text" id="pesof" name="pesof" value="<?php echo $pesof;?>" class="form-control"/>
                                                          </div>
                                                          <p class="col-sm-3 font-small-1 gray">* 4 posiciones: Primeras 2 para peso en kilogramos, últimas 2 para peso en gramos.</p>
                                                        </div>
                                                      </div>
                                                      <div class="col-md-12">
                                                        <div class="form-group">
                                                          <label class="col-sm-2 control-label">Código del Producto Del:</label>
                                                          <div class="col-sm-3 controls">
                                                            <input type="text" id="codigoi" name="codigoi" value="<?php echo $codigoi;?>" class="form-control"/>
                                                          </div>
                                                          <label class="col-sm-1 control-label">al:</label>
                                                          <div class="col-sm-3 controls">
                                                             <input type="text" id="codigof" name="codigof" value="<?php echo $codigof;?>" class="form-control"/>
                                                          </div>
                                                          <p class="col-sm-3 font-small-1 gray">* El código del producto se dara previamente de alta en el catalogo de productos.</p>
                                                        </div>
                                                      </div>
                                                </div>
                                                
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>                                        
                                        

                                        
                                        </div>



<!------------------------------------------------------------------->
<!------------------------------------------------------------------->





                                  </div> 

                                  <div class="col-md-12">
                                    <br>
                                    <br>
                                    <button type="submit" class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover" style="background: #2e58a6;" >
                                      Guardar
                                    </button>
                                  </div>

                                </form>
                                
                                

                                </div>





                                
                                <!--------//////////////-------->
                            </div>
                        </div>
                    </div>
                </div>
              </div>