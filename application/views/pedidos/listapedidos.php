<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<div class="row">
      <div class="col-md-12">
        <h2>Lista Pedidos</h2>
      </div>
    </div>
    <!--Statistics cards Ends-->

    <!--Line with Area Chart 1 Starts-->
    <div class="row">
      <div class="col-sm-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Listado de Pedidos</h4>
              </div>
              <div class="col-md-12 inputbusquedas">
                <div class="col-md-3">
                  
                </div>
                <div class="col-md-1">
                  <a id="btnImprimir" onclick="imprimir();"><button type="button" class="btn btn-raised gradient-purple-bliss white"  style="background: #2e58a6;" ><i class="fa fa-print"></i></button></a>
                </div>
                <div class="col-md-2">
                  <select class="form-control" id="tipo_venta" onchange="seleccionar()">
                    <option value="0" <?php if (isset($_GET['tipo'])) { if ($_GET['tipo']==0) { echo "selected"; } }?>>Todo</option>
                    <option value="1" <?php if (isset($_GET['tipo'])) { if ($_GET['tipo']==1) { echo "selected"; } }?>>Crédito</option>
                    <option value="2" <?php if (isset($_GET['tipo'])) { if ($_GET['tipo']==2) { echo "selected"; } }?>>Contado</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <select class="form-control" id="personalbodega" onchange="seleccionar()">
                    <option value="0">Todo</option>
                    <option value="1" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==1) { echo "selected"; } }?>  >Matriz</option>
                    <option value="2" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==2) { echo "selected"; } }?> >La Fragua</option>
                    <option value="3" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==3) { echo "selected"; } }?> >16 de Septiembre</option>
                    <option value="4" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==4) { echo "selected"; } }?> >Acocota</option>
                    <option value="5" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==5) { echo "selected"; } }?> >Morillotla</option>
                    <option value="6" <?php if (isset($_GET['bod'])) { if ($_GET['bod']==6) { echo "selected"; } }?> >Sonata</option>
                  </select>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                    <button type="button" id="exportar_excel" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-file-excel-o"> </i> Exportar</button>
                  </div>
                </div>
                <div class="col-md-2">
                    <form role="search" class="navbar-form navbar-right mt-1">
                      <div class="position-relative has-icon-right">
                        <input type="text" placeholder="Buscar" id="buscarvent" class="form-control round" oninput="buscarventa()">
                        <div class="form-control-position"><i class="ft-search"></i></div>
                      </div>
                    </form>
                </div>
              </div>
              <div class="card-body">
                  <div class="card-block">
                      <!--------//////////////-------->
                      <table class="table table-striped" id="data-tables" style="width: 100%">
                        <thead>
                          <tr>
                            <th>Folio</th>
                            <th>Fecha Pedido</th>
                            <!--<th>Fecha Entrega</th>-->
                            <th>Dirección</th>
                            <th>Monto</th>
                            <!--<th>Metodo</th>-->
                            <th>Cliente</th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tbodyresultadosvent">

                          <?php foreach ($ventas->result() as $item){ ?>
                           <tr id="trven_<?php echo $item->id_venta; ?>">
                              <td><?php echo $item->id_venta; ?></td>
                              <td><?php echo $item->re_fecha_pedido; ?></td> <!-- cambiar la direccion de cliente, mostrar de acuerdo al pedido y a la dir de pedido_Detalle -->
                              <!--<td><?php echo $item->fecha_entrega; ?></td>-->
                              <td><?php echo $item->direc; ?></td>
                              <td>$ <?php echo number_format($item->monto_total,2,'.',',') ; ?></td>
                              <!--<td><?php echo $item->metodo; ?></td>-->
                              <td><?php echo $item->cliente; ?></td>
                              <td><?php if($item->cancelado==1){ echo '<span class="badge badge-danger">Cancelado</span>'; }
                                if($item->cancelado==2){ echo '<span class="badge badge-danger">Cancelado Parcial</span>'; } ?></td>
                              <td>
                                <button class="btn btn-raised gradient-blackberry white" onclick="ticket(<?php echo $item->id_venta; ?>)" title="Ticket" data-toggle="tooltip" data-placement="top">
                                  <i class="fa fa-book"></i>
                                </button>
                                <button class="btn btn-raised gradient-flickr white" onclick="cancelar(<?php echo $item->id_venta; ?>,<?php echo $item->monto_total; ?>)" title="Cancelar" data-toggle="tooltip" data-placement="top" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                                  <i class="fa fa-times"></i>
                                </button>
                                <button class="btn btn-success success-flickr white" onclick="detalles(<?php echo $item->id_venta; ?>)" title="detalles" data-placement="top" data-toggle="tooltip" data-target="#detalles_reparto" <?php if($item->cancelado==1){ echo 'disabled';} ?>>
                                  <i class="fa fa-eye"></i>
                                </button>
                                <button class="btn btn-success success-flickr white" onclick="pagarParcial(<?php echo $item->id_venta; ?>)" title="Abonar Pago" data-placement="top" data-toggle="tooltip" data-target="#abonar_pago" <?php if($item->cancelado==1 || $item->tipo_metodo!=4){ echo 'disabled';} ?>>
                                  <i class="fa fa-usd"></i>
                                </button>
                                <div>
                                  <label onclick="entregado(<?php echo $item->id_venta; ?>)">
                                    Entregado: 
                                    <input type="checkbox" value="1" id="entregado" class="entregado_<?php echo $item->id_venta; ?>" <?php if($item->id_ventaPD == $item->id_venta && $item->entregado==1) echo 'checked'; if($item->cancelado==1)echo 'disabled'; ?>>
                                  </label>
                                 
                                </div>
                                <div class="">
                                  <label >Pagado:</label>
                                  <input type="checkbox" value="1" id="pagado_<?php echo $item->id_venta; ?>" onclick="confirmacionPago('<?php echo $item->id_venta; ?>')" <?php if($item->id_venta == $item->id_ventaPD && $item->pagado==1) echo 'checked'; if($item->cancelado==1) echo 'disabled'; ?>>
                                </div>
                                <div class="col-md-8">
                                  <select class="form-control" id="repartidorasirnar" <?php if($item->cancelado==1){ echo 'disabled';} ?> onchange="asignarRepartidor(<?php echo $item->id_venta; ?>)">
                                    <option value="">Seleccione repartidor:</option>
                                    <?php 
                                    foreach ($edo_pedido as $x){ //echo "item: " .$x->id_venta;
                                      $select="";
                                      if(isset($item->id_repartidor) && $x->personalId==$item->id_repartidor){ $select="selected"; }
                                      //if($x->id_venta == $item->id_venta){ //echo "son iguales"; 
          
                                        echo "<option value='$x->personalId'$select>$x->nombre $x->apellidos</option>";
                                      //}

                                      /*else{ ?>
                                        <option value=""></option>
                                       <?php 
                                       foreach ($personal as $item) { ?>
                                           <option value="<?php echo $item->personalId;?>"><?php echo $item->nombre.' '.$item->apellidos;?></option>
                                        <?php }
                                     }*/
                                    } //foreach x 
                                    /*foreach ($personal as $item) { ?>
                                       <option value="<?php echo $item->personalId;?>"><?php echo $item->nombre.' '.$item->apellidos;?></option>
                                    <?php }*/
                                    ?>
                                    
                                  </select>
                                </div>
                              </td>
                            </tr>
                            
                          <?php } ?>
                              
                        </tbody>
                      </table>
                      <table class="table table-striped" id="data-tables2" style="display: none;width: 100%">
                        <thead>
                          <tr>
                            <th>Folio</th>
                            <th>Fecha Pedido</th>
                            <!--<th>Fecha Entrega</th>-->
                            <th>Dirección</th>
                            <th>Monto</th>
                            <!--<th>Metodo</th>-->
                            <th>Cliente</th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody id="tbodyresultadosvent2">
                        </tbody>
                        
                      </table>
                      <div class="col-md-12">
                        <div class="col-md-7">
                          
                        </div>
                        <div class="col-md-5">
                          <?php echo $this->pagination->create_links() ?>
                        </div>
                        
                      </div>
                          
                          
              <!--------//////////////-------->
                  </div>
          </div>
      </div>
    </div>
<!------------------------------------------------>
<style type="text/css">
    #iframereporte{
        background: white;
    }
    iframe{
        height: 500px;
        border:0;
        width: 100%;
    }
</style>
<div class="modal fade text-left" id="iframeri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">-->
            <div id="iframereporte"></div>
            <!--</div>-->
            <div class="modal-footer">
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade text-left" id="cancelar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!--<div class="modal-body">
                ¿Desea cancelar el ticket No. <span id="NoTicket"></span> por un total de <span id="CantidadTicket"></span>?
                      <input type="hidden" id="hddIdVenta">
            </div>-->
            <div class="modal-body">
                Articulos especificos para cancelar del ticket #<span id="NoTicket2"></span>: <br> 
                <div id="detVen"></div>
                <input type="hidden" id="hddIdVenta">
                <div class="col text-center">
                   <button type="button" id="sicancelar" class="btn btn-danger">Cancelar Todo</button>
                </div>  
            </div>
            <div class="modal-footer">
                <button type="button" id="sicancelar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id ="detalles_reparto" tabindex="-3" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title ddaa">Detalles de Reparto</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            
          </div>
          <div class="modal-body">
            <span>

            </span> 
            <table id='detalles' class='table' width="100%">
              <thead>
                <tr>
                  <th>Estatus</th>
                  <th>Fecha Entrega</th>
                  <th>Repartidor</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal" id="modalConf" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">CONFIRMACIÓN</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>¿Cambiar pedido a pagado?</p>
        <button type="button" class="btn btn-danger" onclick="Pagar(1)">SI</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="Pagar(0)">NO</button>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>


<!--<div class="modal fade text-left" id="abonar_pago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Abonos a cuenta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <p><span id="total_venta">Total:</span></p>
              <p><span id="txtabono">Abonado: $0.00</span></p>
              <p><span id="txtsaldo">Restante: </span></p>
            </div>
            <div class="modal-footer">
                <button type="button" id="sicancelar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>-->

<div class="modal fade text-left" id="modalAsig" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Asociar Pago</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" >
                <div class="row">
                    <div class="col-md-12" id="deta">

                    </div>
                </div>
                <div class="row marcoproductos">
                    <div class="col-md-12" id="cont_padre">
                        <form id="asociar_pago"> 
                            <input type="hidden" class="form-control" name="id_venta" id="id_venta">
                            <div class="cont_hijo">
                                <input type="hidden" class="form-control" name="id" id="id_pago" value="0">
                                <div class="col-md-4">
                                  <div class="form-group">
                                    <label class="label-control">Fecha: </label>
                                    <input type="date" class="form-control" name="fecha_pago" id="fecha_pago" value="<?php echo date('Y-m-d');?>">
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="label-control">Folio de Pago: </label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="folio_pago" id="folio_pago" value="">
                                        </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="label-control">Monto: </label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" name="cantidad" id="cantidad" value="">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="">    
                                  <div class="col-md-9">
                                    <div class="form-group">
                                        <label class="label-control">Observaciones: </label>
                                        <div class="col-md-12">
                                            <textarea id="observaciones" name="observaciones" class="form-control" value=""></textarea>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="color: transparent;">agregar pago</label>
                                        <button type="button" onclick="add_pago()" class="btn btn-success"><i class="fa fa-plus"></i> Otro Pago</button>
                                    </div>
                                  </div>
                                </div> 
                                  
                            </div>
                            
                        </form>

                    </div>
                    <div class="col-md-12" id="cont_padre_exist">
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                          <hr>
                            <center><button type="submit" class="btn grey btn-outline-primary" id="saveAsoP">Confirmar Nuevo Pago</button></center>
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="modal-footer">
                
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
  $(document).ready(function () {
    var table = $('#data-tables').dataTable({
      "bProcessing": true,
      "bPaginate":false,
      "searching": false,
      order: [0,"desc"]
    });
    $('[data-toggle="tooltip"]').tooltip();
    /*$('#sicancelar').click(function(){
      params = {};
      params.id = $('#hddIdVenta').val();
      $.ajax({
        type:'POST',
        url:'<?php echo base_url(); ?>ListaVentas/cancalarventa',
        data:params,
        async:false,
        success:function(data){
          toastr.success('Cancelado Correctamente','Hecho!');
          //$('#trven_'+params.id).remove();
          location.reload();
        }
      });
    });*/
    $('#sicancelar').click(function(){
      params = {};
      params.id = $('#hddIdVenta').val();
      $.ajax({
        type:'POST',
        url:'<?php echo base_url(); ?>ListaPedidos/cancelarPedido',
        data:params,
        async:false,
        beforeSend: function() {
          $("#sicancelar").prop('disabled',true); 
        },
        success:function(data){
          toastr.success('Pedido cancelado correctamente','Hecho!');
          //$('#trven_'+params.id).remove();
          location.reload();
        }
      });
    });
    $('#confCancel').click(function(){
        location.reload();
    });
    /*$('.asignarrepartidor').click(function(event) {
        var rowsselected = table.rows( { selected: true } ).count();
        if (rowsselected>0) {
            var DATA = [];
            table.rows( { selected: true } ).data().each(function(data) {
                item = {};
                item["foliov"] = data.folio;
                item["repartidor"] = $('#repartidorasirnar option:selected').val();
                DATA.push(item);       
            });
            INFO = new FormData();
            aInfo = JSON.stringify(DATA);
            INFO.append('data', aInfo);
            $.ajax({
                data: INFO,
                type: 'POST',
                url: base_url+'PedidoClientes/addrepartidor',
                processData: false,
                contentType: false,
                async: false,
                statusCode: {
                    404: function(data) {
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function() {
                        toastr.error('Error', '500');
                    }
                },
                success: function(data) {
                    console.log(data);
                    setTimeout(function(){ 
                        reloadtable();
                    }, 1000);
                    
                }
            });
        }else{
            toastr.error('Seleccione un pedido','Advertencia!');
        }
    });*/
    /*$('input').iCheck({
      checkboxClass: 'icheckbox_polaris',
      radioClass: 'iradio_polaris',
      increaseArea: '-10%' // optional
    }).change();*/
    $('#saveAsoP').click(function(event) {
      if($("#fecha_pago").val()!="" && $("#cantidad").val()!=""){
          var DATA  = [];
          if ($("#cont_padre #asociar_pago > .cont_hijo").length>0) {
              var TABLA   = $("#cont_padre #asociar_pago > .cont_hijo");                  
              TABLA.each(function(){         
                  item = {};
                  item ["id"] = $(this).find("input[id*='id']").val();
                  //item ["id_venta"] = $("#id_venta").val();
                  item ["fecha_pago"] = $(this).find("input[id*='fecha_pago']").val();
                  item ["folio_pago"] = $(this).find("input[id*='folio_pago']").val();
                  item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
                  item ["observaciones"] = $(this).find("textarea[id*='observaciones']").val();
                  DATA.push(item);
              });
          }if ($("#cont_padre #asociar_pago > .cont_hijo_clone").length>0) {
              var TABLA   = $("#cont_padre #asociar_pago > .cont_hijo_clone");                  
              TABLA.each(function(){         
                  item = {};
                  item ["id"] = $(this).find("input[id*='id']").val();
                  //item ["id_venta"] = $("#id_venta").val();
                  item ["fecha_pago"] = $(this).find("input[id*='fecha_pago']").val();
                  item ["folio_pago"] = $(this).find("input[id*='folio_pago']").val();
                  item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
                  item ["observaciones"] = $(this).find("textarea[id*='observaciones']").val();
                  DATA.push(item);
              });
          }
          if ($("#cont_padre_exist #asociar_pago > .cont_hijoExist").length>0) {
              var TABLA   = $("#cont_padre_exist #asociar_pago > .cont_hijoExist");                  
              TABLA.each(function(){         
                  item = {};
                  item ["id"] = $(this).find("input[id*='id']").val();
                  //item ["id_venta"] = $("#id_venta").val();
                  item ["fecha_pago"] = $(this).find("input[id*='fecha_pago']").val();
                  item ["folio_pago"] = $(this).find("input[id*='folio_pago']").val();
                  item ["cantidad"] = $(this).find("input[id*='cantidad']").val();
                  item ["observaciones"] = $(this).find("textarea[id*='observaciones']").val();
                  DATA.push(item);
              });
          }
          INFO  = new FormData();
          aInfo   = JSON.stringify(DATA);
          INFO.append('data', aInfo);
          $.ajax({
              data: INFO,
              type: 'POST',
              url: "<?php echo base_url(); ?>"+'ListaPedidos/submitPago/'+$("#id_venta").val(),
              processData: false, 
              contentType: false,
              async: false,
              statusCode:{
                  404: function(data2){
                      //toastr.error('Error!', 'No Se encuentra el archivo');
                  },
                  500: function(){
                      //toastr.error('Error', '500');
                  }
              },
              beforeSend: function(){
                  $("#saveAsoP").attr("disabled",true);
              },
              success: function (result) {
                  console.log(result);
                  $("#saveAsoP").attr("disabled",false);
                  $("#cont_hijo_clone").html('');
                  swal("Éxito!", "Se han guardado correctamente", "success");
                  $('#modalAsig').modal('hide');
                  location.reload();
                  //table.ajax.reload();
              }
          }); 
      }else{
        swal("Error!", "Ingrese fecha y/o monto de operación", "warning");
      }
            
    });

    $("#exportar_excel").on("click", function (){
      exportar();
    });
  });
  function ticket(id){
    $("#iframeri").modal();
    $('#iframereporte').html('<iframe src="<?php echo base_url(); ?>Ticket?id='+id+'&tipo=2"></iframe>');
  }
  /*function cancelar(ID,CantidadTicket){
    $('#cancelar').modal();
    $("#hddIdVenta").val(ID);
    $("#NoTicket").html(ID);
    $("#CantidadTicket").html(CantidadTicket);  
  }*/
  function cancelar(ID,CantidadTicket){
    $('#cancelar').modal();
    $("#hddIdVenta").val(ID);
    $("#NoTicket").html(ID);
    IDG = ID;
    $("#hddIdVenta2").val(ID);
    $("#NoTicket2").html(ID);
    $.ajax({
      type:'json',
      url: '<?php echo base_url(); ?>ListaPedidos/consultaDetallePedido/'+ID,
      async: false,
      statusCode:{
          404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
          500: function(){ toastr.error('Error', '500');}
      },
      success:function(data){
        //console.log("data: "+data);
        $('#detVen').html(data);
      }
    });
  }

   function cancelaParcial(id_det_ven){
    $.ajax({
        type:'POST',
        url: '<?php echo base_url(); ?>ListaVentas/cancelaParcial/'+id_det_ven+"/"+$('#hddIdVenta').val(),
        async: false,
        statusCode:{
            404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
            500: function(){ toastr.error('Error', '500');}
        },
        success:function(data){
          toastr.success('Cancelado Correctamente','Hecho!');
          $.ajax({
            type:'json',
            url: '<?php echo base_url(); ?>ListaPedidos/consultaDetallePedido/'+IDG,
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#detVen').html(data);
            }
          });
          //location.reload(); // falta poner estatus de cancelado
        }
    });
    //$("#CantidadTicket").html(CantidadTicket);  
  }

  function buscarventa(){
    var search=$('#buscarvent').val();
    if (search.length>2) {
        $.ajax({
            type:'POST',
            url: '<?php echo base_url(); ?>ListaPedidos/buscarvent',
            data: {
                buscar: $('#buscarvent').val(),
                bode:$('#personalbodega option:selected').val()
            },
            async: false,
            statusCode:{
                404: function(data){ toastr.error('Error!', 'No Se encuentra el archivo');},
                500: function(){ toastr.error('Error', '500');}
            },
            success:function(data){
                $('#tbodyresultadosvent2').html(data);
            }
        });
      $("#data-tables").css("display", "none");
        $("#data-tables2").css("display", "");
    }else{
        $("#data-tables2").css("display", "none");
        $("#data-tables").css("display", "");
    }
  }
  function seleccionar(){
    var bode=$('#personalbodega option:selected').val();
    var tipo=$('#tipo_venta option:selected').val();
    location.href="<?php echo base_url(); ?>ListaPedidos?bod="+bode+"&tipo="+tipo;
  }
  function imprimir(){
      window.print();
  }

  /*function abonar(id_venta,total){
    console.log("id_venta: "+id_venta);
    console.log("total: "+total);
    $('#abonar_pago').modal();
    $.ajax({
      url: '<?php echo base_url(); ?>Pedidos/pedidodatos',
      type: 'POST',
      data: { id: id_venta},
      success: function(data){ 
          var array2 = $.parseJSON(data);
          $('#total_venta').html("Total: " +total);
          $('#txtabono').html(array2.total_pagado);
          $('#txtsaldo').html(array2.cliente);
      }
    });
  }*/

  function pagarParcial(id){
    //console.log("id de venta: "+id)
    $("#id_venta").val(id);
    $("#folio_pago").val('');
    $("#observaciones").val('');
    $("#cantidad").val('');
    //$("#referencia").val('');
    //var id = $(this).data("id");
    $('#id_pedido').val(id);
    $('#modalAsig').modal(); 
    $.ajax({
        url: "<?php echo base_url(); ?>"+'ListaPedidos/asignar_pago',
        type: 'POST',
        data: { idPed: id },
        success: function(x){
          $("#deta").html(x);
            $("#id_pago").val('0');
            $("#fecha_pago").val();
            $("#folio_pago").val();
            $("#cantidad").val();
            $("#observaciones").val();
         },
        error: function(jqXHR,estado,error){
          $("#deta").html('Hubo un error: '+estado+' '+error);
        }
    });
    
    $.ajax({
        url: "<?php echo base_url(); ?>"+'ListaPedidos/verificar_pago',
        type: 'POST',
        data: { idPed: id },
        success: function(result){
          $("#cont_padre_exist").html(result);
        }
    });
}

function add_pago(){
  console.log("add liquida");
  $row=$(".cont_hijo").clone().attr('class','cont_hijo_clone');
  $row.find(".row").removeClass("row");
  $row.find("button").removeClass("bg-teal").removeClass("waves-effect").addClass("btn-danger").attr("onclick","remove_pago($(this))").find("i").attr('class','fa fa-minus').text(" quitar");
  $row.find("input").val("");
  $row.find("textarea").val("");
  $row.insertAfter(".cont_hijo");
}

function remove_pagoE(i,id,idped){
  /*title = "¿Desea eliminar este registro?";
  swal({
      title: title,
      text: "Se eliminará el pago!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Aceptar",
      closeOnConfirm: true
  }, function (isConfirm) {
    if (isConfirm) {
        console.log("confirmado");*/
        
      $.ajax({
        type:'POST',
        url: "<?php echo base_url(); ?>"+'ListaPedidos/eliminarPago',
        data:{ id: id, id_venta: idped},
        success:function(data){
          i.closest(".cont_hijoExist").remove();
          $("#modalAsig").modal('hide');//ocultamos el modal
          $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
          $('.modal-backdrop').remove();//eliminamos el backdrop del modal
          swal("Éxito", "Pago eliminado correctamente", "success");
          location.reload();
          //table.ajax.reload();
        }
      });   //cierra ajax
    /*}
  });*/
}

function remove_pago(i){
  i.closest(".cont_hijo_clone").remove();
}

function detalles(id_venta){
    $('#detalles_reparto').modal();
    table=$('#detalles').DataTable({
        responsive: true,
        destroy: true,
        "ajax": {
                "url": '<?php echo base_url(); ?>ListaPedidos/detalles_pedido',
                type: "POST",
                "dataSrc": '',
                data:{
                  venta: id_venta,
                },
            },
            "columns": [
                {"data": "esdo"},
                /*{"data": null,
                    "render" : function ( url, type, full) {
                        var msj='';
                        if(full['entregado']=="0"){
                          msj+= "Sin entregar";
                        }
                        else if(full['entregado']=="1"){
                          msj+= "Entregado"
                        }
                        else if(full['entregado']=="2"){
                          msj+= "En curso"
                        }
                        return msj;
                    }
                },*/
                {"data": "fecha_entrega"},
                {"data": "repartidor"}
            ],
            /*order: [[ 0, "desc" ]],
            select: {
                style: 'multi'
            }*/
    });   
}
function asignarRepartidor(ventaid){
    console.log('id repartidor: '+$("#repartidorasirnar").val());
    if (ventaid!="") {
        $.ajax({
            type:'POST',
            url: '<?php echo base_url(); ?>ListaPedidos/asignarRepartidor',
            data: {
                venta:ventaid,
                repartidor: $("#repartidorasirnar").val()
            },
            success:function(data){
              //location.reload(); 
            }
        });
    }
}
function confirmacionPago(id){
  idConf=id;
  $("#modalConf").modal('show');
}

function Pagar(pagado) {
  if(pagado==1){
    $('#pagado_'+idConf+'').prop('checked', true);
  }
  else{
    $('#pagado_'+idConf+'').prop('checked', false);  //fata saber de que pagado (venta)
  }
  
  $.ajax({
    url: "<?php echo base_url(); ?>/ListaPedidos/pagar",
    type: 'POST',
    data: {
      "id": idConf,
      "pago": pagado
    },
    success: function(data) {
      $("#modalConf").modal('hide');
      //table.ajax.reload();
      //swal("Éxito!", "Se ha cambiado el estatus pagado", "success");
      },
      error: function(jqXHR) {
        console.log(jqXHR);
      }
  });
}

function entregado(id) {
  var entregado="";
  if($('.entregado_'+id).is(':checked')){
    entregado = 1;
  }
  else {
    entregado = 0;
  }
  $.ajax({
    url: "<?php echo base_url(); ?>/ListaPedidos/entregado",
    type: 'POST',
    data: {
      "id": id,
      "entrega": entregado
    },
    success: function(data) {
      if(entregado==1){
        swal("Éxito!", "Pedido Entregado", "success");  
      } 
    },
    error: function(jqXHR) {
      console.log(jqXHR);
    }
  });
}

function exportar(){
  var bode=$('#personalbodega option:selected').val();
  var tipo=$('#tipo_venta option:selected').val();
  window.open("<?php echo base_url(); ?>ListaPedidos/exportarPedidos/"+bode+"/"+tipo+"", "_blank");
}
  
</script>