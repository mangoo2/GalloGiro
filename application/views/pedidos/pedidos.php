<!----------------------------------------------------------------------------------------=====-->
<style type="text/css">
	.bloques {
		cursor: pointer;
		width: 23%;
		padding-left: 1%;
		padding-right: 1%;
		float: left;
		margin: 1%;
	}

	.bloquecalcu {
		width: 30%;
		margin: 1%;
		padding: 10px;
		float: left;
		text-align: center;
		font-size: 20px;
		color: white;
		cursor: pointer;
	}

	.logobanana {
		filter: drop-shadow(5px 9px 12px #444);
	}

	.productos {
		max-width: 361px
		width: 80%;
	}

	.addproductoss {
		background: transparent;
		border: 0px !important;
	}

	.bloquecalcu:active {
		background-color: #c55d0d !important;
	}

	.ticketventa {
		width: 100%;
		border: 0px;
		height: 500px;
	}

	.tablenumber {
		background: white;
	}

	.numbertouch {
		font-size: 29px;
		padding-left: 35px;
		padding-right: 35px;
	}

	.numbertouch {
		font-size: 25px;
		padding-left: 35px;
		padding-right: 35px;
	}

	div#mlkeyboard {
		z-index: 99999999999999999999;
	}

	iframe {
		width: 100%;
		height: 560px;
		border: 0;
	}

	.table th, .table td {
		padding: 0.5rem;
	}

	#clientes {
		width: 350px;
	}

</style>
<input type="hidden" id="bodegauser" value="<?php echo $bodegauser; ?>">

<div class="row">
	<div class="col-md-6" style="text-align: center; ">
		<h2 class="masproductos" style="color:#2f58a6;cursor: pointer; font-size: 40px; font-weight: bold;">
			<b>Pedidos</b></h2>
	</div>
	<div class="col-md-6" style="text-align: center; ">
		<h2 class="barcodeSection" style="color:#2f58a6;cursor: pointer; font-size: 40px; font-weight: bold;"><b>Pedidos
				por Escaneo</b></h2>
	</div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				<h4 class="card-title">Pedidos de mostrador</h4>
			</div>
			<div class="card-body">
				<div class="card-block">
					<!--------//////////////-------->
					<!--<div class="row rowObservaciones panelventa4" style="display: none;">
                      <label class="font-medium-2 text-bold-600 ml-1">Observaciones de pedido</label>
                      <textarea class="form-control observaciones" name="observaciones" id="observaciones"></textarea>
                    </div>-->
					<br>
					<div class="row match-height panelventa1">
						<?php foreach ($categoriasll->result() as $item) {
							if ($item->img == '') {
								$imgcat = 'public/img/ops.png';
								$porcentajeimg = '75%';
							} else {
								$imgcat = 'public/img/categoria/' . $item->img;
								$porcentajeimg = '135%';
							}
							?>
							<div class="col-lg-3 col-md-3 col-sm-12 mb-2 classproducto"
								 data-idproducto="<?php echo $item->categoriaId; ?>"
								 data-cate="<?php echo $item->categoria; ?>">
								<div class="card card-inverse bg-info text-center catego" style="height: 300.906px; cursor: pointer;">
									<div class="card-body">
										<div class="card-img overlap"
											 style="background: url('<?php echo base_url(); ?><?php echo $imgcat; ?>');height: 220px;background-size: <?php echo $porcentajeimg; ?>;background-position:  center; background-repeat: no-repeat; ">
											<!--<img src="<?php echo base_url(); ?><?php echo $imgcat; ?>" alt="element 06" width="225" class="mb-1">-->
										</div>
										<div class="card-block">
											<h4 class="card-title"><?php echo $item->categoria; ?></h4>
											<!--<p class="card-text">Donut toffee candy brownie soufflé macaroon.</p>-->
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>
					<div class="row panelventa2" style="display: none;"></div>
					<div class="row panelventa3" style="display: none;"></div>
					<div class="row panelventa4" style="display: none;">
						<div class="col-md-12 classsventa">
						<table class="table col-md-12 text-center">
							<tr>
								<td class="col-md-3"></td>
								<td class="col-md-1"></td>
								<td class="col-md-3">Producto</td>
								<td class="col-md-3">Marca</td>
								<td class="col-md-2">Precio General</td>
							</tr>
							<tbody>
							<tr>
								<td class="col-md-3">
									<div class="col-md-1 selectimg">
									</div>
								</td>
								<td class="col-md-1" type="hidden">
									<input class="col-md-1" id="idproducto" width="1px" hidden="" />
								</td>
								<td class="col-md-3">
									<div class="col-md-1" id="selectproducto" style="max-width:none; !important;"></div>
								</td>
								<td class="col-md-3">
									<div class="col-md-2" id="selectmarca" style="max-width:none; !important;"></div>
								</td>
								<td class="col-md-2">
									<div class="col-md-2" id="selectcosto" style="max-width:none; !important;"></div>
								</td>
							</tr>
							</tbody>
						</table>
							<div id="datosImportante">
								<input type="number" id="precioadi" style="display: none;">
							</div>
							<!--<div class="col-md-2 selectpresentacion"></div>-->

						</div>
						<div class="bloque4 col-md-5">
							<div class="row">
								<!--<div class="controls col-md-12">
									<select class="form-control" name="clientes" id="clientes"
											onchange="reiniciaMetodo();cambia_direcc();" >
										<?php foreach ($clientedefault->result() as $item) {
											echo '<option value="' . $item->ClientesId . '">' . $item->Nom . '</option>';
										}
										?>
									</select>

								</div>-->
								<div class="col-md-12">
	                                <div class="form-group">
	                                    <select class="form-control custom-select" id="clientes" name="clientes">
	                                    </select>
	                                </div>
	                                <br>
	                            </div>
								<!--<div class="controls col-md-12">
									 <select class="form-control" name="clientedirecciones" id="clientedirecciones">
                                        <option value=''>Direccion:</option>
                                       
                                    </select>
								</div>-->
								<div class="controls col-md-12">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="cantidad"
											   id="cantidadcajas" name="cantidadcajas" aria-describedby="button-addon2"
											   title="Valor unitario">
										<!--<input type="text" class="form-control" placeholder="Kilogramos"
											   id="cantidadkilos" name="cantidadkilos" aria-describedby="button-addon2"
											   title="kilogramos" value="0">-->
										<div class="input-group-append">
                                    <span class="input-group-btn" id="button-addon2">
                                      <button class="btn btn-raised btn-primary" type="button" onclick="pulsadaenter()">Agregar</button>
                                    </span>
										</div>
									</div>

								</div>
								<div class="input-group col-md-12">
									<select class="form-control" name="metodopagos" id="metodopagos"
											onchange="verificaCredito(this.value);">
										<?php foreach ($metodopagos->result() as $item) { $select="";
											if($item->metodoId=="4") { $select="selected"; }
											echo '<option value="' . $item->metodoId . '" '.$select.'>' . $item->metodo . '</option>';
										}
										?>
									</select>
								</div>

								<div class="controls col-md-12 " style="display: none" name="divOpcionesCredito"
									 id="divOpcionesCredito">
									<select class="form-control col-md-4" name="opcionesCredito" id="opcionesCredito"
											onchange="muestraInputParcial(this.value);">
										<option value="1">Total</option>
										<option value="2">Parcial</option>
									</select>
									<label for="montoParcial"
										   class="font-medium-2 text-bold-600 ml-1 col-md-3">Monto: </label>
									<input type="text" name="montoParcial" id="montoParcial" style="display: none"
										   class="col-md-3 form-control">
									<br><br><br>
								</div>

							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group ">
										<input type="checkbox" id="ticketventa" class="switchery" data-size="lg"
											   data-on-text="SI" data-off-text="NO" checked/>
										<label for="ticketventa" class="font-medium-2 text-bold-600 ml-1">Ticket
											venta</label>

									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group ">
										<input type="checkbox" id="factura" class="switchery" data-size="lg"
											   data-on-text="SI" data-off-text="NO"/>
										<label for="factura" class="font-medium-2 text-bold-600 ml-1">Factura</label>

									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group ">
										<input type="checkbox" id="descuento" class="switchery" data-size="lg"
											   data-on-text="SI" data-off-text="NO" onchange="checheddescuento()"/>
										<label for="descuento"
											   class="font-medium-2 text-bold-600 ml-1">Descuento</label>

									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="descuento" class="font-medium-2 text-bold-600 ml-1">Fecha Entrega</label>
										<input type="date" value="<?php echo date('Y-m-d');?>" id="fecha_pedido" name="fecha_pedido" class="control-label"/>
										
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group divoptiondescuento" style="display: none">
										<select class="form-control " id="optiondescuento" onchange="calculartotal();">
											<option value="0"> 0 %</option>
											<option value="0.05"> 5 %</option>
											<option value="0.1"> 10 %</option>
											<option value="0.15"> 15 %</option>
										</select>

									</div>
								</div>

							</div>


							<div class="row">
								<div class="col-md-6">
									<div class="col-md-12 btn btn-raised gradient-crystal-clear white"
										 id="realizarventa">
										<i class="fa fa-shopping-cart fa-2x"></i> Registrar Pedido
									</div>
								</div>
								<div class="col-md-1"></div>
								<div class="col-md-5">
									<div class="col-md-12 btn btn-raised gradient-ibiza-sunset white"
										 data-toggle="modal" data-target="#modalconfirm">
										<i class="fa fa-ban"></i> Cancelar
									</div>
								</div>
							</div>

						</div>
						<div class="bloque4 col-md-7">
							<div class="row">
								<table class="table table-striped table-hover no-head-border" id="tableproductos" width="100%">
									<thead class="vd_bg-grey vd_white">
									<tr>
										<td type="hidden"></td>
										<td width="40%">Producto</td>
										<td width="20%">Marca</td>
										<td width="10%">Cantidad</td>
										<td width="10%">Precio</td>
										<td width="10%">Total</td>
										<td type="hidden"></td>
										<td width="10%">X</td>
									</tr>
									</thead>
									<tbody id="addproductos">

									</tbody>
								</table>
							</div>
							<div class="row" style="text-align: center;">
								<a class="btn btn-raised gradient-purple-bliss white btn-block masproductos"><i
										class="fa fa-plus fa-fw "></i></a>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-offset-6">
										<p>cuenta</p>
										<h2>$ <span id="totalpro">0.00</span></h2>
										<input type="hidden" id="totalprod" value="0">
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--------//////////////-------->
				</div>
			</div>
		</div>
	</div>
</div>


<input type="hidden" name="usuarioid" id="usuarioid" value="0">

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/jquery.ml-keyboard.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/demo.css">

<div class="modal fade" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header vd_bg-grey vd_white">
				<!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>-->
				<h4 class="modal-title" id="myModalLabel">Confirmar</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" id="prodelete">
					<div class="col-md-12">
						<h2>¿Desea confirmar la cancelacion?</h2>
					</div>
				</div>
			</div>
			<div class="modal-footer background-login">
				<button type="button" class="btn vd_btn vd_bg-red" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn vd_btn vd_bg-green" data-dismiss="modal" onclick="btncancelar()">
					Confirmar
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalimpresion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="false" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header vd_bg-grey vd_white">
				<!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>-->
				<h4 class="modal-title" id="myModalLabel">Ticket</h4>
			</div>
			<div class="modal-body" style="padding-top: 0px;padding-bottom: 0px;">
				<div class="row" id="iframeticket">

				</div>
			</div>
			<div class="modal-footer background-login">
				<button type="button" class="btn vd_btn vd_bg-green" data-dismiss="modal" onclick="btncancelar()">
					Cerrar
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalclientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="false" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header vd_bg-grey vd_white">
				<!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>-->
				<h4 class="modal-title" id="myModalLabel">Clientes</h4>
			</div>
			<div class="modal-body" style="padding-top: 0px;padding-bottom: 0px;">
				<div class="row">
					<div class="col-md-12">
						<input type="text" name="selectcliente" id="selectcliente" class="form-control"
							   placeholder="Buscar cliente" onInput="buscarclientes()" onChange="buscarclientes()">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="list-group" id="iframeclientes">

						</ul>
					</div>

				</div>
			</div>
			<div class="modal-footer background-login">
				<button type="button" class="btn vd_btn vd_bg-green" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="modal fade text-left" id="modalturno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
	 aria-hidden="false" data-keyboard="false" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Turno</h4>
				<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12" style="text-align: center;">
						<h2>A la espera de apertura de turno</h2>

					</div>

				</div>
			</div>
			<div class="modal-footer">
				<!--<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal" id="btnabrirt">Abrir turno</button>-->
			</div>
		</div>
	</div>
</div>
<div class="modal fade text-left" id="modaldatosfactura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
	 aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="myModalLabel1">Datos Factura</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<textarea id="datosfactura" data-rel="ckeditor" rows="3"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-raised gradient-green-tea white" data-dismiss="modal"
						id="savedatosfactura">Guardar
				</button>
				<button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/switchery.min.css">
<script src="<?php echo base_url(); ?>app-assets/vendors/js/switchery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>app-assets/js/switch.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>public/js/jquery.ml-keyboard.js"></script>
<script src="<?php echo base_url(); ?>public/js/demo.js"></script>
<script type="text/javascript">
	$(document).ready(function ($) {

		<?php if ($sturno == 0) { ?>
		$('#modalturno').modal();
		<?php  } ?>
	});
	<?php if ($sturno == 0) { ?>
	setTimeout(function () {
		//location.reload();
	}, 15000);// 5 segundos

	<?php  } ?>
</script>
