<!DOCTYPE html><html lang="es" class="loading">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
        <meta name="author" content="Mangoo agb">
        <title>Gallo Giro</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>public/img/gallogiro.jpg">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>public/img/gallogiro.jpg">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"><!--colocar el mismo color a la barra de estado superiori-->
        <meta name="description" content="Sistema de punto de venta">
        <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900|Montserrat:300,400,500,600,700,800,900" rel="stylesheet">

        <meta name="theme-color" content="#2f58a6"/>
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/sweetalert2.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/tables/datatable/datatables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/toastr.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chosen.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/balloon.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/pickadate/pickadate.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/js/formvalidation/formValidation.min3f0d.css?v2.2.0">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/app.css">  
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/core/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>app-assets/vendors/js/datatable/datatables.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/animate.css">
        
        <link rel="manifest" href="<?php echo base_url(); ?>manifest.json">
        <script async src="<?php echo base_url(); ?>public/js/jquery.numpad.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>public/css/jquery.numpad.css">
        
        <input type="hidden" name="ssessius" id="ssessius" value="<?php if(isset($_SESSION['usuarioid_tz'])){  echo $_SESSION['usuarioid_tz'];}?>" readonly>
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    </head>
    <body data-col="2-columns" class=" 2-columns ">
        <!-- ////////////////////////////////////////////////////////////////////////////-->
            <div class="wrapper">