
<style type="text/css">
  .vd_red{
    font-weight: bold;
    color: red;
  }
  .vd_green{
    color: #009688; 
  }
  .controls{
    margin-bottom: 10px;
  }
</style>

<div class="row">
  <div class="col-md-12">
    <h2>Presentación</h2>
  </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title"><?php if(isset($presentaciones)){ echo "Editar"; } else { echo "Agregar"; } echo " Presentación";?></h4>
          </div>
          <div class="card-body">
              <div class="card-block form-horizontal col-sm-12">
                  <!--------//////////////-------->
                  <div class="row">
                    <form method="post" role="form" id="form_presenta">
                      <?php if(isset($presentaciones)){
                          echo "<input name='id' value='$presentaciones->presentacionId' hidden />";
                        } 
                        ?>
                      <div class="col-md-12">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Presentacion</label>
                            <div class=" col-md-10 input-group">
                              <input type="text" class="form-control" id="presentacion" name="presentacion" <?php if(isset($presentaciones)) {?>value="<?php echo $presentaciones->presentacion; ?>"<?php } ?>>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="col-md-2 control-label">Unidad</label>
                            <div class=" col-md-10 input-group">
                              <input type="text" class="form-control" id="unidad" name="unidad" <?php if(isset($presentaciones)) {?>value="<?php echo $presentaciones->unidad; ?>"<?php } ?>>
                            </div>
                          </div>
                        </div>


                      </div> 

                    <div class="col-md-12">
                      <br>
                      <br>
                      <button id="SaveButton" type="submit" class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover" style="background: #2e58a6;" >
                        Guardar
                      </button>
                    </div>

                  </form>
                </div>
                  <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>
