<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<div class="row">
  <div class="col-md-12">
    <h2>Reporte de Productos</h2>
  </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Existencias</h4>
        </div>
        <div class="col-md-12">
          <div class="col-md-10">

          </div>
          <div class="col-md-2">
            <div class="form-group">
              <label style="color: transparent;" class="control-label">exportar excel</label>
              <button type="button" id="exportar_excel" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-file-excel-o"> </i> Exportar</button>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="card-block">
          <!--------//////////////-------->
            <table class="table table-striped" id=table_prods style="width: 100%">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Marca</th>
                  <th>Matriz</th>
                  <th>La Fragua</th>
                  <th>16 de Sep.</th>
                  <th>Acocota</th>
                  <th>Morillotla</th>
                  <th>Sonata</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              </table>
        <!--------//////////////-------->
            </div>
        </div>
    </div>
  </div>
</div>
<!------------------------------------------------>
