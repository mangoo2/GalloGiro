<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=edo_cuenta".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" class="table table-striped" id="data-tables" style="width: 100%">
    <tr>
      <th>Folio</th>
      <th>Fecha Venta</th>
      <th>Cliente</th>
      <th>Total de Venta</th>
      <th>Pagos</th>
      <th>Fecha de Pago</th>
      <th>Folio de Pago</th>
      <th>Observaciones</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($info as $item){ 
      if($item->estatus==1 || $item->estatus==null){ ?>
      <tr>
        <td><?php echo $item->id_venta; ?></td>
        <td><?php echo $item->reg; ?></td>
        <td><?php echo $item->Nom; ?></td>
        <?php
        $params["id"]=$item->id_venta;
        $datas=$this->ModeloReportes->getSumaVenta($params);
        $total_venta=0;
        foreach ($datas as $key) {
          $total_venta = $key->total_venta;
        } ?>
        <td>$ <?php echo number_format($total_venta,2,'.',','); ?></td>
        <td>$ <?php echo number_format($item->cantidad,2,'.',','); ?></td>
        <td><?php echo $item->fecha_pago; ?></td>
        <td><?php echo $item->folio_pago; ?></td>
        <td><?php echo $item->observaciones; ?></td>
      </tr>
      <?php } ?>
    <?php } ?>
  </tbody>
</table>