<?php
  require_once('TCPDF/tcpdf.php');

  $cambio=$vcambio;
  $pagacon=$vpagacon;
  /*foreach ($configticket->result() as $item){
    if($_SESSION['bodega_tz'] == $item->sucursal){
      $GLOBALS['titulo'] = $item->titulo;
      $GLOBALS['mensaje'] = $item->mensaje;
      $GLOBALS['mensaje2'] = $item->mensaje2;
      $fuente = $item->fuente;
      $GLOBALS['tamano'] = $item->tamano;
      $GLOBALS['margensup'] = $item->margensup;
    }
  }*/
  
  $GLOBALS['titulo'] = $titulo;
  $GLOBALS['mensaje'] = $mensaje;
  $GLOBALS['mensaje2'] = $mensaje2;
  $fuente = $fuente;
  $GLOBALS['tamano'] = $tamano;
  $GLOBALS['margensup'] = $margensup;
  
  //foreach ($getventas->result() as $item){
    $GLOBALS['idticket']= $idticket;

    $GLOBALS['monto_total'] = $monto_total;
    $GLOBALS['descuento'] = $descuento;
    $GLOBALS['cliente'] = $cliente;
    $GLOBALS['cancelado'] = $cancelado;

    $reg = $reg;
  //}

  $GLOBALS['tipo'] = $tipo;
  $GLOBALS['metodo'] = $metodo;

  $GLOBALS['direccion'] = $direccion;
  $GLOBALS['fecha_ped'] = $fecha_ped;
  
  $GLOBALS['fecha']= date("d-m-Y",strtotime($reg));
  $GLOBALS['hora']= date("G:i",strtotime($reg));
  if ($fuente==1) {
    $GLOBALS['tipofuente'] = "arial";
  }
  if ($fuente==2) {
    $GLOBALS['tipofuente'] = "Times New Roman";
  }
  if ($fuente==3) {
    $GLOBALS['tipofuente'] = "Open Sans";
  }
  if ($fuente==4) {
    $GLOBALS['tipofuente'] = "Calibri";
  }

//=======================================================================================
class MYPDF extends TCPDF {
  //Page header
  public function Header() {
    if($GLOBALS['cancelado']==1){
      // get the current page break margin
      $bMargin = $this->getBreakMargin();
      // get current auto-page-break mode
      $auto_page_break = $this->AutoPageBreak;
      // disable auto-page-break
      $this->SetAutoPageBreak(false, 0);
      // set bacground image
      $img_file = base_url().'public/img/canceladot.png';
      $this->Image($img_file, 2, 40, 55, 45, '', '', '', false, 300, '', false, false, 0);
      // restore auto-page-break status
      $this->SetAutoPageBreak($auto_page_break, $bMargin);
      // set the starting point for the page content
      $this->setPageMark();

      // get the current page break margin
      $bMargin = $this->getBreakMargin();
      // get current auto-page-break mode
      $auto_page_break = $this->AutoPageBreak;
      // disable auto-page-break
      $this->SetAutoPageBreak(false, 0);
      // set bacground image
      $img_file = base_url().'public/img/canceladot.png';
      $this->Image($img_file, 2, 135, 55, 45, '', '', '', false, 300, '', false, false, 0);
      // restore auto-page-break status
      $this->SetAutoPageBreak($auto_page_break, $bMargin);
      // set the starting point for the page content
      $this->setPageMark();
    
    }   
  }
    // Page footer
  public function Footer() {
      $html = ' 
      <table width="100%" border="0">
        <tr>
          <td align="right" class="footerpage">Pagina '.$this->getAliasNumPage().' de '.$this->getAliasNbPages().'</td>
        </tr>
      </table>';
      //$this->writeHTML($html, true, false, true, false, '');
  }
} 
//$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(58, 210), true, 'UTF-8', false);
//$medidas = array(58, 210); // Ajustar los milimetros necesarios;
$medidas = array(58, 240);
$pdf = new MYPDF('P', 'mm', $medidas, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('agb');
$pdf->SetTitle('Ticket');
$pdf->SetSubject('Ticket');
$pdf->SetKeywords('Ticket');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('3', '5', '3');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(false, 0);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 9);
// add a page
$pdf->AddPage();


$imglogo = base_url().'public/img/gallogiro.jpg';
      $html_ho = '
        <style type="text/css">
            .table{text-align:center; font-size:'.$GLOBALS['tamano'].'px; font-family: '.$GLOBALS['tipofuente'].'; }
        </style> 
        <table class="table" width="100%" border="0">
          <tr>
            <td colspan="3"><img src="'.$imglogo.'" width="70px" ></td>
          </tr>
          <tr>
            <th colspan="3">'.$GLOBALS['titulo'].'</th>
          </tr>
          <tr>
              <th colspan="3"></th>
          </tr>
          <tr>
              <th colspan="3">Cliente: '.$GLOBALS['cliente'].'</th>
          </tr>';
          //echo $tipo;
          if($GLOBALS['tipo']==2){
            $html_ho .= '
            <tr>
                <th colspan="3">Fecha Pedido: '.$GLOBALS['fecha_ped'].'</th>
            </tr>';
          }
          $html_ho .= '<tr>
              <th colspan="3">Fecha emisión: '.$GLOBALS['fecha'].'</th>
          </tr>
          <tr>
              <th colspan="3">Hora emisión: '.$GLOBALS['hora'].'</th>
          </tr>
          <tr>
              <th colspan="3">No ticket: '.$GLOBALS['idticket'].'</th>
          </tr>
          <tr>
              <th colspan="3">Este ticket es: Original</th>
          </tr>
        </table>';
        //if($GLOBALS['cancelado']==1) $html_ho.='<img class="imgCancelt" src="'.base_url().'public/img/canceladot.png">';

          
          $html_hc = '
          <style type="text/css">
            .table{text-align:center; font-size:'.$GLOBALS['tamano'].'px; font-family: '.$GLOBALS['tipofuente'].'; }
          </style> 
          <div style="border-top: 1px dashed black;"></div>
          <table width="100%" border="0" class="table">
            <tr>
              <td colspan="3"><img src="'.$imglogo.'" width="70px" ></td>
            </tr>
            <tr>
              <th colspan="3">'.$GLOBALS['titulo'].'</th>
            </tr>
            <tr>
                <th colspan="3"></th>
            </tr>
            <tr>
                <th colspan="3">Cliente: '.$GLOBALS['cliente'].'</th>
            </tr>';
            //echo $tipo;
            if($GLOBALS['tipo']==2){
              $html_hc .= '
              <tr>
                  <th colspan="3">Fecha Pedido: '.$GLOBALS['fecha_ped'].'</th>
              </tr>';
            }
            $html_hc .= '<tr>
                <th colspan="3">Fecha emisión: '.$GLOBALS['fecha'].'</th>
            </tr>
            <tr>
                <th colspan="3">Hora emisión: '.$GLOBALS['hora'].'</th>
            </tr>
            <tr>
                <th colspan="3">No ticket: '.$GLOBALS['idticket'].'</th>
            </tr>
            <tr>
                <th colspan="3">Este ticket es: Copia</th>
            </tr>
          </table>';
          //if($GLOBALS['cancelado']==1) $html_hc.='<img class="imgCancelt" src="'.base_url().'public/img/canceladot.png">';

$html='<style type="text/css">
        .table{text-align:center; font-size:6px; font-family: '.$GLOBALS['tipofuente'].'; }
        .txt5{text-align:center; font-size:5px; font-family: '.$GLOBALS['tipofuente'].'; font-weight: bold; }
        .txt6{text-align:center; font-size:6px; font-family: '.$GLOBALS['tipofuente'].'; font-weight: bold; }
        .txt7{text-align:center; font-size:7px; font-family: '.$GLOBALS['tipofuente'].'; font-weight: bold; }
        .txt8{text-align:center; font-size:8px; font-family: '.$GLOBALS['tipofuente'].'; font-weight: bold; }
        .txt9{text-align:center; font-size:9px; font-family: '.$GLOBALS['tipofuente'].'; font-weight: bold; }
      </style> 
        <table border="0" class="table">
          <thead>
            <tr>
              <th width="20%">Cant.</th>
              <th width="45%">Artículo</th>
              <th width="35%">Precio U.</th>
            </tr>
          </thead>
          <tbody>';
$subtotal=0;
foreach ($getventasd->result() as $rowEmp){
  if ($rowEmp->kilos>0) {
    $rowkilos = $rowEmp->kilos.' kg';
    $kilossub=$rowkilos;
  }else{
    $rowkilos='';
    $kilossub=1;
  }
  //echo $rowEmp->status;

  if($rowEmp->status==0){
    $html .= '
            <tr class="txt8">
              <th width="20%" align="left">'.$rowEmp->cantidad.' Kg</th>
              <th width="45%" align="left">'.$rowEmp->categoria.' '.$rowEmp->marca.' -CANCELADO-</th>
              <th width="35%">$ '.number_format($rowEmp->precio,2,'.',',').'</th>
            </tr>
            ';
            //$subtotal=$subtotal+($rowEmp->cantidad*$kilossub*$rowEmp->total);
            //$subtotal=$subtotal+$rowEmp->total;
  }else{
    $html .= '
            <tr class="txt8">
              <th width="20%" align="left">'.$rowEmp->cantidad.' Kg</th>
              <th width="45%" align="left">'.$rowEmp->categoria.' '.$rowEmp->marca.'</th>
              <th width="35%">$ '.number_format($rowEmp->precio,2,'.',',').'</th>
            </tr>
            ';
            //$subtotal=$subtotal+($rowEmp->cantidad*$kilossub*$rowEmp->total);
            $subtotal=$subtotal+($rowEmp->precio*$rowEmp->cantidad);
  }
}
/*
$htmll .='<tr>
                    <th style="font-size: 7" align="center"></th>
                    <th style="font-size: 7"  align="center">Subtotal</th>
                    <th style="font-size: 6" colspan="3" align="right">$ '.number_format($subtotal,2,'.',',').'</th>
                </tr>';
                */
if ($GLOBALS['descuento']!=0) {
  $descuento =$GLOBALS['descuento'];
  $descuento=$descuento*100;
  $html .='<tr>
              <th style="font-size: 10px;"></th>
              <th style="font-size: 10px;">Descuento</th>
              <th style="font-size: 9px;" align="right">'.$descuento.' %</th>
          </tr>';
}
$html .= '
            <tr>
              <th style="font-size: 10px;"></th>
              <th style="font-size: 10px;"><b>Total</b></th>
              <th style="font-size: 9px;">$ '.number_format($subtotal,2,'.',',').'</th>
            </tr>
            <tr class="txt9">
              <th></th>
              <th>Su pago:</th>';
              if($pagacon!=""){
                $html .= '<th>$'.number_format($pagacon,2,".",",").'</th>';
              }else{
                $html .= '<th></th>';
              }
                
            $html.='</tr>
            <tr class="txt9">
              <th></th>
              <th>Cambio:</th>';
              if($cambio!=""){
                $html .= '<th>$'.number_format($cambio,2,".",",").'</th>';
              }else{
                $html .= '<th></th>';
              }
  
            $html.='</tr>
            <tr class="txt9">
              <th><br></th>
              <th>Método:</th>
              <th>'.$GLOBALS['metodo'].'</th>
            </tr>
            </tbody>
          </table>
          <style type="text/css">
            .table{text-align:center; font-size:'.$GLOBALS['tamano'].'px; font-family: '.$GLOBALS['tipofuente'].'; }
          </style>
          <table border="0" class="table">  
            <tr>
              <th></th>
            </tr>
            <tr>
              <th colspan="3">'.$GLOBALS['mensaje'].'</th>
            </tr>
            <tr>
              <th>'.$GLOBALS['mensaje2'].'</th>
            </tr>
            <tr>
              <th><b>SICOI V 3.1 HECHO POR WWW.MANGOO.MX</b></th>
            </tr>
          </table>';

$pdf->writeHTML($html_ho, true, false, true, false, '');
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->writeHTML($html_hc, true, false, true, false, '');
//$pdf->writeHTML($html_ho, true, false, true, false, '');
$pdf->writeHTML($html, true, false, true, false, '');

//$pdf->AddPage();
//$pdf->writeHTML($html, true, false, true, false, '');

//$pdf->IncludeJS('print(true);'); //abrir pantalla automatica de imprimir
$pdf->Output('Captura.pdf', 'I');

//$pdf->Output('files/'.$GLOBALS["carpeta"].'/facturas/'.$GLOBALS["rrfc"].'_'.$GLOBALS["Folio"].'.pdf', 'F');
