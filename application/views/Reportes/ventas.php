<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<div class="row">
  <div class="col-md-12">
    <h2>Reporte de Ventas - Productos</h2>
  </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
      <div class="card-header">
          <h4 class="card-title">Ventas - Productos</h4>
      </div>
      <div class="col-md-12">
        <div class="col-md-3">
          <div class="form-group">
            <label class="control-label">Desde:</label>
              <input id="fechai" class="form-control date-picker" size="16" type="date" />
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label class="control-label">Hasta:</label>
              <input id="fechaf" class="form-control date-picker" size="16" type="date" />
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label"><i class="fa fa-shopping-basket"></i> Sucursal:</label>
            <select class="form-control custom-select" id="id_sucursal">
              <option value="0">Todas</option>
              <option value="1">Matriz</option>
              <option value="2">La Fragua</option>
              <option value="3">16 de Sep.</option>
              <option value="4">Acocota</option>
              <option value="5">Morillotla</option>
              <option value="6">Sonata</option>
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label style="color: transparent;" class="control-label">exportar excel</label>
            <button type="button" id="exportar_excel" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-file-excel-o"> </i> Exportar</button>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="card-block">
          <!--------//////////////-------->
          <table class="table table-striped" id=table_prodsv style="width: 100%">
            <thead>
              <tr>
                <!--<th># Venta</th>-->
                <th>Producto</th>
                <th>Cantidad</th>
                <th>Precio U.</th>
                <th>Total</th>
                <!--<th>Metodo</th>
                <th>Sucursal</th>-->
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr>
                <td style="text-align: right;" colspan="3"><b>Total:</b></td>
                <td></td>
              </tr>
            </tfoot>
          </table>
      <!--------//////////////-------->
          </div>
      </div>
    </div>

    <div class="card">
      <div class="card-header">
          <h4 class="card-title">Pedidos - Productos Clientes</h4>
      </div>
      <div class="col-md-12">
        <div class="col-md-3">
          <div class="form-group">
            <label class="control-label">Desde:</label>
              <input id="fechaip" class="form-control date-picker" size="16" type="date" />
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <label class="control-label">Hasta:</label>
              <input id="fechafp" class="form-control date-picker" size="16" type="date" />
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label"><i class="fa fa-shopping-basket"></i> Sucursal:</label>
            <select class="form-control custom-select" id="id_sucursal_pp">
              <option value="0">Todas</option>
              <option value="1">Matriz</option>
              <option value="2">La Fragua</option>
              <option value="3">16 de Sep.</option>
              <option value="4">Acocota</option>
              <option value="5">Morillotla</option>
              <option value="6">Sonata</option>
            </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label class="control-label"><i class="fa fa-users"></i> Cliente:</label>
            <select class="form-control custom-select" id="id_cliente">
              <option value="0">Todos</option>
            </select>
          </div>
        </div>
        <div class="col-md-2">
          <div class="form-group">
            <label style="color: transparent;" class="control-label">exportar excel</label>
            <button type="button" id="exportar_excel_pp" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-file-excel-o"> </i> Exportar</button>
          </div>
        </div>
      </div>
      <div class="card-body">
        <div class="card-block">
          <table class="table table-striped" id=table_prod_ped style="width: 100%">
            <thead>
              <tr>
                <th>Producto</th>
                <th>Cantidad</th>
                <th>Precio U.</th>
                <th>Total</th>
                <th>Cliente</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr>
                <td style="text-align: right;" colspan="3"><b>Total:</b></td>
                <td></td>
                <td ></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<!------------------------------------------------>
