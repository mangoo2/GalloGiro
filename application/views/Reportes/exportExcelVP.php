<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=ventas_prods".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" class="table table-striped" id="data-tables" style="width: 100%">
  <thead>
    <tr>
      <th>Producto</th>
      <th>Cantidad</th>
      <th>Precio U.</th>
      <th>Total</th>
    </tr>
  </thead>
  <tbody>
    <?php $tot_fin=0; foreach ($e as $d){ $tot_fin=$tot_fin+$d->total_prods;?>
      <tr>
        <td><?php echo $d->categoria." ".$d->marca; ?></td>
        <td><?php echo round($d->totalc,2); ?></td>
        <td>$ <?php echo number_format($d->precio,2,'.',','); ?></td>
        <td>$ <?php echo number_format($d->total_prods,2,'.',','); ?></td>
      </tr>
    <?php } ?>
  </tbody>
  <tfoot>
    <tr>
      <td style="text-align: right;" colspan="3"><b>Total:</b></td>
      <td>$ <?php echo number_format($tot_fin,2,'.',','); ?></td>
    </tr>
  </tfoot>
</table>