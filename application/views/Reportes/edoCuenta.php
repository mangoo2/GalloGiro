<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/print.css" media="print">
<div class="row">
  <div class="col-md-12">
    <h2>Reporte de Cuentas</h2>
  </div>
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Ventas a credito </h4>
        </div>
        <div class="col-md-12 inputbusquedas">
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Desde:</label>
                <input id="txtInicio" name="txtInicio" class="form-control date-picker" size="16" type="date" />
            </div>
          </div>
          <div class="col-md-3">
            <div class="form-group">
              <label class="control-label">Hasta:</label>
                <input id="txtFin" name="txtFin" class="form-control date-picker" size="16" type="date" />
            </div>
          </div>
          <div class="col-md-5">
            <div class="form-group">
              <label class="control-label"><i class="fa fa-user"></i> Cliente:</label>
              <select class="form-control custom-select" id="cliente" name="cliente">
              </select>
            </div>
          </div>
          <div class="col-md-3">
            <div class="checkbox-list">
              <label> <input title="Click si es por todos los clientes" type="checkbox" id="todos_cli"> Todos los Clientes </label>
            </div>
          </div>
          <!--<div class="col-md-1">
            <div class="form-group">

            </div>
          </div>-->
          <div class="col-md-5">
            <div class="form-group">
              <label class="control-label"><i class="fa fa fa-shopping-cart"></i> Mercado:</label>
              <select class="form-control custom-select" id="id_mercado" name="id_mercado">
              </select>
            </div>
          </div>
          <div class="col-md-5">
          </div>
          <!--<div class="col-md-4">
            <div class="checkbox-list">
              <br>
              <label> <input title="Click si es por todos los clientes" type="checkbox" id="todos_cli"> Todos los Clientes </label>
            </div>
          </div>-->
          <div class="col-md-2">
            <div class="form-group">
              <label style="color: transparent;" class="control-label">exportar excel</label>
              <button type="button" id="exportar_excel" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-file-excel-o"> </i> Exportar</button>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="card-block">
          <!--------//////////////-------->
            <table class="table table-striped" id=table_edos style="width: 100%">
              <thead>
                <tr>
                  <th>Folio</th>
                  <th>Fecha Venta</th>
                  <th>Cliente</th>
                  <!--<th>Total</th>-->
                  <th>Pagos</th>
                  <!--<th>Restante</th>-->
                </tr>
              </thead>
              <tbody>
              </tbody>
              </table>
        <!--------//////////////-------->
            </div>
        </div>
    </div>
  </div>
</div>
<!------------------------------------------------>
