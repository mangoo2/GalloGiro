<?php 
header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=reporte_prods".date('Y-m-d').".xls");
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" class="table table-striped" id="data-tables" style="width: 100%">
    <tr>
      <th>#</th>
      <th>Nombre</th>
      <th>Marca</th>
      <th>Matriz</th>
      <th>La Fragua</th>
      <th>16 de Sep.</th>
      <th>Acocota</th>
      <th>Morillotla</th>
      <th>Sonata</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($info as $item){ ?>
      <tr>
        <td><?php echo $item->productoaddId; ?></td>
        <td><?php echo $item->categoria; ?></td>
        <td><?php echo $item->marca; ?></td>
        <td><?php echo $item->stok; ?></td>
        <td><?php echo $item->stok2; ?></td>
        <td><?php echo $item->stok3; ?></td>
        <td><?php echo $item->stok4; ?></td>
        <td><?php echo $item->stok5; ?></td>
        <td><?php echo $item->stok6; ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>