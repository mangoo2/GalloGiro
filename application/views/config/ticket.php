<?php
$data = array();
foreach ($configticket->result() as $item){
  array_push($data, $item); 
  if($sucursal_session == $item->sucursal){
        $id_ticket = $item->id_ticket;
        $titulo = $item->titulo;
        $mensaje = $item->mensaje;
        $mensaje2 = $item->mensaje2;
        $fuente = $item->fuente;
        $tamano = $item->tamano;
        $margensup = $item->margensup;
        $sucursal = $item->sucursal;
      } 
    }
?>
<div class="row">
    <div class="col-md-12">
      <h2>Ticket </h2>
    </div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
<div class="col-sm-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Configuracion</h4>
        </div>
        <div class="card-body">
            <div class="card-block">
                <!--------//////////////-------->
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <div class="form-group">
                          <label class="col-sm-2 control-label">Sucursal:</label>
                          <div class="col-sm-10 controls">
                            <select id="sucursal" class="form-control">
                              <option value="1" <?php if ($sucursal==1) { echo "selected"; } ?>>Matriz</option>
                              <option value="2" <?php if ($sucursal==2) { echo "selected"; } ?>>La Fragua</option>
                              <option value="3" <?php if ($sucursal==3) { echo "selected"; } ?>>16 de Sep.</option>
                              <option value="4" <?php if ($sucursal==4) { echo "selected"; } ?>>Acocota</option>
                              <option value="5" <?php if ($sucursal==5) { echo "selected"; } ?>>Morillotla</option>
                              <option value="6" <?php if ($sucursal==6) { echo "selected"; } ?>>Sonata</option>
                            </select>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Titulo:</label>
                            <div class="col-sm-10 controls">
                              <textarea id="titulo" name="titulo" class="form-control" rows="3"><?php echo $titulo; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mensaje A:</label>
                            <div class="col-sm-10 controls">
                              <textarea id="mensaje1" name="mensaje1" class="form-control"><?php echo $mensaje; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mensaje B:</label>
                            <div class="col-sm-10 controls">
                              <textarea id="mensaje2" name="mensaje2" class="form-control"><?php echo $mensaje2; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Fuente:</label>
                          <div class="col-sm-8 controls">
                            <select id="fuente" class="form-control">
                              <option value="1" <?php if ($fuente==1) { echo "selected"; } ?> style="font-family: 'arial';">Arial</option>
                              <option value="2" <?php if ($fuente==2) { echo "selected"; } ?> style="font-family: Times New Roman;">Times New Roman</option>
                              <option value="3" <?php if ($fuente==3) { echo "selected"; } ?> style="font-family: 'Open Sans';">Open Sans</option>
                              <option value="4" <?php if ($fuente==4) { echo "selected"; } ?> style="font-family: 'Calibri'">Calibri</option>
                            </select>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Tamaño:</label>
                          <div class="col-sm-8 controls">
                            <select id="tamano" class="form-control">
                              <option value="7" <?php if ($tamano=='7') { echo "selected"; } ?>  style="font-size: 7px;">7</option>
                              <option value="8" <?php if ($tamano=='8') { echo "selected"; } ?> style="font-size: 8px;">8</option>
                              <option value="9" <?php if ($tamano=='9') { echo "selected"; } ?> style="font-size: 9px;">9</option>
                              <option value="10" <?php if ($tamano=='10') { echo "selected"; } ?> style="font-size: 10px;">10</option>
                              <option value="11" <?php if ($tamano=='11') { echo "selected"; } ?> style="font-size: 11px;">11</option>
                              <option value="12" <?php if ($tamano=='12') { echo "selected"; } ?> style="font-size: 12px;">12</option>
                              <option value="13" <?php if ($tamano=='13') { echo "selected"; } ?> style="font-size: 13px;">13</option>
                              <option value="14" <?php if ($tamano=='14') { echo "selected"; } ?> style="font-size: 14px;">14</option>
                            </select>
                          </div>  
                        </div>
                    </div>
                    <div class="col-md-6 mb-2">
                        <div class="form-group">
                          <label class="col-sm-4 control-label">Margen Superior:</label>
                          <div class="col-sm-8 controls">
                            <select id="margsup" class="form-control">
                              <option value="1" <?php if ($margensup=='1') { echo "selected"; } ?> >1</option>
                              <option value="2" <?php if ($margensup=='2') { echo "selected"; } ?> >2</option>
                              <option value="3" <?php if ($margensup=='3') { echo "selected"; } ?> >3</option>
                              <option value="4" <?php if ($margensup=='4') { echo "selected"; } ?> >4</option>
                              <option value="5" <?php if ($margensup=='5') { echo "selected"; } ?> >5</option>
                              <option value="6" <?php if ($margensup=='6') { echo "selected"; } ?> >6</option>
                              <option value="7" <?php if ($margensup=='7') { echo "selected"; } ?> >7</option>
                              <option value="8" <?php if ($margensup=='8') { echo "selected"; } ?> >8</option>
                              <option value="9" <?php if ($margensup=='9') { echo "selected"; } ?> >9</option>
                              <option value="10" <?php if ($margensup=='10') { echo "selected"; } ?> >10</option>
                              <option value="11" <?php if ($margensup=='11') { echo "selected"; } ?> >11</option>
                              <option value="12" <?php if ($margensup=='12') { echo "selected"; } ?> >12</option>
                              
                            </select>
                            
                          </div>  
                        </div>
                      </div>
                    <div class="col-md-12 mb-2">
                      <br>
                      <br>
                      <button class="btn btn-raised gradient-purple-bliss white" id="guardar" style="background: #2e58a6;" ><i class="icon-ok"></i> Guardar</button>
                    </div>
                    
                </div>
                
                <!--------//////////////-------->
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    var base_url=$("#base_url").val();
    $(document).ready(function(){

    $('#guardar').click(function(){
        params = {};
        params.titulo = $('#titulo').val();
        params.mensaje1 = $('#mensaje1').val();
        params.mensaje2 = $('#mensaje2').val();
        params.fuente = $('#fuente option:selected').val();
        params.tamano = $('#tamano option:selected').val();
        params.margsup = $('#margsup option:selected').val();
        params.sucursal = $('#sucursal option:selected').val();
        $.ajax({
            type:'POST',
            url: base_url+'Config_ticket/updateticket',
            data:params,
            async:false,
            beforeSend: function(){
              $("#guardar").attr("disabled",true);
            },
            success:function(data){
                //console.log(data);
                toastr.success('Guardado Correctamente','Hecho!');
                setTimeout(function () {
                  window.location.reload();
                }, 1500);
            }
        });
    });

    $('#sucursal').change(function reload_data(){
      current_sucursal = $('#sucursal option:selected').val();
      <?php foreach ($data as $dato){ ?>
        if (current_sucursal == <?php echo $dato->sucursal; ?>){
          $('#titulo').val(<?php echo json_encode($dato->titulo);?>);
          $('#mensaje1').val(<?php echo json_encode($dato->mensaje);?>);
          $('#mensaje2').val(<?php echo json_encode($dato->mensaje2);?>);
          $('#fuente').val(<?php echo json_encode($dato->fuente);?>);
          $('#tamano').val(<?php echo json_encode($dato->tamano);?>);
          $('#margsup').val(<?php echo json_encode($dato->margensup);?>);
          //$('#sucursal option:selected').val(<?php echo json_encode($dato->sucursal);?>);
        }
      <?php }?>
    });

    
});
</script>

