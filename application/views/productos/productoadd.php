<?php
if (isset($_GET['id'])) {
	$id = $_GET['id'];
	$label = 'Editar Producto';
	$personalv = $this->ModeloProductos->getproducto($id);
	foreach ($personalv->result() as $item) {
		$id = $item->productoaddId;
		$productoId = $item->productoId;
		$precompra = $item->precompra;
		$MarcaId = $item->MarcaId;
		$codigoProducto = $item->codigoProducto;
	}
	$personald = $this->ModeloProductos->getproductodll($id, 1);
	foreach ($personald->result() as $item) {
		$subId = $item->subId;
		$option = '<option value="' . $item->presentacionId . '">' . $item->presentacion . '</option>';
		$PresentaId = $item->PresentacionId;
		$stok = $item->stok;
		$precio = $item->precio;
		$cantidad_mm = $item->cantidad_mm;
		$precio_mm = $item->precio_mm;
		$cantidad_m = $item->cantidad_m;
		$precio_m = $item->precio_m;

		$stok2 = $item->stok2;
		$precio2 = $item->precio2;
		$cantidad_mm2 = $item->cantidad_mm2;
		$precio_mm2 = $item->precio_mm2;
		$cantidad_m2 = $item->cantidad_m2;
		$precio_m2 = $item->precio_m2;

		$stok3 = $item->stok3;
		$precio3 = $item->precio3;
		$cantidad_mm3 = $item->cantidad_mm3;
		$precio_mm3 = $item->precio_mm3;
		$cantidad_m3 = $item->cantidad_m3;
		$precio_m3 = $item->precio_m3;

		$stok4 = $item->stok4;
		$precio4 = $item->precio4;
		$cantidad_mm4 = $item->cantidad_mm4;
		$precio_mm4 = $item->precio_mm4;
		$cantidad_m4 = $item->cantidad_m4;
		$precio_m4 = $item->precio_m4;

		$stok5 = $item->stok5;
		$precio5 = $item->precio5;
		$cantidad_mm5 = $item->cantidad_mm5;
		$precio_mm5 = $item->precio_mm5;
		$cantidad_m5 = $item->cantidad_m5;
		$precio_m5 = $item->precio_m5;

		$stok6 = $item->stok6;
		$precio6 = $item->precio6;
		$cantidad_mm6 = $item->cantidad_mm6;
		$precio_mm6 = $item->precio_mm6;
		$cantidad_m6 = $item->cantidad_m6;
		$precio_m6 = $item->precio_m6;
	}
} else {
	$label = 'Nuevo producto';
	$id = 0;
	$productoId = '';
	$precompra = 0;
	$codigoProducto = 0;
	$MarcaId = '';
	$subId = 0;
	$option = '';
	$stok = '';
	$precio = '';
	$cantidad_mm = '';
	$precio_mm = '';
	$cantidad_m = '';
	$precio_m = '';
	$stok2 = '';
	$precio2 = '';
	$cantidad_mm2 = '';
	$precio_mm2 = '';
	$cantidad_m2 = '';
	$precio_m2 = '';
	$stok3 = '';
	$precio3 = '';
	$cantidad_mm3 = '';
	$precio_mm3 = '';
	$cantidad_m3 = '';
	$precio_m3 = '';
	$stok4 = '';
	$precio4 = '';
	$cantidad_mm4 = '';
	$precio_mm4 = '';
	$cantidad_m4 = '';
	$precio_m4 = '';
	$stok5 = '';
	$precio5 = '';
	$cantidad_mm5 = '';
	$precio_mm5 = '';
	$cantidad_m5 = '';
	$precio_m5 = '';
	$stok6 = '';
	$precio6 = '';
	$cantidad_mm6 = '';
	$precio_mm6 = '';
	$cantidad_m6 = '';
	$precio_m6 = '';
}
?>
<style type="text/css">
	.vd_red {
		font-weight: bold;
		color: red;
		margin-bottom: 5px;
	}

	.vd_green {
		color: #009688;
	}

	input, select, textarea {
		margin-bottom: 15px;
	}

	.preprecio_0,
	.precantidad_1_0, .precantidad_1_1, .precantidad_1_2, .precantidad_1_3, .precantidad_1_4, .precantidad_1_5,
	.preprecio_1_0, .preprecio_1_1, .preprecio_1_2, .preprecio_1_3, .preprecio_1_4, .preprecio_1_5,
	.precantidad_2_0, .precantidad_2_1, .precantidad_2_2, .precantidad_2_3, .precantidad_2_4, .precantidad_2_5,
	.preprecio_2_0, .preprecio_2_1, .preprecio_2_2, .preprecio_2_3, .preprecio_2_4, .preprecio_2_5,
	.precantidad_3_0, .precantidad_3_1, .precantidad_3_2, .precantidad_3_3, .precantidad_3_4, .precantidad_3_5,
	.preprecio_3_0, .preprecio_3_1, .preprecio_3_2, .preprecio_3_3, .preprecio_3_4, .preprecio_3_5 {
		max-width: 140px;
	}

	.prepreciom_0,
	.prepreciom_1_0, .prepreciom_1_1, .prepreciom_1_2, .prepreciom_1_3, .prepreciom_1_4, .prepreciom_1_5,
	.prepreciom_2_0, .prepreciom_2_1, .prepreciom_2_2, .prepreciom_2_3, .prepreciom_2_4, .prepreciom_2_5,
	.prepreciom_3_0, .prepreciom_3_1, .prepreciom_3_2, .prepreciom_3_3, .prepreciom_3_4, .prepreciom_3_5,
	.prepreciomm_0,
	.prepreciomm_1_0, .prepreciomm_1_1, .prepreciomm_1_2, .prepreciomm_1_3, .prepreciomm_1_4, .prepreciomm_1_5,
	.prepreciomm_2_0, .prepreciomm_2_1, .prepreciomm_2_2, .prepreciomm_2_3, .prepreciomm_2_4, .prepreciomm_2_5,
	.prepreciomm_3_0, .prepreciomm_3_1, .prepreciomm_3_2, .prepreciomm_3_3, .prepreciomm_3_4, .prepreciomm_3_5 {
		max-width: 147px;
	}

	.apartirm_0,
	.apartirm_1_0, .apartirm_1_1, .apartirm_1_2, .apartirm_1_3, .apartirm_1_4, .apartirm_1_5,
	.apartirm_2_0, .apartirm_2_1, .apartirm_2_2, .apartirm_2_3, .apartirm_2_4, .apartirm_2_5,
	.apartirm_3_0, .apartirm_3_1, .apartirm_3_2, .apartirm_3_3, .apartirm_3_4, .apartirm_3_5,
	.apartirmm_0,
	.apartirmm_1_0, .apartirmm_1_1, .apartirmm_1_2, .apartirmm_1_3, .apartirmm_1_4, .apartirmm_1_5,
	.apartirmm_2_0, .apartirmm_2_1, .apartirmm_2_2, .apartirmm_2_3, .apartirmm_2_4, .apartirmm_2_5,
	.apartirmm_3_0, .apartirmm_3_1, .apartirmm_3_2, .apartirmm_3_3, .apartirmm_3_4, .apartirmm_3_5 {
		max-width: 100px;
	}
</style>
<input type="hidden" name="productoid" id="productoid" value="<?php echo $id; ?>">
<div class="row">
	<div class="col-md-12">
		<h2>Producto</h2>
	</div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header">
				<h4 class="card-title"><?php echo $label; ?></h4>
			</div>
			<div class="card-body">
				<div class="card-block form-horizontal">
					<!--------//////////////-------->
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-2 control-label">Producto:</label>
								<div class=" col-md-4">
									<select class="form-control" id="pcategoria" name="pcategoria">
										<?php foreach ($categorias->result() as $item) { ?>
											<option
												value="<?php echo $item->categoriaId; ?>" <?php if ($item->categoriaId == $productoId) {
												echo 'selected';
											} ?> ><?php echo $item->categoria; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-2 control-label">Marca:</label>
								<div class=" col-md-4">
									<select class="form-control" id="pmarca" name="pmarca">
										<?php foreach ($marcaall->result() as $item) { ?>
											<option
												value="<?php echo $item->marcaid; ?>" <?php if ($item->marcaid == $MarcaId) {
												echo 'selected';
											} ?> ><?php echo $item->marca; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-2 control-label">Presentación:</label>
								<div class=" col-md-4">
									<select class="form-control" id="PresentacionId" name="PresentacionId">
										<?php foreach ($presentacion as $item) { ?>
											<option
												value="<?php echo $item->presentacionId; ?>" <?php if ($item->presentacionId == $PresentaId) {
												echo 'selected';
											} ?> ><?php echo $item->presentacion; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-2 control-label">Precio Compra:</label>
								<div class=" col-md-4">
									<input type="number" class="form-control" id="pcompra" name="pcompra"
										   value="<?php echo $precompra ?>">
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label class="col-md-2 control-label">Codigo de Producto:</label>
								<div class=" col-md-4">
									<input type="text" class="form-control" id="codigoProducto" name="codigoProducto"
										   value="<?php echo $codigoProducto ?>">
								</div>
							</div>
						</div>


						<div class="col-md-12"><br><br></div>
						<div class="col-md-12 table-responsive">
							<table class="table table-hover " id="productosv">
								<thead>
								<tr>
									<th><label>Sucursal</label></th>
									<th><label>Stock</label></th>
									<th><label>Precio</label></th>
									<th><label>a partir medio mayoreo</label></th>
									<th><label>Precio medio mayoreo</label></th>
									<th><label>a partir mayoreo</label></th>
									<th><label>Precio mayoreo</label></th>
									<th></th>
								</tr>
								</thead>
								<tbody id="divaddproductos">
								<tr class="classproducto_0">
									<td>

									<input type="hidden" id="ppresentacionid" value="<?php echo $subId; ?>">
									<?php if($_SESSION['perfilid_tz']==1){ ?>
										<div class="form-group input-group mb-3">
                      <span class="input-group-addon">
                        MATRIZ
                      </span>
										</div>
										<div class="form-group input-group mb-3">
	                    <span class="input-group-addon">
	                      LA FRAGUA
	                    </span>
										</div>
										<div class="form-group input-group mb-3">
                      <span class="input-group-addon">
                        16 de Sep.
                      </span>
                    </div>

										<div class="form-group input-group mb-3">
                      <span class="input-group-addon">
                        ACOCOTA
                      </span>
										</div>
										<div class="form-group input-group mb-3">
											<span class="input-group-addon">
												MORILLOTLA
											</span>
										</div>
										<div class="form-group input-group mb-3">
											<span class="input-group-addon">
												SONATA
											</span>
										</div>
                      <?php }
                      else if($_SESSION['bodega_tz']==1){ ?>
                      	<div class="form-group input-group mb-3">
                          <span class="input-group-addon">
                            MATRIZ
                          </span>
												</div>
                      <?php } 
                      if($_SESSION['bodega_tz']==2){ ?>
                         <div class="form-group input-group mb-3">
                            <span class="input-group-addon">
                              LA FRAGUA
                            </span>
												</div>
                      <?php }	
                      if($_SESSION['bodega_tz']==3){ ?>
                        <div class="form-group input-group mb-3">
                          <span class="input-group-addon">
                            16 de Sep.
                          </span>
                        </div>
											<?php }	

                      if($_SESSION['bodega_tz']==4){ ?>
												<div class="form-group input-group mb-3">
                          <span class="input-group-addon">
                            Acocota
                          </span>
                        </div>
											<?php }	

                      if($_SESSION['bodega_tz']==5){ ?>
												<div class="form-group input-group mb-3">
                          <span class="input-group-addon">
                            Morillotla	
                          </span>
                        </div>
											<?php }	

                      if($_SESSION['bodega_tz']==6){ ?>
												<div class="form-group input-group mb-3">
                          <span class="input-group-addon">
                            Sonata
                          </span>
                        </div>
                    	<?php } ?> 
                          <!--<div class="form-group input-group mb-3">
                            <span class="input-group-addon">
                              16 DE SEPTIEMBRE
                            </span>
                          </div>
                          <div class="form-group input-group mb-3">
                            <span class="input-group-addon">
                              SONATA
                            </span>
                          </div>
                          <div class="form-group input-group mb-3">
                            <span class="input-group-addon">
                              AMALUCAN
                            </span>
                          </div>-->
									</td>
									<td>
										<?php if($_SESSION['perfilid_tz']==1){ ?>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="precantidad1" value="<?php echo $stok; ?>"
												   class="form-control precantidad_1_0">
											<span class="input-group-addon">Kg</span>
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="precantidad2" value="<?php echo $stok2; ?>"
												   class="form-control precantidad_2_0">
											<span class="input-group-addon">Kg</span>
										</div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="precantidad3" value="<?php echo $stok3; ?>" 
													class="form-control precantidad_3_0" >
                      <span class="input-group-addon">Kg</span>
	                  </div>

										<div class="form-group input-group mb-3">
											<input type="number" name="" id="precantidad4" value="<?php echo $stok4; ?>"
												  class="form-control precantidad_3_0">
											<span class="input-group-addon">Kg</span>
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="precantidad5" value="<?php echo $stok5; ?>"
												  class="form-control precantidad_3_0">
											<span class="input-group-addon">Kg</span>
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="precantidad6" value="<?php echo $stok6; ?>"
													class="form-control precantidad_3_0">
											<span class="input-group-addon">Kg</span>
										</div>

	                    <?php }
	                     else if($_SESSION['bodega_tz']==1){ ?>
	                      <div class="form-group input-group mb-3">
												<input type="number" name="" id="precantidad1" value="<?php echo $stok; ?>"
													   class="form-control precantidad_1_0">
												<span class="input-group-addon">Kg</span>
												</div>
	                   <?php } 
                      if($_SESSION['bodega_tz']==2){ ?>
                          <div class="form-group input-group mb-3">
												<input type="number" name="" id="precantidad2" value="<?php echo $stok2; ?>"
													   class="form-control precantidad_2_0">
												<span class="input-group-addon">Kg</span>
											</div>
                        <?php }	
                        if($_SESSION['bodega_tz']==3){ ?>
                          <div class="form-group input-group mb-3">
                                <input type="number" name="" id="precantidad3" value="<?php echo $stok3; ?>" class="form-control precantidad_3_0" >
                                <span class="input-group-addon">Kg</span>
                          </div>
                      <?php } 

                        if($_SESSION['bodega_tz']==4){ ?>
                          <div class="form-group input-group mb-3">
                            <input type="number" name="" id="precantidad4" value="<?php echo $stok4; ?>" 
															class="form-control precantidad_4_0" >
                            <span class="input-group-addon">Kg</span>
                          </div>
											<?php }	
                        if($_SESSION['bodega_tz']==5){ ?>
                          <div class="form-group input-group mb-3">
                            <input type="number" name="" id="precantidad5" value="<?php echo $stok5; ?>" 
															class="form-control precantidad_5_0" >
                            <span class="input-group-addon">Kg</span>
                          </div>
                      <?php } 
                        if($_SESSION['bodega_tz']==6){ ?>
                          <div class="form-group input-group mb-3">
                            <input type="number" name="" id="precantidad6" value="<?php echo $stok6; ?>" 
															class="form-control precantidad_6_0" >
                            <span class="input-group-addon">Kg</span>
                          </div>
                      <?php } ?> 
	                                      <!--<div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="precantidad4" value="<?php echo $stok4; ?>" class="form-control precantidad_3_0" >
	                                            <span class="input-group-addon">Kg</span>
	                                      </div>
	                                      <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="precantidad5" value="<?php echo $stok5; ?>" class="form-control precantidad_3_0" >
	                                            <span class="input-group-addon">Kg</span>
	                                      </div>
	                                      <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="precantidad6" value="<?php echo $stok6; ?>" class="form-control precantidad_3_0">
	                                            <span class="input-group-addon">Kg</span>
	                                      </div>-->
									</td>
									<td>
										<?php if($_SESSION['perfilid_tz']==1){ ?>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="preprecio" value="<?php echo $precio; ?>"
												   class="form-control preprecio_0">
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="preprecio2" value="<?php echo $precio2; ?>"
												   class="form-control preprecio_0">
										</div>
										<div class="form-group input-group mb-3">
	                    <input type="number" name="" id="preprecio3" value="<?php echo $precio3; ?>" 
												class="form-control preprecio_0">
	                  </div>

										<div class="form-group input-group mb-3">
											<input type="number" name="" id="preprecio4" value="<?php echo $precio4; ?>"
												   class="form-control preprecio_0">
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="preprecio5" value="<?php echo $precio5; ?>"
												   class="form-control preprecio_0">
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="preprecio6" value="<?php echo $precio6; ?>"
												   class="form-control preprecio_0">
										</div>
	                                    <?php }
	                                    else if($_SESSION['bodega_tz']==1){ ?>
	                                    	<div class="form-group input-group mb-3">
												<input type="number" name="" id="preprecio" value="<?php echo $precio; ?>"
													   class="form-control preprecio_0">
											</div>
	                                    <?php } 
	                                    if($_SESSION['bodega_tz']==2){ ?>
	                                        <div class="form-group input-group mb-3">
												<input type="number" name="" id="preprecio2" value="<?php echo $precio2; ?>"
													   class="form-control preprecio_0">
											</div>
	                                    <?php }	
	                                    if($_SESSION['bodega_tz']==3){ ?>
		                                    <div class="form-group input-group mb-3">
		                                        <input type="number" name="" id="preprecio3" value="<?php echo $precio3; ?>" class="form-control preprecio_0">
		                                    </div>
		                                <?php }	

	                                    if($_SESSION['bodega_tz']==4){ ?>
		                                    <div class="form-group input-group mb-3">
		                                        <input type="number" name="" id="preprecio4" value="<?php echo $precio4; ?>" 
																						class="form-control preprecio_0">
		                                    </div>
		                                <?php } 

	                                    if($_SESSION['bodega_tz']==5){ ?>
		                                    <div class="form-group input-group mb-3">
		                                        <input type="number" name="" id="preprecio5" value="<?php echo $precio5; ?>" 
																						class="form-control preprecio_0">
		                                    </div>
		                                <?php } 

	                                    if($_SESSION['bodega_tz']==6){ ?>
		                                    <div class="form-group input-group mb-3">
		                                        <input type="number" name="" id="preprecio6" value="<?php echo $precio6; ?>" 
																						class="form-control preprecio_0">
		                                    </div>
		                                <?php } ?> 
																		
                                                <!--  <div class="form-group input-group mb-3">
                                                    <input type="number" name="" id="preprecio4" value="<?php echo $precio4; ?>" class="form-control preprecio_0">
                                                  </div>
                                                  <div class="form-group input-group mb-3">
                                                    <input type="number" name="" id="preprecio5" value="<?php echo $precio5; ?>" class="form-control preprecio_0">
                                                  </div>
                                                  <div class="form-group input-group mb-3">
                                                    <input type="number" name="" id="preprecio6" value="<?php echo $precio6; ?>" class="form-control preprecio_0">
                                                  </div>-->
									</td>
									<td>
										<?php if($_SESSION['perfilid_tz']==1){ ?>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="apartirmm"
												   value="<?php echo $cantidad_mm; ?>" class="form-control apartirmm_0">
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="apartirmm2"
												   value="<?php echo $cantidad_mm2; ?>"
												   class="form-control apartirmm_0">
										</div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="apartirmm3" value="<?php echo $cantidad_mm3; ?>" 
												class="form-control apartirmm_0">
                    </div>

										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="apartirmm4" value="<?php echo $cantidad_mm4; ?>" 
												class="form-control apartirmm_0">
                    </div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="apartirmm5" value="<?php echo $cantidad_mm5; ?>" 
												class="form-control apartirmm_0">
                    </div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="apartirmm6" value="<?php echo $cantidad_mm6; ?>" 
												class="form-control apartirmm_0">
                    </div>
                                        <?php }
	                                    else if($_SESSION['bodega_tz']==1){ ?>
	                                    	<div class="form-group input-group mb-3">
												<input type="number" name="" id="apartirmm"
													   value="<?php echo $cantidad_mm; ?>" class="form-control apartirmm_0">
											</div>
	                                    <?php } 
	                                    if($_SESSION['bodega_tz']==2){ ?>
	                                        <div class="form-group input-group mb-3">
												<input type="number" name="" id="apartirmm2"
													   value="<?php echo $cantidad_mm2; ?>"
													   class="form-control apartirmm_0">
											</div>
	                                    <?php }	
	                                    if($_SESSION['bodega_tz']==3){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="apartirmm3" value="<?php echo $cantidad_mm3; ?>" class="form-control apartirmm_0">
	                                        </div>
		                                <?php } 

	                                    if($_SESSION['bodega_tz']==4){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="apartirmm3" value="<?php echo $cantidad_mm4; ?>" class="form-control apartirmm_0">
	                                        </div>
		                                <?php } 
	                                    if($_SESSION['bodega_tz']==5){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="apartirmm3" value="<?php echo $cantidad_mm5; ?>" class="form-control apartirmm_0">
	                                        </div>
		                                <?php } 
	                                    if($_SESSION['bodega_tz']==6){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="apartirmm3" value="<?php echo $cantidad_mm6; ?>" class="form-control apartirmm_0">
	                                        </div>
		                                <?php } ?> 

                                                  <!--<div class="form-group input-group mb-3">
                                                  <input type="number" name="" id="apartirmm4" value="<?php echo $cantidad_mm4; ?>" class="form-control apartirmm_0">
                                                  </div>
                                                  <div class="form-group input-group mb-3">
                                                  <input type="number" name="" id="apartirmm5" value="<?php echo $cantidad_mm5; ?>" class="form-control apartirmm_0">
                                                  </div>
                                                  <div class="form-group input-group mb-3">
                                                  <input type="number" name="" id="apartirmm6" value="<?php echo $cantidad_mm6; ?>" class="form-control apartirmm_0">
                                                  </div>-->
									</td>
									<td>
										<?php if($_SESSION['perfilid_tz']==1){ ?>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="prepreciomm"
												   value="<?php echo $precio_mm; ?>" class="form-control prepreciomm_0">
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="prepreciomm2"
												   value="<?php echo $precio_mm2; ?>"
												   class="form-control prepreciomm_0">
										</div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="prepreciomm3" value="<?php echo $precio_mm3; ?>" class="form-control prepreciomm_0">
                    </div>

										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="prepreciomm4" value="<?php echo $precio_mm4; ?>" class="form-control prepreciomm_0">
                    </div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="prepreciomm5" value="<?php echo $precio_mm5; ?>" class="form-control prepreciomm_0">
                    </div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="prepreciomm6" value="<?php echo $precio_mm6; ?>" class="form-control prepreciomm_0">
                    </div>

                                        <?php }
	                                    else if($_SESSION['bodega_tz']==1){ ?>
	                                    	<div class="form-group input-group mb-3">
												<input type="number" name="" id="prepreciomm"
													   value="<?php echo $precio_mm; ?>" class="form-control prepreciomm_0">
											</div>
	                                    <?php } 
	                                    if($_SESSION['bodega_tz']==2){ ?>
	                                        <div class="form-group input-group mb-3">
												<input type="number" name="" id="prepreciomm2"
													   value="<?php echo $precio_mm2; ?>"
													   class="form-control prepreciomm_0">
											</div>
	                                    <?php }	
	                                    if($_SESSION['bodega_tz']==3){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="prepreciomm3" value="<?php echo $precio_mm3; ?>" class="form-control prepreciomm_0">
	                                        </div>
		                                <?php } 
	                                    if($_SESSION['bodega_tz']==4){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="prepreciomm3" value="<?php echo $precio_mm4; ?>" class="form-control prepreciomm_0">
	                                        </div>
		                                <?php } 
	                                    if($_SESSION['bodega_tz']==5){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="prepreciomm3" value="<?php echo $precio_mm5; ?>" class="form-control prepreciomm_0">
	                                        </div>
		                                <?php } 
	                                    if($_SESSION['bodega_tz']==6){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="prepreciomm3" value="<?php echo $precio_mm6; ?>" class="form-control prepreciomm_0">
	                                        </div>
		                                <?php } ?>
                                                  <!--<div class="form-group input-group mb-3">
                                                  <input type="number" name="" id="prepreciomm4" value="<?php echo $precio_mm4; ?>" class="form-control prepreciomm_0">
                                                  </div>
                                                  <div class="form-group input-group mb-3">
                                                  <input type="number" name="" id="prepreciomm5" value="<?php echo $precio_mm5; ?>" class="form-control prepreciomm_0">
                                                  </div>
                                                  <div class="form-group input-group mb-3">
                                                  <input type="number" name="" id="prepreciomm6" value="<?php echo $precio_mm6; ?>" class="form-control prepreciomm_0">
                                                  </div>-->
									</td>
									<td>
										<?php if($_SESSION['perfilid_tz']==1){ ?>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="apartirm0"
												   value="<?php echo $cantidad_m; ?>" class="form-control apartirm_0">
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="apartirm2"
												   value="<?php echo $cantidad_m2; ?>" class="form-control apartirm_0">
										</div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="apartirm3" value="<?php echo $cantidad_m3; ?>" 
												class="form-control apartirm_0">
                    </div>  

										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="apartirm4" value="<?php echo $cantidad_m4; ?>" 
												class="form-control apartirm_0">
                    </div>  
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="apartirm5" value="<?php echo $cantidad_m5; ?>" 
												class="form-control apartirm_0">
                    </div>  
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="apartirm6" value="<?php echo $cantidad_m6; ?>" 
												class="form-control apartirm_0">
                    </div>  
                                        <?php }
	                                    else if($_SESSION['bodega_tz']==1){ ?>
	                                    	<div class="form-group input-group mb-3">
												<input type="number" name="" id="apartirm0"
													   value="<?php echo $cantidad_m; ?>" class="form-control apartirm_0">
											</div>
	                                    <?php } 
	                                    if($_SESSION['bodega_tz']==2){ ?>
	                                        <div class="form-group input-group mb-3">
												<input type="number" name="" id="apartirm2"
													   value="<?php echo $cantidad_m2; ?>" class="form-control apartirm_0">
											</div>
	                                    <?php }	
	                                    if($_SESSION['bodega_tz']==3){ ?>
		                                    <div class="form-group input-group mb-3">
	                                        	<input type="number" name="" id="apartirm3" value="<?php echo $cantidad_m3; ?>" class="form-control apartirm_0">
	                                        </div>
		                                <?php } 
																	
	                                    if($_SESSION['bodega_tz']==4){ ?>
		                                    <div class="form-group input-group mb-3">
	                                        	<input type="number" name="" id="apartirm3" value="<?php echo $cantidad_m4; ?>" class="form-control apartirm_0">
	                                        </div>
		                                <?php } 
																	
	                                    if($_SESSION['bodega_tz']==5){ ?>
		                                    <div class="form-group input-group mb-3">
	                                        	<input type="number" name="" id="apartirm3" value="<?php echo $cantidad_m5; ?>" class="form-control apartirm_0">
	                                        </div>
		                                <?php } 
																	
	                                    if($_SESSION['bodega_tz']==6){ ?>
		                                    <div class="form-group input-group mb-3">
	                                        	<input type="number" name="" id="apartirm3" value="<?php echo $cantidad_m6; ?>" class="form-control apartirm_0">
	                                        </div>
		                                <?php } ?>
                                                <!--  <div class="form-group input-group mb-3">
                                                    <input type="number" name="" id="apartirm4" value="<?php echo $cantidad_m4; ?>" class="form-control apartirm_0">
                                                  </div>  
                                                  <div class="form-group input-group mb-3">
                                                    <input type="number" name="" id="apartirm5" value="<?php echo $cantidad_m5; ?>" class="form-control apartirm_0">
                                                  </div>  
                                                  <div class="form-group input-group mb-3">
                                                    <input type="number" name="" id="apartirm6" value="<?php echo $cantidad_m6; ?>" class="form-control apartirm_0">
                                                  </div> -->
									</td>
									<td>
										<?php if($_SESSION['perfilid_tz']==1){ ?>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="prepreciom0"
												   value="<?php echo $precio_m; ?>" class="form-control prepreciom_0">
										</div>
										<div class="form-group input-group mb-3">
											<input type="number" name="" id="prepreciom2"
												   value="<?php echo $precio_m2; ?>" class="form-control prepreciom_0">
										</div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="prepreciom3" value="<?php echo $precio_m3; ?>" 
												class="form-control prepreciom_0">
                    </div>

										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="prepreciom4" value="<?php echo $precio_m4; ?>" 
												class="form-control prepreciom_0">
                    </div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="prepreciom5" value="<?php echo $precio_m5; ?>" 
												class="form-control prepreciom_0">
                    </div>
										<div class="form-group input-group mb-3">
                      <input type="number" name="" id="prepreciom6" value="<?php echo $precio_m6; ?>" 
												class="form-control prepreciom_0">
                    </div>
                                        <?php }
	                                    else if($_SESSION['bodega_tz']==1){ ?>
	                                    	<div class="form-group input-group mb-3">
												<input type="number" name="" id="prepreciom0"
													   value="<?php echo $precio_m; ?>" class="form-control prepreciom_0">
											</div>
	                                    <?php } 
	                                    if($_SESSION['bodega_tz']==2){ ?>
	                                        <div class="form-group input-group mb-3">
												<input type="number" name="" id="prepreciom2"
													   value="<?php echo $precio_m2; ?>" class="form-control prepreciom_0">
											</div>
	                                    <?php }	
	                                    if($_SESSION['bodega_tz']==3){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="prepreciom3" value="<?php echo $precio_m3; ?>" class="form-control prepreciom_0">
	                                        </div>
		                                <?php } 

	                                    if($_SESSION['bodega_tz']==4){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="prepreciom3" value="<?php echo $precio_m4; ?>" class="form-control prepreciom_0">
	                                        </div>
		                                <?php } 
																	
	                                    if($_SESSION['bodega_tz']==5){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="prepreciom3" value="<?php echo $precio_m5; ?>" class="form-control prepreciom_0">
	                                        </div>
		                                <?php } 
																	
	                                    if($_SESSION['bodega_tz']==6){ ?>
		                                    <div class="form-group input-group mb-3">
	                                            <input type="number" name="" id="prepreciom3" value="<?php echo $precio_m6; ?>" class="form-control prepreciom_0">
	                                        </div>
		                                <?php } ?> 
                                                <!--  <div class="form-group input-group mb-3">
                                                    <input type="number" name="" id="prepreciom4" value="<?php echo $precio_m4; ?>" class="form-control prepreciom_0">
                                                  </div>  
                                                  <div class="form-group input-group mb-3">  
                                                    <input type="number" name="" id="prepreciom5" value="<?php echo $precio_m5; ?>" class="form-control prepreciom_0">
                                                  </div>  
                                                  <div class="form-group input-group mb-3">  
                                                    <input type="number" name="" id="prepreciom6" value="<?php echo $precio_m6; ?>" class="form-control prepreciom_0">
                                                  </div> -->
										<input type="hidden" id="tipo" value="1"></td>
									<td></td>
								</tr>
								<?php
								if (isset($_GET['id'])) {
									$id = $_GET['id'];
									$personald = $this->ModeloProductos->getproductodll($id, 0);
									foreach ($personald->result() as $item) {
										$subId = $item->subId;
										$option = '<option value="' . $item->presentacionId . '">' . $item->presentacion . '</option>';

										$unidad = $item->unidad;

										$stok = $item->stok;
										$precio = $item->precio;
										$cantidad_mm = $item->cantidad_mm;
										$precio_mm = $item->precio_mm;
										$cantidad_m = $item->cantidad_m;
										$precio_m = $item->precio_m;

										$stok2 = $item->stok2;
										$precio2 = $item->precio2;
										$cantidad_mm2 = $item->cantidad_mm2;
										$precio_mm2 = $item->precio_mm2;
										$cantidad_m2 = $item->cantidad_m2;
										$precio_m2 = $item->precio_m2;

										$stok3 = $item->stok3;
										$precio3 = $item->precio3;
										$cantidad_mm3 = $item->cantidad_mm3;
										$precio_mm3 = $item->precio_mm3;
										$cantidad_m3 = $item->cantidad_m3;
										$precio_m3 = $item->precio_m3;

										$stok4 = $item->stok4;
										$precio4 = $item->precio4;
										$cantidad_mm4 = $item->cantidad_mm4;
										$precio_mm4 = $item->precio_mm4;
										$cantidad_m4 = $item->cantidad_m4;
										$precio_m4 = $item->precio_m4;

										$stok5 = $item->stok5;
										$precio5 = $item->precio5;
										$cantidad_mm5 = $item->cantidad_mm5;
										$precio_mm5 = $item->precio_mm5;
										$cantidad_m5 = $item->cantidad_m5;
										$precio_m5 = $item->precio_m5;

										$stok6 = $item->stok6;
										$precio6 = $item->precio6;
										$cantidad_mm6 = $item->cantidad_mm6;
										$precio_mm6 = $item->precio_mm6;
										$cantidad_m6 = $item->cantidad_m6;
										$precio_m6 = $item->precio_m6;
										?>
										<tr class="classproductoss_<?php echo $subId; ?>">
											<td>
												<input type="hidden" id="ppresentacionid" value="<?php echo $subId; ?>">
												<select class="form-control ppresentacion_<?php echo $subId; ?>"
														id="ppresentacion"
														name="ppresentacion"><?php echo $option; ?></select>
											</td>
											<td>
												<input type="hidden" id="unidad" value="<?php echo $unidad; ?>"
													   readonly>
												<input type="number" name="" id="precantidad1"
													   value="<?php echo $stok; ?>"
													   class="form-control precantidad_<?php echo $subId; ?>" readonly>
												<input type="number" name="" id="precantidad2"
													   value="<?php echo $stok2; ?>"
													   class="form-control precantidad_<?php echo $subId; ?>" readonly>
												<input type="number" name="" id="precantidad3"
													   value="<?php echo $stok3; ?>"
													   class="form-control precantidad_<?php echo $subId; ?>" readonly>
												<input type="number" name="" id="precantidad4"
													   value="<?php echo $stok4; ?>"
													   class="form-control precantidad_<?php echo $subId; ?>" readonly>
												<input type="number" name="" id="precantidad5"
													   value="<?php echo $stok5; ?>"
													   class="form-control precantidad_<?php echo $subId; ?>" readonly>
												<input type="number" name="" id="precantidad6"
													   value="<?php echo $stok6; ?>"
													   class="form-control precantidad_<?php echo $subId; ?>" readonly>
											</td>
											<td>
												<input type="number" name="" id="preprecio"
													   value="<?php echo $precio; ?>"
													   class="form-control preprecio_1_<?php echo $subId; ?>">
												<input type="number" name="" id="preprecio2"
													   value="<?php echo $precio2; ?>"
													   class="form-control preprecio_2_<?php echo $subId; ?>">
												<input type="number" name="" id="preprecio3"
													   value="<?php echo $precio3; ?>"
													   class="form-control preprecio_3_<?php echo $subId; ?>">
												<input type="number" name="" id="preprecio4"
													   value="<?php echo $precio3; ?>"
													   class="form-control preprecio_4_<?php echo $subId; ?>">
												<input type="number" name="" id="preprecio5"
													   value="<?php echo $precio3; ?>"
													   class="form-control preprecio_5_<?php echo $subId; ?>">
												<input type="number" name="" id="preprecio6"
													   value="<?php echo $precio3; ?>"
													   class="form-control preprecio_6_<?php echo $subId; ?>">
											</td>
											<td>
												<input type="number" name="" id="apartirmm"
													   value="<?php echo $cantidad_mm; ?>"
													   class="form-control apartirmm_1_<?php echo $subId; ?>">
												<input type="number" name="" id="apartirmm2"
													   value="<?php echo $cantidad_mm2; ?>"
													   class="form-control apartirmm_2_<?php echo $subId; ?>">
												<input type="number" name="" id="apartirmm3"
													   value="<?php echo $cantidad_mm3; ?>"
													   class="form-control apartirmm_3_<?php echo $subId; ?>">
												<input type="number" name="" id="apartirmm4"
													   value="<?php echo $cantidad_mm4; ?>"
													   class="form-control apartirmm_3_<?php echo $subId; ?>">
												<input type="number" name="" id="apartirmm5"
													   value="<?php echo $cantidad_mm5; ?>"
													   class="form-control apartirmm_3_<?php echo $subId; ?>">
												<input type="number" name="" id="apartirmm6"
													   value="<?php echo $cantidad_mm6; ?>"
													   class="form-control apartirmm_3_<?php echo $subId; ?>">
											</td>
											<td>
												<input type="number" name="" id="prepreciomm"
													   value="<?php echo $precio_mm; ?>"
													   class="form-control prepreciomm_1_<?php echo $subId; ?>">
												<input type="number" name="" id="prepreciomm2"
													   value="<?php echo $precio_mm2; ?>"
													   class="form-control prepreciomm_2_<?php echo $subId; ?>">
												<input type="number" name="" id="prepreciomm3"
													   value="<?php echo $precio_mm3; ?>"
													   class="form-control prepreciomm_3_<?php echo $subId; ?>">

												<input type="number" name="" id="prepreciomm4"
													   value="<?php echo $precio_mm4; ?>"
													   class="form-control prepreciomm_4_<?php echo $subId; ?>">
												<input type="number" name="" id="prepreciomm5"
													   value="<?php echo $precio_mm5; ?>"
													   class="form-control prepreciomm_5_<?php echo $subId; ?>">
												<input type="number" name="" id="prepreciomm6"
													   value="<?php echo $precio_mm6; ?>"
													   class="form-control prepreciomm_6_<?php echo $subId; ?>">
											</td>
											<td>
												<input type="number" name="" id="apartirm0"
													   value="<?php echo $cantidad_m; ?>"
													   class="form-control apartirm_1_<?php echo $subId; ?>">
												<input type="number" name="" id="apartirm2"
													   value="<?php echo $cantidad_m2; ?>"
													   class="form-control apartirm_2_<?php echo $subId; ?>">
												<input type="number" name="" id="apartirm3"
													   value="<?php echo $cantidad_m3; ?>"
													   class="form-control apartirm_3_<?php echo $subId; ?>">

														 <input type="number" name="" id="apartirm4"
													   value="<?php echo $cantidad_m4; ?>"
													   class="form-control apartirm_4_<?php echo $subId; ?>">
														 <input type="number" name="" id="apartirm5"
													   value="<?php echo $cantidad_m5; ?>"
													   class="form-control apartirm_5_<?php echo $subId; ?>">
														 <input type="number" name="" id="apartirm6"
													   value="<?php echo $cantidad_m6; ?>"
													   class="form-control apartirm_6_<?php echo $subId; ?>">
											</td>
											<td>
												<input type="number" name="" id="prepreciom0"
													   value="<?php echo $precio_m; ?>"
													   class="form-control prepreciom_1_<?php echo $subId; ?>">
												<input type="number" name="" id="prepreciom2"
													   value="<?php echo $precio_m2; ?>"
													   class="form-control prepreciom_2_<?php echo $subId; ?>">
												<input type="number" name="" id="prepreciom3"
													   value="<?php echo $precio_m3; ?>"
													   class="form-control prepreciom_3_<?php echo $subId; ?>">

														 <input type="number" name="" id="prepreciom4"
													   value="<?php echo $precio_m4; ?>"
													   class="form-control prepreciom_4_<?php echo $subId; ?>">
														 <input type="number" name="" id="prepreciom5"
													   value="<?php echo $precio_m5; ?>"
													   class="form-control prepreciom_5_<?php echo $subId; ?>">
														 <input type="number" name="" id="prepreciom6"
													   value="<?php echo $precio_m6; ?>"
													   class="form-control prepreciom_6_<?php echo $subId; ?>">


												<input type="hidden" id="tipo" value="0"></td>
											<td>
												<a href="#" class="btn btn-raised btn-danger"
												   onclick="deleterow(<?php echo $subId; ?>)"><i
														class="fa fa-minus"></i></a>
											</td>
										</tr>
										<?php
									}
								}
								?>
								</tbody>
								<tfoot>
								<!--
                                <tr>
                                  <td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                                  <td><a class="btn btn-raised btn-primary" id="addsubproductos" ><i class="fa fa-plus"></i></a></td>
                                </tr>
                                -->
								</tfoot>
							</table>
						</div>


						<div class="col-md-12">
							<button class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover"
									style="background: #2e58a6;" type="submit" id="savepr">Guardar
							</button>
						</div>

					</div>

					<!--------//////////////-------->
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('#generacodigo').click(function (event) {
			var d = new Date();
			var dia = d.getDate();//1 31
			var dias = d.getDay();//0 6
			var mes = d.getMonth();//0 11
			var yy = d.getFullYear();//9999
			var hr = d.getHours();//0 24
			var min = d.getMinutes();//0 59
			var seg = d.getSeconds();//0 59
			var yyy = 18;
			var ram = Math.floor((Math.random() * 10) + 1);
			var codigo = seg + '' + min + '' + hr + '' + yyy + '' + ram + '' + mes + '' + dia + '' + dias;
			//var condigo0=condigo.substr(0,13);
			//console.log(codigo);
			$('#pcodigo').val(codigo);
		});
	});

	function deleterow(ids) {
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>Productos/deleterow',
			data: {
				id: ids
			},
			async: false,
			statusCode: {
				404: function (data) {
					toastr.error('Error!', 'No Se encuentra el archivo');
				},
				500: function () {
					toastr.error('Error', '500');
				}
			},
			success: function (data) {
				if (data == 0) {
					toastr.success('Hecho!', 'Eliminado Correctamente');
					$('.classproductoss_' + ids).remove();
				} else {
					toastr.error('Imposible de eliminar producto en uso', 'Error');
				}

			}
		});
	}
</script>
