<style type="text/css">
	.vd_red {
		font-weight: bold;
		color: red;
	}

	.vd_green {
		color: #009688;
	}

</style>

<input type="hidden" name="clienteid" id="clienteid" value="<?php echo $cliente[0]->ClientesId; ?>">
<input type="hidden" id="contador_direcciones" name="contador_direcciones">
<div class="row">
	<div class="col-md-12">
		<h2>Cliente</h2>
	</div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header">
				<div class="row">
					<div class="col-md-6">
						<h4 class="card-title">EDITAR CLIENTE</h4>
					</div>
					
				</div>
			</div>
			<div class="card-body">
				<div class="card-block form-horizontal">
					<!--------//////////////-------->
					<div class="row">
						<form method="post" role="form" id="formclientes">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-md-2 control-label">Nombre:</label>
										<div class=" col-md-10 input-group">
											<input type="text" class="form-control" id="nombrecli" name="nombrecli"
												   value="<?php echo $cliente[0]->Nom; ?>">
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-md-2 control-label">Correo:</label>
										<div class=" col-md-10 input-group">
											<input type="email" class="form-control" id="correocli" name="correocli"
												   value="<?php echo $cliente[0]->Correo;?>">
										</div>
									</div>
								</div>

								<div class="col-md-12">
									<h3>Contacto Directo</h3>
									<hr/>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Nombre contacto:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="contactocli"
													   name="contactocli" value="<?php echo $cliente[0]->nombrec; ?>">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Correo:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="correoccli"
													   name="correoccli" value="<?php echo $cliente[0]->correoc; ?>">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Teléfono:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="telefonocli"
													   name="telefonocli" value="<?php echo $cliente[0]->telefonoc; ?>">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
			                                <label class="col-md-3 control-label"> Mercado:</label>
			                                <div class=" col-md-9">
			                                    <select class="form-control" id="id_mercado" name="id_mercado">
			                                    	<option value="<?php echo $cliente[0]->id_mercado; ?>"><?php echo $cliente[0]->mercado; ?></option>
			                                    </select>
			                                </div>
			                            </div>
			                        </div>
								</div>
								<div class="col-md-2">
									<div class="checkbox-list">
										<label><input title="Click si el cliente tiene datos fiscales" type="checkbox" id="muestraDF" <?php echo ($cliente[0]->df == 1)? "checked" : "";?>> Datos fiscales: </label><br>
									</div>
								</div>
								<div id="datosFiscales" style="display: <?php echo ($cliente[0]->df == 1)? "block" : "none";?>;">
									<div class="col-md-12">
										<h3>Datos Fiscales</h3>
										<hr/>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Razon Social:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="razonSocialDF" value="<?php echo $cliente[0]->razonSocialDF;?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Referencia:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="referenciaDF" value="<?php echo $cliente[0]->referenciaDF;?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Correo Electrónico:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="correoDF" value="<?php echo $cliente[0]->correoDF;?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Municipio:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="municipioDF" value="<?php echo $cliente[0]->municipioDF;?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Calle:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="calleDF" value="<?php echo $cliente[0]->calleDF;?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Estado:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="estadoDF" value="<?php echo $cliente[0]->estadoDF;?>">
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">No. Exterior:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="noExteriorDF" value="<?php echo $cliente[0]->noExteriorDF;?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">País:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="paisDF" value="<?php echo $cliente[0]->paisDF;?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">No. Interior:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="noInteriorDF" value="<?php echo $cliente[0]->noInteriorDF;?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Colonia:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="coloniaDF" value="<?php echo $cliente[0]->coloniaDF;?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">RFC:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="rfcDF" value="<?php echo $cliente[0]->rfcDF;?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Localidad:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="localidadDF" value="<?php echo $cliente[0]->localidadDF;?>">
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">CURP:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="curpDF" value="<?php echo $cliente[0]->curpDF;?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Teléfono:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="telefonoDF" value="<?php echo $cliente[0]->telefonoDF;?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">No. de Cuenta:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="noCuentaDF" value="<?php echo $cliente[0]->noCuentaDF;?>">
												</div>
											</div>
										</div>
									</div>
								</div>
								

								<div class="col-md-12">
									<hr><br>
									<h3>Domicilio</h3>
									<hr/>
								</div>

								<div id="domicilio1" name="domicilio1" class="domicilio1">
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Calle:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="callecli"
														   name="callecli" value="<?php echo $cliente[0]->Calle;; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="col-md-7 control-label">No Exterior:</label>
												<div class=" col-md-5 input-group">
													<input type="text" class="form-control" id="nexteriorcli"
														   name="nexteriorcli"
														   value="<?php echo $cliente[0]->noExterior; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="col-md-7 control-label">No Interior:</label>
												<div class=" col-md-5 input-group">
													<input type="text" class="form-control" id="ninteriorcli"
														   name="ninteriorcli"
														   value="<?php echo $cliente[0]->noInterior; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Colonia:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="coloniacli"
														   name="coloniacli"
														   value="<?php echo $cliente[0]->Colonia; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Localidad:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="localidadcli"
														   name="localidadcli"
														   value="<?php echo $cliente[0]->Localidad; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Municipio:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="municipiocli"
														   name="municipiocli"
														   value="<?php echo $cliente[0]->Municipio; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Código postal:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="codigopcli"
														   name="codigopcli"
														   value="<?php echo $cliente[0]->CodigoPostal; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Estado:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="estadocli"
														   name="estadocli" value="<?php echo $cliente[0]->Estado; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">País:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="paiscli"
														   name="localidadcli" value="<?php echo $cliente[0]->Pais; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Alias:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="alias1"
														   name="alias" value="<?php echo $cliente[0]->alias; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<hr/>
									</div>
								</div>

								<div id="domiciliosBase">
									<?php foreach ($domicilios as $domi) { ?>
										<input class="idDom" value="<?php echo $domi->idDomicilio;?>" style="display: none;">
										<div id="domicilioUpdate<?php echo $domi->idDomicilio; ?>">
											<div id="domicilio1" name="domicilio1" class="domicilio1">
												<div class="col-md-12">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-md-3 control-label">Calle:</label>
															<div class=" col-md-9 input-group">
																<input type="text" class="form-control" id="callecli"
																	   name="callecli"
																	   value="<?php echo $domi->Calle;; ?>">
															</div>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<label class="col-md-7 control-label">No Exterior:</label>
															<div class=" col-md-5 input-group">
																<input type="text" class="form-control" id="nexteriorcli"
																	   name="nexteriorcli"
																	   value="<?php echo $domi->noExterior; ?>">
															</div>
														</div>
													</div>
													<div class="col-md-3">
														<div class="form-group">
															<label class="col-md-7 control-label">No Interior:</label>
															<div class=" col-md-5 input-group">
																<input type="text" class="form-control" id="ninteriorcli"
																	   name="ninteriorcli"
																	   value="<?php echo $domi->noInterior; ?>">
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-md-3 control-label">Colonia:</label>
															<div class=" col-md-9 input-group">
																<input type="text" class="form-control" id="coloniacli"
																	   name="coloniacli"
																	   value="<?php echo $domi->Colonia; ?>">
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-md-3 control-label">Localidad:</label>
															<div class=" col-md-9 input-group">
																<input type="text" class="form-control" id="localidadcli"
																	   name="localidadcli"
																	   value="<?php echo $domi->Localidad; ?>">
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-md-3 control-label">Municipio:</label>
															<div class=" col-md-9 input-group">
																<input type="text" class="form-control" id="municipiocli"
																	   name="municipiocli"
																	   value="<?php echo $domi->Municipio; ?>">
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-md-3 control-label">Código postal:</label>
															<div class=" col-md-9 input-group">
																<input type="text" class="form-control" id="codigopcli"
																	   name="codigopcli"
																	   value="<?php echo $domi->CodigoPostal; ?>">
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-md-3 control-label">Estado:</label>
															<div class=" col-md-9 input-group">
																<input type="text" class="form-control" id="estadocli"
																	   name="estadocli"
																	   value="<?php echo $domi->Estado; ?>">
															</div>
														</div>
													</div>
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-md-3 control-label">País:</label>
															<div class=" col-md-9 input-group">
																<input type="text" class="form-control" id="paiscli"
																	   name="localidadcli"
																	   value="<?php echo $domi->Pais; ?>">
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-12">
													<div class="col-md-6">
														<div class="form-group">
															<label class="col-md-3 control-label">Alias:</label>
															<div class=" col-md-9 input-group">
																<input type="text" class="form-control" id="alias1"
																	   name="alias"
																	   value="<?php echo $domi->alias; ?>">
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-6">
													<button class="btn btn-sm btn-danger eliminaDomicilio" data-value="<?php echo $domi->idDomicilio; ?>"><i class="fa fa-trash-o"></i> Eliminar</button>
												</div>

												<div class="col-md-12">
													<hr/>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>


								<div id="domicilios"></div>


						</div>
						<div class="col-md-12">
							<a href="#" class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover"
							   style="background: #2e58a6;" id="editar">Guardar</a>
						</div>

							<div class="modal" tabindex="-1" role="dialog" id="modalConfirmacion">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title">Mensaje de confirmación</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<p>Estás a punto de eliminar un domicilio.</p>
											<p>¿?Desaea continuar?</p>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-primary" id="aceptarEliminarDomicilio">Aceptar</button>
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
										</div>
									</div>
								</div>
							</div>


					</div>


					<!--------//////////////-------->
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/clientesUpdate.js?v=<?php echo date("Ymdgis"); ?>"></script>