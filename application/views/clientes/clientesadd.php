<style type="text/css">
	.vd_red {
		font-weight: bold;
		color: red;
	}

	.vd_green {
		color: #009688;
	}
</style>
<input type="hidden" id="contador_direcciones" name="contador_direcciones">
<div class="row">
	<div class="col-md-12">
		<h2>Cliente</h2>
	</div>
</div>
<!--Statistics cards Ends-->
<!--Line with Area Chart 1 Starts-->
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header">
				<div class="row">
					<div class="col-md-6">
						<h4 class="card-title"><?php echo $label; ?></h4>
					</div>
					
				</div>
			</div>
			<div class="card-body">
				<div class="card-block form-horizontal">
					<!--------//////////////-------->
					<div class="row">
						<form method="post" role="form" id="formclientes">
							<div class="col-md-12">
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-md-2 control-label">Nombre:</label>
										<div class=" col-md-10 input-group">
											<input type="text" class="form-control" id="nombrecli" name="nombrecli"
												   value="<?php echo $Nom; ?>">
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-md-2 control-label">Correo:</label>
										<div class=" col-md-10 input-group">
											<input type="email" class="form-control" id="correocli" name="correocli"
												   value="<?php echo $Correo; ?>">
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<!--<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-4 control-label">Dias de credito:</label>
											<div class=" col-md-8 input-group">
												<input type="number" class="form-control" id="dcredito" name="dcredito"
													   value="<?php echo $dcredito; ?>">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Límite de crédito:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="correoccli"
													   name="correoccli" value="<?php echo $correoc; ?>">
											</div>
										</div>
									</div>
									<br/><br/><br/><br/>-->
								</div>


								<div class="col-md-12">
									<h3>Contacto Directo</h3>
									<hr/>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Nombre contacto:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="contactocli"
													   name="contactocli" value="<?php echo $nombrec; ?>">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Correo:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="correoccli"
													   name="correoccli" value="<?php echo $correoc; ?>">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Teléfono:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="telefonocli"
													   name="telefonocli" value="<?php echo $telefonoc; ?>">
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
			                                <label class="col-md-3 control-label"> Mercado:</label>
			                                <div class=" col-md-9">
			                                    <select class="form-control" id="id_mercado" name="id_mercado"></select>
			                                </div>
			                            </div>
			                        </div>
									<!--<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Extención:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="extenciocli"
													   name="extenciocli" value="<?php echo $extencionc; ?>">
											</div>
										</div>
									</div>-->
								</div>
								<!--<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Nextel:</label>
											<div class=" col-md-9 input-group">
												<input type="text" class="form-control" id="nextelcli" name="nextelcli"
													   value="<?php echo $nextelc; ?>">
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-md-3 control-label">Descripción:</label>
											<div class=" col-md-9 input-group">
												<textarea class="form-control" id="descripcioncli"
														  name="descripcioncli"><?php echo $descripcionc; ?></textarea>
											</div>
										</div>
									</div>
								</div>-->

								<div class="col-md-6">
									<div class="checkbox-list">
										<label> <input type="checkbox" id="muestraDF"> Datos fiscales:</label>
									</div>
								</div>

								<div id="datosFiscales" style="display: none;">
									<div class="col-md-12">
										<h3>Datos Fiscales</h3>
										<hr/>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Razon Social:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="razonSocialDF">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Referencia:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="referenciaDF">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Correo Electrónico:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="correoDF">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Municipio:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="municipioDF">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Calle:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="calleDF">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Estado:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="estadoDF">
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">No. Exterior:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="noExteriorDF">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">País:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="paisDF">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">No. Interior:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="noInteriorDF">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Colonia:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="coloniaDF">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">RFC:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="rfcDF">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Localidad:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="localidadDF">
												</div>
											</div>
										</div>
									</div>

									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">CURP:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="curpDF">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Teléfono:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="telefonoDF">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">No. de Cuenta:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="noCuentaDF">
												</div>
											</div>
										</div>
									</div>
								</div>



								<div class="col-md-12">
									<hr><br>
									<h3>Domicilio</h3>
									<hr/>
									<!--<div class="col-sm-12">
										<button type="button"
												class="addButtonDireccion btn btn-sm btn-success pull-right"
												onclick="duplicar()">Agregar Otra dirección
										</button>
									</div>-->
								</div>

								<div id="domicilio1" name="domicilio1" class="domicilio1">
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Calle:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="callecli"
														   name="callecli" value="<?php echo $Calle; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="col-md-7 control-label">No Exterior:</label>
												<div class=" col-md-5 input-group">
													<input type="text" class="form-control" id="nexteriorcli"
														   name="nexteriorcli" value="<?php echo $noExterior; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label class="col-md-7 control-label">No Interior:</label>
												<div class=" col-md-5 input-group">
													<input type="text" class="form-control" id="ninteriorcli"
														   name="ninteriorcli" value="<?php echo $noInterior; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Colonia:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="coloniacli"
														   name="coloniacli" value="<?php echo $Colonia; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Localidad:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="localidadcli"
														   name="localidadcli" value="<?php echo $Localidad; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Municipio:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="municipiocli"
														   name="municipiocli" value="<?php echo $Municipio; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Código postal:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="codigopcli"
														   name="codigopcli" value="<?php echo $CodigoPostal; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Estado:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="estadocli"
														   name="estadocli" value="<?php echo $Estado; ?>">
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">País:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="paiscli"
														   name="localidadcli" value="<?php echo $Pais; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="col-md-6">
											<div class="form-group">
												<label class="col-md-3 control-label">Alias:</label>
												<div class=" col-md-9 input-group">
													<input type="text" class="form-control" id="alias1"
														   name="alias" value="<?php echo $alias; ?>">
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<hr/>
									</div>
								</div>
								<div id="domicilios"></div>


						</form>
						<div class="col-md-12">
							<a href="#" class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover"
							   style="background: #2e58a6;" id="savecl">Guardar</a>
						</div>


					</div>


					<!--------//////////////-------->
				</div>
			</div>
		</div>
	</div>
</div>
