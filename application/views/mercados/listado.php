
<input type="hidden" id="perfilid_tz" value="<?php echo $_SESSION['perfilid_tz'];?>">
<input type="hidden" id="bodega_tz" value="<?php echo $_SESSION['bodega_tz'];?>">
<div class="row">
  <div class="col-md-12">
    <h2>Mercados</h2>
  </div>
  
  <div class="col-md-12">
    <div class="col-md-10"></div>
    <div class="col-md-2">
      <a href="<?php echo base_url(); ?>index.php/Mercados/mercadosadd" class="btn btn-raised gradient-ibiza-sunset white sidebar-shadow"><i class="fa fa-plus"></i></span> Nuevo Mercado</a>
    </div>
  </div>
  
  
</div>
<!--Statistics cards Ends-->

<!--Line with Area Chart 1 Starts-->
<div class="row">
  <div class="col-sm-12">
      <div class="card">
          <div class="card-header">
              <h4 class="card-title">Listado de mercados</h4>
          </div>
          <div class="card-body">
              <div class="card-block">
                  <!--------//////////////-------->
                  <table class="table table-striped" id="data-tables" style="width: 100%">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Direccion</th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
          <!--------//////////////-------->
              </div>
          </div>
      </div>
  </div>
</div>

<div class="modal fade text-left" id="eliminacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Mensaje de confirmaci&oacute;n</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ¿Desea <b>Eliminar</b> el registro ?
                      <input type="hidden" id="hddId">
            </div>
            <div class="modal-footer">
                <button type="button" id="sieliminar" class="btn btn-raised gradient-purple-bliss white" data-dismiss="modal">Aceptar</button>
                <button type="button" class="btn btn-raised gradient-ibiza-sunset white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
