
<div class="row">
  <div class="col-md-12">
    <h2>Mercados</h2>
  </div>
</div>
  <!--Statistics cards Ends-->
  <!--Line with Area Chart 1 Starts-->
  <div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Formulario de Mercado</h4>
            </div>
            <div class="card-body">
                <div class="card-block form-horizontal">
                    <!--------//////////////-------->
                    <div class="row">
                      <form method="post" role="form" id="form_mercado" class="form-horizontal">
                        <?php if(isset($m)){ ?>
                            <input type="hidden" name="id" id="id" value="<?php echo $m->id;?>">
                        <?php }else { ?>
                            <input type="hidden" name="id" id="id" value="0">
                        <?php } ?>
                        <div class="col-md-12">
                          <div class="col-md-12">
                            <h3>Datos generales</h3>
                            <hr />
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nombre:</label>
                                <div class="col-sm-8 controls">                                                         
                                  <input type="text" name="nombre" class="form-control" id="nombre" <?php if(isset($m)) echo "value='$m->nombre'"; ?>>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <br>
                            </div>
                          <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Dirección:</label>
                                <div class="col-sm-8 controls">
                                  <input type="text" name="direc" id="direc" class="form-control" <?php if(isset($m)) echo "value='$m->direc'"; ?>>
                              </div>
                            </div>
                          </div>
                        </div> 
                        <div class="col-md-12">
                            <br>
                            <br>
                            <button type="submit" class="btn btn-raised gradient-purple-bliss white shadow-z-1-hover" style="background: #2e58a6;" id="save">Guardar</button>
                          </div>
                      </form>
                      
                    </div>
                    <!--------//////////////-------->
                </div>
            </div>
        </div>
    </div>
  </div>