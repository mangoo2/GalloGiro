<?php 

header("Content-Type: text/html;charset=UTF-8");
header("Pragma: public");
header("Expires:0");
header("Cache-Control:must-revalidate,post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Type: application/vnd.ms-excel;");
header("Content-Disposition: attachment; filename=lista_ventas".date('Y-m-d').".xls");
	
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<table border="1" class="table table-striped" id="data-tables" style="width: 100%">
    <tr>
      <th>Folio</th>
      <th>Fecha</th>
      <th>Vendedor</th>
      <th>Monto</th>
      <th>Método</th>
      <th>Cliente</th>
      <th>Estatus</th>
    </tr>
  </thead>
  <tbody id="tbodyresultadosvent">
    <?php foreach ($info->result() as $item){ ?>
     <tr id="trven_<?php echo $item->id_venta; ?>">
        <td><?php echo $item->id_venta; ?></td>
        <td><?php echo $item->reg; ?></td>
        <td><?php echo $item->vendedor; ?></td>
        <td>$ <?php echo number_format($item->monto_total,2,'.',',') ; ?></td>
        <td><?php echo $item->metodo; ?></td>
        <td><?php echo $item->cliente; ?></td>
        <td><?php if($item->cancelado==1){ echo '<span class="badge badge-danger">Cancelado</span>';}
          if($item->cancelado==2){ echo '<span class="badge badge-danger">Cancelado Parcial</span>';} ?>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>