var base_url = $("#base_url").val();
$(document).ready(function () {

	$('#id_mercado').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un mercado',
        ajax: {
            url: base_url+'Mercados/searchMercado',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

	$("#editar").click(function () {
		var listDomicilios = new Array();
		var listDomiciliosBase = new Array();

		//var $valid = $("#formclientes").valid();
		var contador = 0;
		$("#domicilios").find("input:text").each(function () {
			if (contador == 0) {
				//Calle
				Calle = $(this).val();
				contador++;
			} else if (contador == 1) {
				//No exterior
				noExterior = $(this).val();
				contador++;
			} else if (contador == 2) {
				//Numero interior
				noInterior = $(this).val();
				contador++;
			} else if (contador == 3) {
				//Colonia
				Colonia = $(this).val();
				contador++;
			} else if (contador == 4) {
				//Localidad
				Localidad = $(this).val();
				contador++;
			} else if (contador == 5) {
				//Municipio
				Municipio = $(this).val();
				contador++;
			} else if (contador == 6) {
				//Codigo postal
				CodigoPostal = $(this).val();
				contador++;
			} else if (contador == 7) {
				//Estado
				Estado = $(this).val();
				contador++;
			} else if (contador == 8) {
				//Pais
				Pais = $(this).val();
				contador++;
			} else if (contador == 9) {
				//Alias
				alias = $(this).val();
				var direccion = new Object();
				direccion.Calle = Calle;
				direccion.noExterior = noExterior;
				direccion.noInterior = noInterior;
				direccion.Colonia = Colonia;
				direccion.Localidad = Localidad;
				direccion.Municipio = Municipio;
				direccion.Estado = Estado;
				direccion.Pais = Pais;
				direccion.CodigoPostal = CodigoPostal;
				direccion.alias = alias;
				direccion.status = 1;
				contador++;
				listDomicilios.push(direccion);
				contador = 0;
			}
		});


		contador = 0;
		$("#domiciliosBase").find("input:text").each(function () {
			if(contador==0){
				//id del domicilio
				idDomicilio = $(this).val();
				contador ++;
			}else if (contador == 1) {
				//Calle
				Calle = $(this).val();
				contador++;
			} else if (contador == 2) {
				//No exterior
				noExterior = $(this).val();
				contador++;
			} else if (contador == 3) {
				//Numero interior
				noInterior = $(this).val();
				contador++;
			} else if (contador == 4) {
				//Colonia
				Colonia = $(this).val();
				contador++;
			} else if (contador == 5) {
				//Localidad
				Localidad = $(this).val();
				contador++;
			} else if (contador == 6) {
				//Municipio
				Municipio = $(this).val();
				contador++;
			} else if (contador == 7) {
				//Codigo postal
				CodigoPostal = $(this).val();
				contador++;
			} else if (contador == 8) {
				//Estado
				Estado = $(this).val();
				contador++;
			} else if (contador == 9) {
				//Pais
				Pais = $(this).val();
				contador++;
			} else if (contador == 10) {
				//Alias
				alias = $(this).val();
				var direccionBase = new Object();
				direccionBase.idDomicilio = idDomicilio;
				direccionBase.Calle = Calle;
				direccionBase.noExterior = noExterior;
				direccionBase.noInterior = noInterior;
				direccionBase.Colonia = Colonia;
				direccionBase.Localidad = Localidad;
				direccionBase.Municipio = Municipio;
				direccionBase.Estado = Estado;
				direccionBase.Pais = Pais;
				direccionBase.CodigoPostal = CodigoPostal;
				direccionBase.alias = alias;
				direccionBase.status = 1;
				contador++;
				listDomiciliosBase.push(direccionBase);
				contador = 0;
			}
		});
		//if ($valid) {
			$.ajax({
				type: 'POST',
				url: base_url+'Clientes/clientesEdita',
				data: {
					ClientesId: $('#clienteid').val(),
					Nom: $('#nombrecli').val(),
					Correo: $('#correocli').val(),
					Calle: $('#callecli').val(),
					noExterior: $('#nexteriorcli').val(),
					noInterior: $('#ninteriorcli').val(),
					Colonia: $('#coloniacli').val(),
					Localidad: $('#localidadcli').val(),
					Municipio: $('#municipiocli').val(),
					CodigoPostal: $('#codigopcli').val(),
					Estado: $('#estadocli').val(),
					Pais: $('#paiscli').val(),
					alias: $("#alias1").val(),
					nombrec: $('#contactocli').val(),
					correoc: $('#correoccli').val(),
					telefonoc: $('#telefonocli').val(),
					id_mercado: $('#id_mercado option:selected').val(),
					//extencionc: $('#extenciocli').val(),
					//nextelc: $('#nextelcli').val(),
					//descripcionc: $('#descripcioncli').val(),
					dcredito: $('#dcredito').val(),
					razonSocialDF: $("#razonSocialDF").val(),
					referenciaDF: $("#referenciaDF").val(),
					correoDF: $("#correoDF").val(),
					municipioDF: $("#municipioDF").val(),
					calleDF: $("#calleDF").val(),
					estadoDF: $("#estadoDF").val(),
					coloniaDF: $("#coloniaDF").val(),
					noExteriorDF: $("#noExteriorDF").val(),
					paisDF: $("#paisDF").val(),
					noInteriorDF: $("#noInteriorDF").val(),
					rfcDF: $("#rfcDF").val(),
					localidadDF: $("#localidadDF").val(),
					curpDF: $("#curpDF").val(),
					telefonoDF: $("#telefonoDF").val(),
					noCuentaDF: $("#noCuentaDF").val(),
					df : $("#muestraDF").is(":checked"),
					listaDirecciones: listDomicilios,
					//listDomiciliosBase: listDomiciliosBase,
				},
				async: false,
				statusCode: {
					404: function (data) {
						toastr.error('Error!', 'No Se encuentra el archivo');
					},
					500: function () {
						toastr.error('Error', '500');
					}
				},
				success: function (data) {
					toastr.success('Hecho!', 'Guardado Correctamente');
					setInterval(function () {
						//location.href = '../../Clientes';
						window.location.href = base_url+"Clientes";
					}, 3000);
				}
			});
		//}
	});

	$(".eliminaDomicilio").click(function () {
		$("#modalConfirmacion").modal();
		idDomicilioEliminar = $(this).attr("data-value");

	});

	$("#muestraDF").click(function () {
		if ($(this).is(":checked") == true) {
			$("#datosFiscales").show();
		} else {
			$("#datosFiscales").hide();
		}
	});
	$("#aceptarEliminarDomicilio").click(function () {
		$.ajax({
			url: base_url+"Clientes/eliminar_dom",
			data:{
				idDomicilio: idDomicilioEliminar
			},
			type: "POST",
			success: function () {
				$("#domicilioUpdate"+idDomicilioEliminar).remove();
				$("#modalConfirmacion").modal("toggle");

			}
		});
	});
});



function duplicar() {
	var direcciones_actuales = $('#contador_direcciones').val();
	var no_direcciones = direcciones_actuales;
	no_direcciones++;

	$('#contador_direcciones').val(no_direcciones);
	var $template = $('#domicilio1'),
		$clone = $template
			.clone()
			.show()
			.removeAttr('id')
			.removeAttr('class')
			.attr("class", "domicilios")
			.attr('id', "domicilio" + no_direcciones)
			.insertAfter('#domicilio' + direcciones_actuales)
			.append('<div class="col-md-12">\
                                    <button type="button" onclick="eliminarDireccion(' + no_direcciones + ')" class="mt-0 mb-0 btn-sm pull-right btn btn-danger"><i class="fa fa-minus"></i></button>\
                               </div>')

	$clone.find("input").val("");
	$("#domicilios").append($clone);
}

function eliminarDireccion(no_direcciones) {
	console.log('#domicilio' + no_direcciones);
	$('#domicilio' + no_direcciones).remove();

	// Remove element containing the option
	var valorContador = $('#contador_direcciones').val();
	valorContador = valorContador - 1;
	$('#contador_direcciones').val(valorContador);

}
