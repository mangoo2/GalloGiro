var base_url = $("#base_url").val();
$(document).ready(function () {
	// Inicializar el contacor de direcciones en 1
	$('#contador_direcciones').val(1);

	var form_register = $('#formclientes');
	var error_register = $('.alert-danger', form_register);
	var success_register = $('.alert-success', form_register);

	var $validator1 = form_register.validate({
		errorElement: 'div', //default input error message container
		errorClass: 'vd_red', // default input error message class
		focusInvalid: false, // do not focus the last invalid input
		ignore: "",
		rules: {
			nombrecli: {
				required: true
			},
			productoname: {
				required: true
			},


		},

		errorPlacement: function (error, element) {
			if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
				element.parent().append(error);
			} else if (element.parent().hasClass("vd_input-wrapper")) {
				error.insertAfter(element.parent());
			} else {
				error.insertAfter(element);
			}
		},

		invalidHandler: function (event, validator) { //display error alert on form submit
			success_register.fadeOut(500);
			error_register.fadeIn(500);
			scrollTo(form_register, -100);

		},

		highlight: function (element) { // hightlight error inputs

			$(element).addClass('vd_bd-red');
			$(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

		},

		unhighlight: function (element) { // revert the change dony by hightlight
			$(element)
				.closest('.control-group').removeClass('error'); // set error class to the control group
		},

		success: function (label, element) {
			label
				.addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
				.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
			$(element).removeClass('vd_bd-red');
		}
	});

	$('#id_mercado').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un mercado',
        ajax: {
            url: base_url+'Mercados/searchMercado',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

	//=====================================================================================================
	$('#savecl').click(function (event) {
		var listDomicilios = new Array();

		var $valid = $("#formclientes").valid();
		var contador = 0;
		$(".domicilios").find("input:text").each(function () {
			if (contador == 0) {
				//Calle
				Calle = $(this).val();
				contador++;
			} else if (contador == 1) {
				//No exterior
				noExterior = $(this).val();
				contador++;
			} else if (contador == 2) {
				//Numero interior
				noInterior = $(this).val();
				contador++;
			} else if (contador == 3) {
				//Colonia
				Colonia = $(this).val();
				contador++;
			} else if (contador == 4) {
				//Localidad
				Localidad = $(this).val();
				contador++;
			} else if (contador == 5) {
				//Municipio
				Municipio = $(this).val();
				contador++;
			} else if (contador == 6) {
				//Codigo postal
				CodigoPostal = $(this).val();
				contador++;
			} else if (contador == 7) {
				//Estado
				Estado = $(this).val();
				contador++;
			} else if (contador == 8) {
				//Pais
				Pais = $(this).val();
				contador++;
			} else if (contador == 9) {
				//Alias
				alias = $(this).val();
				var direccion = new Object();
				direccion.Calle = Calle;
				direccion.noExterior = noExterior;
				direccion.noInterior = noInterior;
				direccion.Colonia = Colonia;
				direccion.Localidad = Localidad;
				direccion.Municipio = Municipio;
				direccion.Estado = Estado;
				direccion.Pais = Pais;
				direccion.CodigoPostal = CodigoPostal;
				direccion.alias = alias;
				direccion.status = 1;
				contador++;
				listDomicilios.push(direccion);
				contador = 0;
			}
		});
		if ($valid) {
			$.ajax({
				type: 'POST',
				url: base_url+'Clientes/clienteadd',
				data: {
					ClientesId: $('#clienteid').val(),
					Nom: $('#nombrecli').val(),
					Correo: $('#correocli').val(),
					Calle: $('#callecli').val(),
					noExterior: $('#nexteriorcli').val(),
					noInterior: $('#ninteriorcli').val(),
					Colonia: $('#coloniacli').val(),
					Localidad: $('#localidadcli').val(),
					Municipio: $('#municipiocli').val(),
					CodigoPostal: $('#codigopcli').val(),
					Estado: $('#estadocli').val(),
					Pais: $('#paiscli').val(),
					alias: $("#alias1").val(),
					nombrec: $('#contactocli').val(),
					correoc: $('#correoccli').val(),
					telefonoc: $('#telefonocli').val(),
					id_mercado: $('#id_mercado option:selected').val(),
					//extencionc: $('#extenciocli').val(),
					//nextelc: $('#nextelcli').val(),
					//descripcionc: $('#descripcioncli').val(),
					dcredito: $('#dcredito').val(),
					razonSocialDF: $("#razonSocialDF").val(),
					coloniaDF: $("#coloniaDF").val(),
					referenciaDF: $("#referenciaDF").val(),
					correoDF: $("#correoDF").val(),
					municipioDF: $("#municipioDF").val(),
					calleDF: $("#calleDF").val(),
					estadoDF: $("#estadoDF").val(),
					noExteriorDF: $("#noExteriorDF").val(),
					paisDF: $("#paisDF").val(),
					noInteriorDF: $("#noInteriorDF").val(),
					rfcDF: $("#rfcDF").val(),
					localidadDF: $("#localidadDF").val(),
					curpDF: $("#curpDF").val(),
					telefonoDF: $("#telefonoDF").val(),
					noCuentaDF: $("#noCuentaDF").val(),
					df : $("#muestraDF").is(":checked"),
					listaDirecciones: listDomicilios
				},
				async: false,
				statusCode: {
					404: function (data) {
						toastr.error('Error!', 'No Se encuentra el archivo');
					},
					500: function () {
						toastr.error('Error', '500');
					}
				},
				success: function (data) {
					toastr.success('Hecho!', 'Guardado Correctamente');
					setInterval(function () {
						//location.href = '../../Clientes';
						window.location.href = base_url+"Clientes";
					}, 3000);
				}
			});
		}
	});


	$("#muestraDF").click(function () {
		if ($(this).is(":checked") == true) {
			$("#datosFiscales").show();
		} else {
			$("#datosFiscales").hide();
		}
	});
});

function buscarcliente() {
	var search = $('#buscarcli').val();
	if (search.length > 2) {
		$.ajax({
			type: 'POST',
			url: base_url+'Clientes/buscarcli',
			data: {
				buscar: $('#buscarcli').val()
			},
			async: false,
			statusCode: {
				404: function (data) {
					toastr.error('Error!', 'No Se encuentra el archivo');
				},
				500: function () {
					toastr.error('Error', '500');
				}
			},
			success: function (data) {
				$('#tbodyresultadoscli2').html(data);
			}
		});
		$("#data-tables").css("display", "none");
		$("#data-tables2").css("display", "");
	} else {
		$("#data-tables2").css("display", "none");
		$("#data-tables").css("display", "");
	}
}


function duplicar() {
	var direcciones_actuales = $('#contador_direcciones').val();
	var no_direcciones = direcciones_actuales;
	no_direcciones++;

	$('#contador_direcciones').val(no_direcciones);
	var $template = $('#domicilio1'),
		$clone = $template
			.clone()
			.show()
			.removeAttr('id')
			.removeAttr('class')
			.attr("class", "domicilios")
			.attr('id', "domicilio" + no_direcciones)
			.insertAfter('#domicilio' + direcciones_actuales)
			.append('<div class="col-md-12">\
                                    <button type="button" onclick="eliminarDireccion(' + no_direcciones + ')" class="mt-0 mb-0 btn-sm pull-right btn btn-danger"><i class="fa fa-minus"></i></button>\
                               </div>')

	$clone.find("input").val("");
	$("#domicilios").append($clone);
}

function eliminarDireccion(no_direcciones) {
	console.log('#domicilio' + no_direcciones);
	$('#domicilio' + no_direcciones).remove();

	// Remove element containing the option
	var valorContador = $('#contador_direcciones').val();
	valorContador = valorContador - 1;
	$('#contador_direcciones').val(valorContador);

}
