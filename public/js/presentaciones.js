var base_url = $('#base_url').val();

$(document).ready(function () {
    
    table = $('#datatables_presenta').DataTable({
        "ajax": {
            "url": base_url+"index.php/Presentaciones/get_records"
        },
        "columns": [
            {"data": "presentacionId"},
            {"data": "presentacion"},
            {"data": "unidad"},
            {
            "data": null,
            "defaultContent": "<button class='btn btn-success btn-rounded btn-icon edit' type='button'><i class='fa fa-pencil-square-o'></i></button>\
              <button class='btn btn-danger btn-rounded btn-icon delete' type='button'><i class='fa fa-trash-o'></i></button>"
          },
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[10, 25, 50, 100], [10, 25, 50, 100]],
        //"dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            {   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });

    $('#datatables_presenta').on('click', 'button.edit', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        window.location.href = base_url+"index.php/Presentaciones/Edit/"+data.presentacionId;
    }); 
    $('#datatables_presenta').on('click', 'button.delete', function () {
        var tr = $(this).closest('tr');
        var data = table.row(tr).data();
        DelElement(data.presentacionId);
    });
  
     $('#form_presenta').validate({
        rules: {
            presentacion: "required",
            unidad: "required",      
        },
        messages: {
            presentacion: "Campo requerido",
            unidad: "Campo requerido",
        },
        submitHandler: function (form) {
             $.ajax({
                 type: "POST",
                 url: base_url+"index.php/Presentaciones/submit",
                 data: $(form).serialize(),
                 beforeSend: function(){
                    $("#SaveButton").attr("disabled",true);
                 },
                 success: function (result) {
                    console.log(result);
                    $("#SaveButton").attr("disabled",false);
                    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                    setTimeout(function () { window.location.href = base_url+"index.php/Presentaciones" }, 1500);
                 }
             });
             return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });   

});

function DelElement(id){
  idDell=id;
  $("#ModalDelPro").modal('show');
}

function Dell() {
  $.ajax({
    url: base_url+'index.php/Presentaciones/delete',
    type: 'POST',
    data: {
      "id": idDell,
    },
    success: function(data) {
      $("#ModalDelPro").modal('hide');
      table.ajax.reload();
      swal("Éxito!", "Se ha eliminado correctamente", "success");
      },
      error: function(jqXHR) {
        console.log(jqXHR);
      }
  });
}