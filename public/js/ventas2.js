var base_url = $('#base_url').val();
var band=0;

$(document).ready(function() {	

	$('#vcliente').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un cliente',
	  	ajax: {
	    	url: base_url+'Ventas/searchcli',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.ClientesId,
                    text: element.Nom
                });
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
		selectOnClose: true;
		$('#vproducto').select2('open').on('focus');
	});

	$('#vproducto').select2({
		width: 'resolve',
		minimumInputLength: 3,
		minimumResultsForSearch: 10,
		placeholder: 'Buscar un Producto',
	  	ajax: {
	    	url: base_url+'Ventas/searchproducto',
	    	dataType: "json",
	    	data: function (params) {
	      	var query = {
	        	search: params.term,
	        	type: 'public'
	      	}
	      	return query;
	    },
	    processResults: function(data){
	    	var clientes=data;
	    	var itemscli = [];
	    	data.forEach(function(element) {
                itemscli.push({
                    id: element.productoaddId,
                    text: element.categoria+' / '+element.marca
                });
                //console.log("productoaddId: "+element.productoaddId);
            });
            return {
                results: itemscli
            };	    	
	    },  
	  }
	}).on('select2:select', function (e) {
	    var data = e.params.data;
	    //console.log(data);
	    addproducto();
	    //mandaPrecio(data.productoaddId,$(".prec_"+data.productoaddId+"").val());
	});
	$('#vproducto').select2('open').on('focus');
	$('#ingresaventa').click(function(event) {
            addventas();
            
    });
    $('#btnabrirt').click(function(){
		$.ajax({
			type:'POST',
			url:base_url+'Ventas/abrirturno',
			data:{
				cantidad: $('#cantidadt').val(),
				nombre: $('#nombredelturno').val()
			},
			async:false,
			success:function(data){
				toastr.success('Turno Abierto','Hecho!');
				$('#modalturno').modal('hide');
				
			}
		});
		
	});
	$( "#vingreso" ).keypress(function(e) {
	  	if(e.which == 13) {
       		addventas();
    	}
	});
	$( "#vingresot" ).keypress(function(e) {
	  	if(e.which == 13) {
       		addventas();
    	}
	});

	/*$( "#vcantidad" ).keyup(function(e) {
	  	setTimeout(function(){ 
           
        }, 1500);
	});*/
	/*$(document).on('keydown', '.select2', function(e) {
	 	if (e.originalEvent && e.which == 09) {
			//e.preventDefault();
	    	//$(this).siblings('select').select2('open');
	    	$('#vproducto').select2('open').on('focus');
	  	}
	});*/
	//$(document).on("select2:close", '.select2-hidden-accessible', function () { $(this).focus(); });


});
function addproducto(){
	if ($('#vcantidad').val()>0) {
		prod= $('#vproducto').val();
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/addproducto2',
	        data: {
	            cant: $('#vcantidad').val(),
	            prod: $('#vproducto').val(),
	            band: $('#band_'+prod+'').val()
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//console.log(data);
	                $('#class_productos').html(data);
	                $("#tipo_costo").attr("disabled",true);
	            }
	    });
		$('#vcantidad').val(1);
		$('#vproducto').html('');
	    $("#vproducto").val(0).change();
	    $('#vproducto').select2('open').on('focus');
	}
    calculartotal();
}

function recalcula(id){
    total = $(".cant_"+id+"").val() * $(".prec_"+id+"").val();
    //console.log("total: "+total);
    $(".vstotal_"+id+"").val(total);
	$("#band_"+id+"").val(total);
	band=1;
    calculartotal();
    setTimeout(function(){ 
		mandaPrecio(id,$(".prec_"+id+"").val());
    }, 1000);
    
}

function mandaPrecio(id,precio){
	//var idr=$('.prec_'+id).data('rowprecio');
	$.ajax({
        type:'POST',
        url: base_url+'Ventas/precio_prod',
        data: {
        	id: id,
        	precio: precio
        },
        success:function(data){
        	console.log(data);
        	//console.log("ok");
        }
    });
}

function calculartotal(){
	var addtp = 0;
	$(".vstotal").each(function() {
        var vstotal = $(this).val();
        addtp += Number(vstotal);
    });
    //console.log("addtp: "+addtp);
    //var descuento1=100/$('#mdescuento').val();
    $('#vsbtotal').val(addtp);
    var descuento=addtp*$('#mdescuento').val();
    $('#cantdescuento').val(descuento);
    var total=parseFloat(addtp)-parseFloat(descuento);
    $('#vtotal').val(Number(total).toFixed(2));
    ingreso();
}
function ingreso(){
	var ingresoe= $('#vingreso').val();
	var ingresot= $('#vingresot').val();
	var ingreso=parseFloat(ingresoe)+parseFloat(ingresot);
	var totals=$('#vtotal').val();

	if(ingreso>0)
		var cambio = parseFloat(ingreso)-parseFloat(totals);
	//if (cambio<0) {
	//	cambio=0;
	//}
	$('#vcambio').val(cambio);
}
function limpiar(){
	$('#class_productos').html('');
	$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/productoclear',
	        async: false,
	        statusCode:{
	            404: function(data){
	            	toastr.error('Error!', 'No Se encuentra el archivo');
	        	},
	        	500: function(){
	                toastr.error('Error', '500');
	            }
	        },
	        success:function(data){
	            	
	        }
	});
}
function deletepro(id){
	
	$.ajax({
        type:'POST',
        url: base_url+'Ventas/deleteproducto',
        data: {
            idd: id
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            	$('.producto_'+id).remove();
            	calculartotal();
            }
        });
}
function addventas(){
	//if (/*$('#vcambio').val()>=0 || */parseFloat($("#vingreso").val())!=0 || parseFloat($("#vingresot").val()!=0)) {
	if (parseFloat($('#vcambio').val())>=0) {
		/*console.log("vcambio: "+$('#vcambio').val());
		console.log("vingreso: "+$('#vingreso').val());
		console.log("vingresot: "+$('#vingresot').val());
		console.log("tipo_costo: "+$('#tipo_costo').val());*/

		var vcambio = $('#vcambio').val();
		var efec =$('#vingreso').val()==''?0:$('#vingreso').val();
		var tar = $('#vingresot').val();
		var vpagacon = parseFloat(efec)+parseFloat(tar);
		var vingresot =$('#vingresot').val()==''?0:$('#vingresot').val();
		var vtotal = $('#vtotal').val();
		var metodo = $('#mpago').val();
		var fact = 0;
		var efectivo = parseFloat(vtotal)-parseFloat(vingresot);
        $.ajax({
            type:'POST',
            url: base_url+'Ventas/ingresarventa',
            data: {
            	uss: $('#ssessius').val(),
                cli: $('#vcliente').val(),
                mpago: $('#mpago').val(),
                desc: $('#mdescuento').val(),
                descu: $('#cantdescuento').val(),
                sbtotal:$('#vsbtotal').val(),
                total: vtotal,
                efectivo: efectivo,
                tarjeta: vingresot,
                fac: fact,
                tipo: 1
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            	var idventa=data;
            	var DATA  = [];
    			var TABLA   = $("#productosv tbody > tr");
				TABLA.each(function(){         
	                item = {};
	                item ["idventa"] = idventa;
	                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
	                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
	                item ["kilos"]  = $(this).find("input[id*='vscanti']").val();
	                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
	                DATA.push(item);
	            });
				INFO  = new FormData();
				aInfo   = JSON.stringify(DATA);
				INFO.append('data', aInfo);
	            $.ajax({
	                data: INFO,
	                type: 'POST',
	                url : base_url+'Ventas/ingresarventapro/1',
	                processData: false, 
	                contentType: false,
	                async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(){
                            toastr.error('Error', '500');
                        }
                    },
	                success: function(data){
	                }
	            });
	            checkprint = document.getElementById("checkimprimir").checked;
				if (checkprint==true) {
					$("#iframeri").modal();
					//$('#iframereporte').html('<iframe src="Visorpdf?filex=Ticket&iden=id&id='+idventa+'"></iframe>');	
					$('#iframereporte').html('<iframe src="'+base_url+'Ticket?id='+idventa+'&vcambio='+vcambio+'&vpagacon='+vpagacon+'&metodo='+metodo+'&tipo=1"></iframe>');		
				}else{
					toastr.success( 'Venta Realizada','Hecho!');
				}
				limpiar();
			    $("#vcliente").val(45).change();
			    $('#vtotal').val('');
			    $('#vcambio').val('');
			    $('#vingreso').val('');
			    $('#vingresot').val('');
			    $("#mpago").attr("disabled",false);
				$("#mdescuento").attr("disabled",false);
				$("#vingreso").attr("disabled",false);
				$("#vingresot").attr("disabled",false);
            }
        });
    }else{
    	toastr.error('No se puede realizar la venta debido a que no ha ingresado el saldo para liquidar la venta','Error!');
    }
}
