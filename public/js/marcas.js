var base_url = $('#base_url').val();

$(document).ready(function () {
    
    $('#datatables_marcas').DataTable({
        "ajax": {
            "url": base_url+"index.php/marcas/getListadoMarcas"
        },
        "columns": [
            {"data": "marcaid"},
            {"data": "imgm",
                render:function(data,type,row)
                {
                    var imagen = '';
                    if (row.imgm=='') 
                    {
                        imagen='public/img/imagennodisponible.png';
                    }
                    else
                    {
                        imagen='public/img/marcas/'+row.imgm;
                    }
                    var html = '<img src="'+base_url+'/'+imagen+'" class="imgpro" >';
                    return html;
                }
            },
            {"data": "marca"},
            {"data": "marcaid",
                render:function(data,type,row)
                {
                    var html = '<button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                              <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                  <span class="sr-only">Toggle Dropdown</span>\
                              </button>\
                              <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                  <a class="dropdown-item" href="'+base_url+'index.php/Marcas/marcasadd?id='+row.marcaid+'">Editar</a>\
                                  <a class="dropdown-item" onclick="productodelete('+row.marcaid+');"href="#">Eliminar</a>\
                              </div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
        //"dom": 'Bfrtpl',
        // Muestra el botón de Excel para importar la tabla
        "buttons": [
            {   
                extend: 'excelHtml5',
                text: ' Descargar Excel <i class="fa fa-download"></i>',
                className: 'btn classBotonFratsa'
            }
        ],
        // Cambiamos lo principal a Español
        "language": {
            "lengthMenu": "Desplegando _MENU_ elementos por página",
            "zeroRecords": "Lo sentimos - No se han encontrado elementos",
            "sInfo":  "Mostrando registros del _START_ al _END_ de un total de _TOTAL_  registros",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado de _MAX_ registros totales)",
            "search": "Buscar : _INPUT_",
            "paginate": {
                "previous": "Página previa",
                "next": "Siguiente página"
              }
        }
    });
  
    // Formulario para ingreso de marca
        $("#form_marca")
                .on('success.form.fv', function (e) {
                    // Prevent form submission
                    e.preventDefault();
                    var $form = $(e.target);
                    // Use Ajax to submit form data
                    var fd = new FormData($form[0]);
                    
                    $.ajax({
                        type: "POST",
                        url: base_url+"index.php/marcas/insertUpdateMarca",
                        cache: false,
                        data: fd,
                        processData: false,
                        contentType: false,
                        success: function (data) 
                        {
                            if (data==1)
                            {
                                var texto="Se ha registrado la información de manera correcta";
                                var url="";
                                
                                swal({
                                    title: 'Éxito!',
                                    text: texto,
                                    type: 'success',
                                    showCancelButton: false,
                                    allowOutsideClick: false
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        window.location = base_url+"index.php/marcas";
                                    }
                                }).catch(swal.noop);
                            }
                            else{
                                swal("Error!", "Se produjo un error, intente de nuevO o contacte al administrador del sistema", "error")
                            }
                        }
                    }); // fin ajax
                })
                .formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        txtRazonSocial: {
                            validators: {
                                notEmpty: {
                                    message: "Por favor ingrese la Razón Social"
                                }
                            }
                        }
                    }
                });

});