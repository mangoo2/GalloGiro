	var productoid;
	var productoname;
	var marcaid;
	var marcaname;
	var categoriaid;
	var categorianame;
	var imagenselect;
	var base_url=$('#base_url').val();
$(document).ready(function() {
	var idventa;	
	/*$('#clientes').click(function(event) {
		$('#modalclientes').modal();
		presskey();
	});*/

	setTimeout(function(){ presskey(); }, 2000);
	$('#realizarventa').click(function(event) {
		var factura = $('#factura').is(':checked')==true?1:0;
		// Validar venta si hay algun producto que vender
		var TABLA   = $("#tableproductos tbody > tr");
		var aux_int = 0;

		TABLA.each(function(){   
            aux_int =1;
        });
        if(aux_int==1){    
            $.ajax({ 
                type:'POST',
                url: base_url+'Ventas/ingresarventa',
                data: {
                	uss: $('#ssessius').val(),
                    cli: $('#clientes option:selected').val(),
                    total:$('#totalprod').val(),
                    fac:factura,
                    desc:$('#optiondescuento option:selected').val(),
                    mpago: $('#metodopagos option:selected').val()
                    
                },
                async: false,
                statusCode:{
                    404: function(data){
                        toastr.error('Error!', 'No Se encuentra el archivo');
                    },
                    500: function(data){
                    	console.log(data);
                        toastr.error('Error', '500');
                    }
                },
                success:function(data){
                	idventa=data;
                	var DATA  = [];
        			var TABLA   = $("#tableproductos tbody > tr");
        				TABLA.each(function(){         
			                item = {};
			                item ["idventa"] = idventa;
			                item ["producto"]   = $(this).find("input[id*='vsproid']").val();
			                item ["cantidad"]  = $(this).find("input[id*='vscanti']").val();
			                item ["kilos"]  = $(this).find("input[id*='vskilos']").val();
			                item ["precio"]  = $(this).find("input[id*='vsprecio']").val();
			                DATA.push(item);
			            });
        				INFO  = new FormData();
        				aInfo   = JSON.stringify(DATA);
        				INFO.append('data', aInfo);
			            $.ajax({
			                data: INFO,
			                type: 'POST',
			                url : base_url+'Ventas/ingresarventapro/'+1,
			                processData: false, 
			                contentType: false,
			                async: false,
		                    statusCode:{
		                        404: function(data){
		                            toastr.error('Error!', 'No Se encuentra el archivo');
		                        },
		                        500: function(){
		                        	console.log(data.responseText);
		                            toastr.error('Error', '500');
		                        }
		                    },
			                success: function(data){
			                }
			            });
			            var ticket = $('#ticketventa').is(':checked')==true?1:0;
			            var factura = $('#factura').is(':checked')==true?1:0;
			            if (ticket==1) {
			            	$("#modalimpresion").modal();
							$('#iframeticket').html('<iframe src="Ticket?id='+idventa+'&tipo=1"></iframe>');
			            }else{
			            	toastr.success( 'Venta Realizada','Hecho!');
			            	setTimeout(function(){ btncancelar(); }, 2000);
			            }
			            if (factura==1) {
			            	$("#modaldatosfactura").modal();
			            }
			            btncancelar2();
                }
            });
        }else{
           toastr.error('Error!', 'No has agregado ningún producto');	
        }    

	});
	/*$('#selectcliente').mlKeyboard({   //descomentar para el toch
          layout: 'es_ES'
        });*/
	$('#nombredelturno').mlKeyboard({
          layout: 'es_ES'
        });
	$('#cantidadt').mlKeyboard({
          layout: 'es_ES'
        });
	$('#btnabrirt').click(function(){
		$.ajax({
			type:'POST',
			url:base_url+'Ventas/abrirturno',
			data:{
				cantidad: $('#cantidadt').val(),
				nombre: $('#nombredelturno').val(),
				bodega: $('#bodegauser').val(),
			},
			async:false,
			statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(data){
                	console.log(data);
                    toastr.error('Error', '500');
                }
            },
			success:function(data){
				toastr.success('Turno Abierto','Hecho!');
				$('#modalturno').modal('hide');
				
			}
		});		
	});

	$('.classproducto').addClass('zoomIn animated');
	$('#secosto').numpad();
    $('#cantidadcajas').numpad();
    $('#cantidadkilos').numpad();
    //$('#cantidadt').numpad();
	$('.panelventa1').click(function(){
		$('.classproducto').removeClass('zoomIn animated');
        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
        $('.classproductosub').addClass('zoomIn animated');
    });
    $('.panelventa2').click(function(){
    	$('.classproductosub').removeClass('zoomIn animated');
        $(".panelventa2").css("display","none");
        $(".panelventa3").css("display","block");
        $('.classproductotipo').addClass('zoomIn animated');
    });
    $('.panelventa3').click(function(){
    	$('.classproductotipo').removeClass('zoomIn animated');

        $(".panelventa3").css("display","none");
        $(".panelventa4").css("display","block");
        $('input[name=cantidadcajas]').focus(); 
        $('.classsventa').addClass('zoomIn animated');
    });
    $('#cambiaTipoVenta').click(function(){
    	//console.log("cambio tipo de venta");
    	$('.classproductotipo').removeClass('zoomIn animated');

        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
        $('.classsventa').addClass('zoomIn animated');
    });
    $('.masproductos').click(function(event) {
    	$('.panelventa2').html('');
    	$('.panelventa3').html('');
    	$('.classproducto').addClass('zoomIn animated');
    	$(".panelventa4").css("display","none");
    	$(".panelventa1").css("display","block");
    });
    $(".classproducto").click(function(event) {
    	productoid= $(this).attr('data-idproducto');
        $("#id_prod_gral").val(productoid);
    	productoname= $(this).attr('data-cate');
    	cargamarcas(productoid);
    	if (productoid!=1) {
    		$("#cantidadkilos").css("display","none");
    		$("#cantidadkilos").val(0);
    	}else{
    		$("#cantidadkilos").css("display","block");
    	}
    });
    //$(".panelventa1").css("display","none");
    //$(".panelventa4").css("display","block");
    $('#savedatosfactura').click(function() {
    	$.ajax({ 
                    type:'POST',
                    url: base_url+'Ventas/datosfactura',
                    data: {
                    	idventa: idventa,
                        datos: $('#datosfactura').val()
                        
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(data){
                        	console.log(data);
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                    	toastr.success( 'Datos Fiscales','Guardados!');
                    }
                });
    });

    $('#clientes').select2({
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Cliente',
        ajax: {
            url: base_url+'Pedidos/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.ClientesId,
                        text: element.Nom,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        reiniciaMetodo();
    });
});
$.fn.numpad.defaults.gridTpl = '<table class="table modal-content tablenumber"></table>';
$.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
$.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
$.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn vd_btn vd_bg-yellow numbertouch"></button>';
//$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn numbertouch2" style="width: 100%;" onclick="addproductoss()"></button>';
$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn numbertouch2" style="width: 100%;"></button>';
$.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};

function mandaPrecio(id,precio){
    //var idr=$('.prec_'+id).data('rowprecio');
    //console.log("id en mandar precio: "+id);
    //console.log("precio: "+precio);
    $.ajax({
        type:'POST',
        url: base_url+'Ventas/precio_prod',
        data: {
            id: id,
            precio: precio
        },
        success:function(data){
            //console.log(data);
            //console.log("ok");
        }
    });
}
function recalcula(id){
    var precio = $(".prec_"+id+"").val();
    var cant = $(".cant_"+id+"").val();
    var total = cant * precio;
    //console.log("id: "+id);
    //console.log("precio: "+precio);

    $(".prec_"+id+"").val('');
    $(".prec_"+id+"").val(precio);
    $(".vstotal_"+id+"").val(total);
    //$("#band_"+id+"").val(total);
    //band=1;
    calculartotal();
    setTimeout(function(){ 
        mandaPrecio(id,$(".prec_"+id+"").val());
    }, 1000);
    
}
function calculartotal(){
		var addtp = 0;
		$(".vstotal").each(function() {
	        var vstotal = $(this).val();
	        addtp += Number(vstotal);
	    });
        //console.log("addtp: "+addtp);
	    var descuento0=addtp*$('#optiondescuento option:selected').val();
	    var total=parseFloat(addtp)-parseFloat(descuento0);
	    $('#totalprod').val(total);
	    $('#totalpro').html(Number(total).toFixed(2));
}
function deletepro(id){
		
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/deleteproducto',
	        data: {
	            idd: id
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(data){
	                	console.log(data.responseText);
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	$('.producto_'+id).remove();
	            	calculartotal();
	            }
	        });
	}
function cargamarcas(idp){
	$.ajax({
        type: 'POST',
        url: base_url+'Ventas/cargarmarcas',
        data: {id: idp},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');

            },
            500: function(data) {
            	toastr.error('Error', '500');
            	console.log(data.responseText);
            }
        },
        success: function(data) {
        	$('.panelventa2').html('');
            $('.panelventa2').html(data);
        }
    });
}
function marcasselec(idm,mname,imggs){
	marcaid=idm;
	marcaname=mname;
	imagenselect=imggs;
	$.ajax({
        type: 'POST',
        url: base_url+'Ventas/cargarpresentacion',
        data: {pro: productoid,
        		mar:idm},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(data) {
            	console.log(data.responseText);
            	toastr.error('Error', '500');
            }
        },
        success: function(data) {
        	$('.panelventa3').html('');
            //$('.panelventa3').html(data);
            $('.panelventa3').html(data);
        }
    });
}
function presentselec(id,name,precio){
	categorianame=name;
	categoriaid=id;
	$('.selectimg').html('<img src="'+imagenselect+'" width="100px">');
	$('.selectproducto').html(productoname);
	$('.selectmarca').html(marcaname);
	$('.selectpresentacion').html(categorianame);
	$('.selectcosto').html('$ '+new Intl.NumberFormat('es-MX').format(precio));
}
function addproductoss(){
	setTimeout(function(){
		$.ajax({
	        type: 'POST',
	        url: base_url+'Ventas/productoverificarstok',
	        data: {
	        		proc: categoriaid,
	        		cant: $('#cantidadcajas').val()
	        	},
	        async: false,
	        statusCode: {
	            404: function(data) {
	                toastr.error('Error!', 'No Se encuentra el archivo');
	            },
	            500: function(data) {
	            	console.log(data.responseText);
	                toastr.error('Error', data);
	            }
	        },
	        success: function(data) {
	        	var array = $.parseJSON(data);
	        	//console.log(array);
	        	if (array.estatus==0) {
	        		$('#cantidadcajas').val(array.canti);
	        		toastr.error('No hay suficiente en existencia, existencia en stock:'+array.canti ,'Error!');
	        	}else{
	        		setTimeout(function(){ addproductos(); }, 1000);
	        	}
	        }
	    });
    }, 1000);
}

function addproductos(){
	$.ajax({
        type: 'POST',
        url: base_url+'Ventas/addproducto',
        data: {
        		proc: categoriaid,
                prod: $("#id_prod_gral").val(),
        		cant: $('#cantidadcajas').val(),
        		kilos: $('#cantidadcajas').val(),
        		producton:productoname,
        		marcan:marcaname,
        		categorian:categorianame
        	},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(data) {
            	console.log(data.responseText);
                toastr.error('Error', data);
            }
        },
        success: function(data) {
            //console.log(data);
		    $('#addproductos').html(data);
		    calculartotal();
		    $('#cantidadkilos').val(0)
        }
    });
}
function pulsadaenter(){
	addproductos();
}
function buscarclientes(){
	var searchcliente=$('#selectcliente').val();
	if (searchcliente.length>=2) {
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/searchcliente',
	        data: {
	            search: searchcliente
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(data){
	                	console.log(data.responseText);
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	$('#iframeclientes').html(data);
	            }
	    });
	}
}
function btncancelar(){
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/productoclear',
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	location.reload();
	            }
	        });
}
function btncancelar2(){
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/productoclear',
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//location.reload();
	            }
	        });
}
function presskey(){
	$('.presskey').click(function() {
		buscarclientes();
	});
}
function addcliente(id,name){
	$('#clientes').html('<option value="'+id+'">'+name+'</option>');
}
function checheddescuento(){
	var descuento = $('#descuento').is(':checked')==true?1:0;
    if (descuento==1) {
        $.ajax({
            type: 'POST',
            url: base_url+'Inicio/descuentov',
            data: {des: descuento},
            async: false,
            statusCode: {
                404: function(data) {
                    notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", 'No se encuentra el archivo');
                },
                500: function(data) {
                    notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", data);
                }
            },
            success: function(data) {
              console.log(data);
              if (data==1) {
                

                if ($('#descuento').is(':checked')==true) {
                  $(".divoptiondescuento").css("display", "block");
                }else{
                  $(".divoptiondescuento").css("display", "none");
                  $("#optiondescuento").val(0);
                  calculartotal();
                }

                
                
              }else{
                $(".optiondescuento").css("display", "none");
                $("#optiondescuento").val(0);
                //$('#descuento').prop('checked', false);
              }
            }
        });
    }
}

function verificaCredito(metodo)
{
	/*if(metodo==4)
	{
		if($('#clientes').val()==45)
		{
			alert('El cliente aún no tiene una línea de crédito establecida. Capture primero este dato en el apartado de catálogos y posteriormente realice la venta.');
		}
		else
		{
			alert('El cliente cuenta con $10,000 de crédito, de los cuales actualmente tiene libres: $5,300.');
			$("#divOpcionesCredito").css("display","block");
		}


	}*/
}

function muestraInputParcial(valor)
{
	if(valor==2)
	{
		$("#montoParcial").css("display","block");
	}
}

function reiniciaMetodo()
{
	console.log('entrando');
	$('[name=metodopagos]').val(1);
}

$('#cambiaTipoVenta').click(function() 
{
	//console.log("cambio tipo de venta");
	if($("#divManual:visible").length > 0)
	{
		$('#divManual').attr('style', 'display: none');
		//$('#catalogoProds').attr('style', 'display: none');
		$('#divBarcode').attr('style', 'display: block');
	}
	else
	{
		$('#divBarcode').attr('style', 'display: none');
		$('#divManual').attr('style', 'display: block');
	}
});


function cambioPrecio(val,cont){
	//console.log('valor de precio:' +val);
	//$('#vsprecio').val(val);
	$('#vsprecio'+cont).attr('value',val);
	var total = parseFloat($('#vsprecio'+cont).val() * $('#vscanti'+cont).val())
	$('#vstotal'+cont).val(total);
	/*$('#totalpro').html(parseFloat($('#vsprecio').val() * $('#vscanti').val()));*/
	/*console.log('#vstotal'+cont +val);
	console.log('#vstotal'+cont +val);
	console.log('total'+total);*/
	calculartotal();
}