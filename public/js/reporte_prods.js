var base_url=$('#base_url').val();
$(document).ready(function() {
    $("#exportar_excel").on("click", function (){
      exportar();
    });
    loadTable();
}); 

function loadTable(){
    table = $('#table_prods').DataTable({
        destroy:true,
        "ajax": {
            url: base_url+"Productos/get_prod_rep",
            type: "post",
            error: function() {
                //$("#tabla_consumibles").css("display", "none");
            },
        },
        "columns": [
            {"data": 'productoaddId'},
            {"data": 'categoria'},
            {"data": 'marca'},
            {"data": 'stok'},
            {"data": 'stok2'},
            {"data": 'stok3'},
            {"data": 'stok4'},
            {"data": 'stok5'},
            {"data": 'stok6'},
        ],
        order: [
            [0, "asc"]
        ]
        
    });
}

function exportar(){
  window.open(base_url+"Reportes/generarReporteProds", "_blank");
}