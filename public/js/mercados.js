var base_url = $('#base_url').val();

$(document).ready(function () {
    
    var table=$('#data-tables').DataTable({
        "ajax": {
            "url": base_url+"index.php/Mercados/datatable_records"
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "direc"},
            {"data": null,
                render:function(data,type,row)
                {
                    var html = '<button type="button" class="btn btn-raised btn-outline-warning"><i class="fa fa-cog"></i></button>\
                              <button type="button" class="btn btn-raised btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
                                  <span class="sr-only">Toggle Dropdown</span>\
                              </button>\
                              <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(84px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">\
                                  <a class="dropdown-item" href="'+base_url+'index.php/Mercados/mercadosadd/'+row.id+'">Editar</a>\
                                  <a class="dropdown-item" onclick="deleteMerc('+row.id+');"href="#">Eliminar</a>\
                              </div>';
                    return html;
                }
            }
        ],
        "order": [[ 0, "desc" ]],
        "lengthMenu": [[25, 50, 100], [25, 50, 100]],
    });
  
    $('#form_mercado').validate({
        rules: {
            nombre: "required",
            direc: "required"
        },
        messages: {
            nombre: "Campo obligatorio",
            direc: "Campo obligatorio"
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: base_url+"Mercados/registrar/",
                data: $(form).serialize(),
                beforeSend: function(){
                    $("#save").attr("disabled",true);
                },
                success: function (result) {
                    //console.log(result);
                    $("#save").attr("disabled",false);
                    swal("Éxito!", "Se han realizado los cambios correctamente", "success");
                    setTimeout(function () { window.location.href = base_url+"Mercados" }, 1500);
                }
            });
            return false; // required to block normal submit for ajax
         },
        errorPlacement: function(label, element) {
          label.addClass('mt-2 text-danger');
          label.insertAfter(element);
        },
        highlight: function(element, errorClass) {
          $(element).parent().addClass('has-danger')
          $(element).addClass('form-control-danger')
        }
    });

    $('#sieliminar').click(function(event) {
        var idp =$('#hddId').val();
        $.ajax({
            type:'POST',
            url: base_url+'Mercados/delete',
            data: {id:idp},
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(){
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
                //console.log(data);
                //location.reload();
                toastr.success('Hecho!', 'eliminado Correctamente');
                table.ajax.reload();
            }
        });
    });

});

function deleteMerc(id){
  $('#hddId').val(id);
  $('#eliminacion').modal();
  
}