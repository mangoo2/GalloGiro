var base_url=$("#base_url").val();
$(document).ready(function () {
   table=$('#tablaGastos').DataTable({
      responsive: true,
      "bProcessing": true,
      "order": [[ 0, "desc" ]],
      "ajax": {
              "url": base_url+"Gastos/datatable_gastos",
              type: "post",
          },
          "columns": [
              {"data": "id"},
              {"data": "descrip"},
              {"data": null,
                  "render" : function ( url, type, full) {
                      var msj='';
                      msj+="$" +full["monto_gasto"];
                      return msj;
                  }
              },
              {"data": "fecha_gasto"},
              {
                  "data": null,
                  "defaultContent": "<button class='btn btn-danger btn-rounded btn-icon delete' type='button'><i class='fa fa-trash-o'></i></button>"
              }
          ],
      //language: languageTables
  });

  $('#tablaGastos').on('click', 'button.delete', function () {
      var tr = $(this).closest('tr');
      var data = table.row(tr).data();
      deleteG(data.id);
  });


  $('#form-insert-gasto').validate({
      rules: {
        descrip: "required",
        monto_gasto: "required",
        fecha_gasto: "required",
      },
      messages: {
          descrip: "Ingrese un concepto",
          monto_gasto: "Ingrese un monto",
          fecha_gasto: "Ingrese una fecha",
      },
      submitHandler: function (form) {
           $.ajax({
               type: "POST",
               url: base_url+"Gastos/insertarGasto",
               data: $(form).serialize(),
               beforeSend: function(){
                  $("#btn_submit").attr("disabled",true);
               },
               success: function (result) {
                  console.log(result);
                  $("#btn_submit").attr("disabled",false);
                  swal("Exito!", "Se han realizado los cambios correctamente", "success");
                  table.ajax.reload();
               }
           });
           return false; // required to block normal submit for ajax
       },
      errorPlacement: function(label, element) {
        label.addClass('mt-2 text-danger');
        label.insertAfter(element);
      },
      highlight: function(element, errorClass) {
        $(element).parent().addClass('has-danger')
        $(element).addClass('form-control-danger')
      }
  });


});


function deleteG(id) {
  $.ajax({
    url: base_url+'Gastos/delete',
    type: 'POST',
    data: {
      "id": id,
    },
    success: function(data) {
      table.ajax.reload();
      swal("Éxito!", "Se ha eliminado correctamente", "success");
      },
      error: function(jqXHR) {
        console.log(jqXHR);
      }
  });
}