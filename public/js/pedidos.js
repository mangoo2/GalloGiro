	var productoid;
	var id_prod;
	var productoname;
	var marcaid;
	var marcaname;
	var categoriaid;
	var categorianame;
	var imagenselect;
	var id_producto;
	var base_url=$('#base_url').val();
$(document).ready(function() {
	var idventa;	
	/*$('#clientes').click(function(event) {
		$('#modalclientes').modal();
		presskey();
	});*/
	//setTimeout(function(){ presskey(); }, 2000);
	$('#realizarventa').click(function(event) {
		var factura = $('#factura').is(':checked')==true?1:0;
        var TABLA   = $("#tableproductos tbody > tr");
        var aux_int = 0;
            TABLA.each(function(){
                aux_int =1;    
            });
            if(aux_int==1){    
                $.ajax({ 
                    type:'POST',
                    url: base_url+'Ventas/ingresarpedido',
                    data: {
                    	uss: $('#ssessius').val(),
                        cli: $('#clientes option:selected').val(),
                        total:$('#totalprod').val(),
                        fac:factura,
                        des:$('#optiondescuento option:selected').val(),
                        metodo: $('#metodopagos option:selected').val()
                        
                    },
                    async: false,
                    statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(data){
                        	console.log(data);
                            toastr.error('Error', '500');
                        }
                    },
                    success:function(data){
                    	$('#totalpro').val('');
                    	
                    	idventa=data;
                    	var DATA  = [];
            			var TABLA   = $("#tableproductos tbody > tr");
        				TABLA.each(function(){         
			                item = {};
			                item ["idventa"] = idventa;
                            item ["direccion"] = $("#clientedirecciones option:selected").text();
                            item ["fecha_pedido"] = $("#fecha_pedido").val();
			                item ["producto"] = $(this).find("input[id*='vsproid']").val();
			                item ["cantidad"] = $(this).find("input[id*='vscanti']").val();
			                item ["kilos"]  = $(this).find("input[id*='vskilos']").val();
			                item ["precio"] = $(this).find("input[id*='vsprecio']").val();
			                DATA.push(item);
			            });
        				INFO  = new FormData();
        				aInfo   = JSON.stringify(DATA);
        				INFO.append('data', aInfo);
			            $.ajax({
			                data: INFO,
			                type: 'POST',
			                url : base_url+'Ventas/ingresarventapro/'+2,
			                processData: false, 
			                contentType: false,
			                async: false,
		                    statusCode:{
		                        404: function(data){
		                            toastr.error('Error!', 'No Se encuentra el archivo');
		                        },
		                        500: function(){
		                        	console.log(data.responseText);
		                            toastr.error('Error', '500');
		                        }
		                    },
			                success: function(data){
                                $("#addproductos").empty();
                                $('#totalprod').val(0.00);
                                $('#cantidadcajas').val(0);
			                }
			            });
			            var ticket = $('#ticketventa').is(':checked')==true?1:0;
			            var factura = $('#factura').is(':checked')==true?1:0;
			            if (ticket==1) {
			            	$("#modalimpresion").modal();
							$('#iframeticket').html('<iframe src="Ticket?id='+idventa+'&tipo=2"></iframe>');
			            }else{
			            	toastr.success( 'Venta Realizada','Hecho!');
			            	setTimeout(function(){ btncancelar(); }, 2000);
			            }
			            if (factura==1) {
			            	$("#modaldatosfactura").modal();
			            }
			            btncancelar2();
                    }
                });
            }else{
               toastr.error('Error!', 'No has agregado ningún producto');   
            }   
	});
	/*$('#selectcliente').mlKeyboard({
          layout: 'es_ES'
        });*/
	$('#nombredelturno').mlKeyboard({
          layout: 'es_ES'
        });
	$('#cantidadt').mlKeyboard({
          layout: 'es_ES'
        });
	$('#btnabrirt').click(function(){
				$.ajax({
					type:'POST',
					url:base_url+'Ventas/abrirturno',
					data:{
						cantidad: $('#cantidadt').val(),
						nombre: $('#nombredelturno').val(),
						bodega: $('#bodegauser').val(),
					},
					async:false,
					statusCode:{
                        404: function(data){
                            toastr.error('Error!', 'No Se encuentra el archivo');
                        },
                        500: function(data){
                        	console.log(data);
                            toastr.error('Error', '500');
                        }
                    },
					success:function(data){
						toastr.success('Turno Abierto','Hecho!');
						$('#modalturno').modal('hide');
						
					}
				});		
	});

	$('.classproducto').addClass('zoomIn animated');
	$('#secosto').numpad();
    $('#cantidadcajas').numpad();
    $('#cantidadkilos').numpad();
    //$('#cantidadt').numpad();
	/*$('.panelventa1').click(function(){
		$('.classproducto').removeClass('zoomIn animated');
        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
        $('.classproductosub').addClass('zoomIn animated');
    });
    $('.panelventa2').click(function(){
    	$('.classproductosub').removeClass('zoomIn animated');
        $(".panelventa2").css("display","none");
        $(".panelventa4").css("display","block");
        $('.classproductotipo').addClass('zoomIn animated');
    });*/
    /*$('.panelventa3').click(function(){
    	$('.classproductotipo').removeClass('zoomIn animated');
        $(".panelventa3").css("display","none");
        $(".panelventa4").css("display","block");
        $('input[name=cantidadcajas]').focus(); 
        $('.classsventa').addClass('zoomIn animated');
    });
    $('.masproductos').click(function(event) {
    	$('.panelventa2').html('');
    	$('.panelventa3').html('');
    	$('.classproducto').addClass('zoomIn animated');
    	$(".panelventa4").css("display","none");
    	$(".panelventa1").css("display","block");
    });*/ 

    $('.panelventa1').click(function(){
		$('.classproducto').removeClass('zoomIn animated');
        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
        $('.classproductosub').addClass('zoomIn animated');
    });
    $('.panelventa2').click(function(){
    	$('.classproductosub').removeClass('zoomIn animated');
        $(".panelventa2").css("display","none");
        $(".panelventa3").css("display","block");
        $('.classproductotipo').addClass('zoomIn animated');
    });
    $('.panelventa3').click(function(){
    	$('.classproductotipo').removeClass('zoomIn animated');

        $(".panelventa3").css("display","none");
        $(".panelventa4").css("display","block");
        $('input[name=cantidadcajas]').focus(); 
        $('.classsventa').addClass('zoomIn animated');
    });
    $('#cambiaTipoVenta').click(function(){
    	//console.log("cambio tipo de venta");
    	$('.classproductotipo').removeClass('zoomIn animated');

        $(".panelventa1").css("display","none");
        $(".panelventa2").css("display","block");
        $('.classsventa').addClass('zoomIn animated');
    });
    $('.masproductos').click(function(event) {
    	$('.panelventa2').html('');
    	$('.panelventa3').html('');
    	$('.classproducto').addClass('zoomIn animated');
    	$(".panelventa4").css("display","none");
    	$(".panelventa1").css("display","block");
    });
    $(".classproducto").click(function(event) {
		productoid= $(this).attr('data-idproducto');
    	productoname= $(this).attr('data-cate');
        //console.log("productoid: "+productoid);
        //console.log("productoname: "+productoname);
    	cargamarcas(productoid);
        //$('#idproducto').html(productoid);
    	if (productoid!=1) {
    		$("#cantidadkilos").css("display","none");
    		$("#cantidadkilos").val(0);
    	}else{
    		$("#cantidadkilos").css("display","block");
    	}
    });

    $("#clientes").on("change",function(){
        cambia_direcc();

    });
    if($("#clientes").val()!=""){
        cambia_direcc();
    }

    //$(".panelventa1").css("display","none");
    //$(".panelventa4").css("display","block");
    $('#savedatosfactura').click(function() {
    	$.ajax({ 
            type:'POST',
            url: base_url+'Ventas/datosfactura',
            data: {
            	idventa: idventa,
                datos: $('#datosfactura').val()
                
            },
            async: false,
            statusCode:{
                404: function(data){
                    toastr.error('Error!', 'No Se encuentra el archivo');
                },
                500: function(data){
                	console.log(data);
                    toastr.error('Error', '500');
                }
            },
            success:function(data){
            	toastr.success( 'Datos Fiscales','Guardados!');
            }
        });
    });

    $('#clientes').select2({
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Cliente',
        ajax: {
            url: base_url+'Pedidos/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.ClientesId,
                        text: element.Nom,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        reiniciaMetodo();
    }); 


    /*$("#vsprecio").on("change",function(){
        total = $("#vscanti").val() * $("#vsprecio").val();
        $("#vstotal").val(total);
    });*/

});

$.fn.numpad.defaults.gridTpl = '<table class="table modal-content tablenumber"></table>';
$.fn.numpad.defaults.backgroundTpl = '<div class="modal-backdrop in"></div>';
$.fn.numpad.defaults.displayTpl = '<input type="text" class="form-control" />';
$.fn.numpad.defaults.buttonNumberTpl =  '<button type="button" class="btn vd_btn vd_bg-yellow numbertouch"></button>';
//$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn numbertouch2" style="width: 100%;" onclick="addproductoss()"></button>';
$.fn.numpad.defaults.buttonFunctionTpl = '<button type="button" class="btn numbertouch2" style="width: 100%;"></button>';
$.fn.numpad.defaults.onKeypadCreate = function(){$(this).find('.done').addClass('btn-primary');};

function recalcula(id){
    total = $(".cant_"+id+"").val() * $(".prec_"+id+"").val();
    //console.log("total: "+total);
    $(".vstotal_"+id+"").val(total);
    calculartotal();
}

function calculartotal(){
		var addtp = 0;
		$(".vstotal").each(function() {
	        var vstotal = $(this).val();
	        addtp += Number(vstotal);
	    });
	    var descuento0=addtp*$('#optiondescuento option:selected').val();
	    var total=parseFloat(addtp)-parseFloat(descuento0);
	    $('#totalprod').val(total);
	    $('#totalpro').html(Number(total).toFixed(2));
}
function deletepro(id){
		
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/deleteproducto',
	        data: {
	            idd: id
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(data){
	                	console.log(data.responseText);
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	$('.producto_'+id).remove();
	            	calculartotal();
	            }
	        });
	}
function cargamarcas(idp){
    //console.log("carga marcas, idp: "+idp);
	$.ajax({
        type: 'POST',
        url: base_url+'Pedidos/cargarmarcas',
        data: {id: idp},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');

            },
            500: function(data) {
            	toastr.error('Error', '500');
            	console.log(data.responseText);
            }
        },
        success: function(data) {
        	//console.log(data);
        	$('.panelventa2').html('');
            $('.panelventa2').html(data);
            $('#idproducto').html(idp);
        }
    });
}
function marcasselec(idm,mname,imggs){
	marcaid=idm;
	marcaname=mname;
	imagenselect=imggs;
	$.ajax({
        type: 'POST',
        url: base_url+'Ventas/cargarpresentacion',
        data: {pro: productoid,
        		mar:idm},
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(data) {
            	console.log(data.responseText);
            	toastr.error('Error', '500');
            }
        },
        success: function(data) {
        	$('.panelventa3').html('');
            $('.panelventa3').html(data);
        }
    });
}
function presentselec(id_prod, marca,precio){
	/*categoriaid=id;*/
	id_producto=id_prod;
    //console.log("id prod desde presentselec: "+id_prod);
	$('.selectimg').html('<img src="'+imagenselect+'" width="100px">');
	//$('#idproducto').html(id_prod);
	$('#selectproducto').html(productoname);
	$('#selectmarca').html(marca);
	$('#selectcosto').html('$ '+new Intl.NumberFormat('es-MX').format(precio));

	//Datos para hacer calculo;
	$("#precioadi").val(precio);
	nombreProductoTabla = productoname;
	marcaTabla = marca;
	precioTabla = precio;
}
function addproductos(){
	var totalTablaFinal = $("#cantidadcajas").val() * $("#precioadi").val();
	cantidadTabla = $("#cantidadcajas").val();
	
    var id_producto = $("#idproducto").text();
    //console.log("prod en cabecera: "+id_producto);
    var repetido=0;
    var TABLA   = $("#tableproductos tbody > tr");
    TABLA.each(function(){         
        var prode   = $(this).find("input[id*='vsproid']").val();
        //console.log("prod en tabla lista: " +prode);
        if (prode==$("#idproducto").text()) {
            repetido=1;
        }
        
    });
	if(repetido==0){
		$("#addproductos").prepend(`
			<tr class="text-left" id="tr_prod_${id_producto}">
				<td><input type="hidden" name="vsproid" id="vsproid" value="${id_producto}" readonly style="background: transparent;border: 0px; width: 10px;"></td>
				<td width="40%"><input type="text" name="nombre" id="nombre" value="${nombreProductoTabla}" readonly style="background: transparent;border: 0px;"></td>
				<td width="20%"><input type="text" name="marca" id="marca" value="${marcaTabla}" readonly style="background: transparent;border: 0px; width: 60px;"></td>
				<td width="10%"><input class="cant_${id_producto}" type="number" name="vscanti" id="vscanti" value="${cantidadTabla}" readonly style="background: transparent;border: 0px; width: 40px;"></td>
				<td width="10%"><input class="prec_${id_producto}" type="number" oninput="recalcula(${id_producto})" name="vsprecio" id="vsprecio" value="${precioTabla}" style="background: transparent;border: 0px; width: 60px;"></td>
				<td width="10%"><input type="number" class="vstotal vstotal_${id_producto}" name="vstotal" id="vstotal" value="${totalTablaFinal}" readonly style="background: transparent;border: 0px; width: 60px;"></td>
				<td><input type="hidden" name="vskilos" id="vskilos" value="0" readonly style="background: transparent;border: 0px; width: 10px;"></td>
				<td width="10%"><i class="fa fa-trash-o" style="color: red:" onclick="deletepros(${id_producto})"></i></td>
			</tr>
		`);
        calculartotal();
	}else{
		var canttot = parseFloat($('.cant_'+id_producto+'').val()) + parseFloat($('#cantidadcajas').val());
        //var canttot = parseFloat($('.cant_'+id_producto+'').val());
        //console.log("canttot: "+canttot);
		$('.cant_'+id_producto+'').attr('value',canttot);
		$('.vstotal_'+id_producto+'').val(parseFloat($('.prec_'+id_producto+'').val() * canttot));
	}
	calculartotal();
	/*$.ajax({
        type: 'POST',
        url: 'Pedidos/addproducto',
        data: {
                proc: categoriaid,
                cant: $('#cantidadcajas').val(),
                kilos: $('#cantidadkilos').val(),
                producton:productoname,
                marcan:marcaname,
                categorian:categorianame
            },
        async: false,
        statusCode: {
            404: function(data) {
                toastr.error('Error!', 'No Se encuentra el archivo');
            },
            500: function(data) {
                console.log(data.responseText);
                toastr.error('Error', data);
            }
        },
        success: function(data) {
            console.log(data);
            $('#addproductos').html(data);
            calculartotal();
            $('#cantidadkilos').val(0)
        }
    });*/
}
function addproductoss(){
	setTimeout(function(){
		$.ajax({
	        type: 'POST',
	        url: base_url+'Ventas/productoverificarstok',
	        data: {
	        		proc: categoriaid,
	        		cant: $('#cantidadcajas').val()
	        	},
	        async: false,
	        statusCode: {
	            404: function(data) {
	                toastr.error('Error!', 'No Se encuentra el archivo');
	            },
	            500: function(data) {
	            	console.log(data.responseText);
	                toastr.error('Error', data);
	            }
	        },
	        success: function(data) {
	        	var array = $.parseJSON(data);
	        	//console.log(array);
	        	if (array.estatus==0) {
	        		$('#cantidadcajas').val(array.canti);
	        		toastr.error('No hay suficiente en existencia, existencia en stock:'+array.canti ,'Error!');
	        	}else{
	        		setTimeout(function(){ addproductos(); }, 1000);
	        	}
	        }
	    });
    }, 1000);
}

function pulsadaenter(){
	addproductos();
}
function buscarclientes(){
	var searchcliente=$('#selectcliente').val();
	if (searchcliente.length>3) {
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/searchcliente',
	        data: {
	            search: searchcliente
	            },
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(data){
	                	console.log(data.responseText);
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	$('#iframeclientes').html(data);
	            }
	        });
	}
}
function btncancelar(){
	//id = $(this).attr("id");
    id = $('#prodelete').val();
    //console.log("id: "+id);
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/productoclear',
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//location.reload();
	            	//$('#modalconfirm').modal('hide');
	            	//var t = $('#tableproductos').DataTable();
	            	//$(this).parents('tr').remove().draw(false);
	            	/*let $tr = $(this).closest('tr');
					// Le pedimos al DataTable que borre la fila
					t.row($tr).remove().draw(false);
					$($tr).remove();*/
					//t.row(this).remove().draw();
                    $('#tr_prod_'+id).remove();
	            }
	        });
}
function btncancelar2(){
		$.ajax({
	        type:'POST',
	        url: base_url+'Ventas/productoclear',
	            async: false,
	            statusCode:{
	                404: function(data){
	                    toastr.error('Error!', 'No Se encuentra el archivo');
	                },
	                500: function(){
	                    toastr.error('Error', '500');
	                }
	            },
	            success:function(data){
	            	//location.reload();
	            }
	        });
}
/*function presskey(){
	$('.presskey').click(function() {
		buscarclientes();
	});
}*/
function addcliente(id,name){
	$('#clientes').html('<option value="'+id+'">'+name+'</option>');
}
function checheddescuento(){
	var descuento = $('#descuento').is(':checked')==true?1:0;
    if (descuento==1) {
        $.ajax({
            type: 'POST',
            url: base_url+'Inicio/descuentov',
            data: {des: descuento},
            async: false,
            statusCode: {
                404: function(data) {
                    notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", 'No se encuentra el archivo');
                },
                500: function(data) {
                    notification("topright", "error", "fa fa-exclamation-triangle vd_yellow", "Excepción!", data);
                }
            },
            success: function(data) {
              console.log(data);
              if (data==1) {
                

                if ($('#descuento').is(':checked')==true) {
                  $(".divoptiondescuento").css("display", "block");
                }else{
                  $(".divoptiondescuento").css("display", "none");
                  $("#optiondescuento").val(0);
                  calculartotal();
                }

                
                
              }else{
                $(".optiondescuento").css("display", "none");
                $("#optiondescuento").val(0);
                //$('#descuento').prop('checked', false);
              }
            }
        });
    }
}

function verificaCredito(metodo) //ESTA FUNCION NO SIRVE, SE DEBE IMPLEMENTAR DESDE 0
{
	if(metodo==4)
	{
		/*if($('#clientes').val()==45)
		{
			alert('El cliente aún no tiene una línea de crédito establecida. Capture primero este dato en el apartado de catálogos y posteriormente realice la venta.');
		}
		else
		{
			alert('El cliente cuenta con $10,000 de crédito, de los cuales actualmente tiene libres: $5,300.');
			$("#divOpcionesCredito").css("display","block");
		}*/
        //se verificará monto de limite por cliente? preguntar e implementar de ser el caso!
	}
}

function muestraInputParcial(valor)
{
	if(valor==2)
	{
		$("#montoParcial").css("display","block");
	}
}
$('#cambiaTipoVenta').click(function() 
{
	//console.log("cambio tipo de venta");
	if($("#divManual:visible").length > 0)
	{
		$('#divManual').attr('style', 'display: none');
		//$('#catalogoProds').attr('style', 'display: none');
		$('#divBarcode').attr('style', 'display: block');
	}
	else
	{
		$('#divBarcode').attr('style', 'display: none');
		$('#divManual').attr('style', 'display: block');
	}
});
function reiniciaMetodo()
{
	console.log('entrando');
	$('[name=metodopagos]').val(4);
}

function deletepros(id){
    $('#modalconfirm').modal();
    $('#prodelete').val(id);
}

function cambia_direcc(){
    $.ajax({
        type: "POST",
        url: base_url+"index.php/Pedidos/searchContacto",
        data: {cliente: $("#clientes").val()},
        //async: false,
        //dataType: "JSON",
        success: function (data) {
            $("#clientedirecciones").empty();
            $("#clientedirecciones").append(data);
        }
    });
}


