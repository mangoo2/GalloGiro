var base_url=$('#base_url').val();
$(document).ready(function() {
    $('#cliente').select2({
        width: 'resolve',
        minimumInputLength: 2,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un Cliente',
        ajax: {
            url: base_url+'Reportes/searchclientes',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.ClientesId,
                        text: element.Nom,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        loadTable();
    }); 

    $('#id_mercado').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar un mercado',
        ajax: {
            url: base_url+'Mercados/searchMercado',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        loadTable();
    });

    $("#txtInicio").on("change", function(){
        if($('#todos_cli').is(':checked') == false && $("#txtInicio").val()!="" && $("#txtFin").val()!="" && $("#cliente").val()!=null){
            loadTable();
        }
    });
    $("#txtFin").on("change", function(){
        if($('#todos_cli').is(':checked') == false && $("#txtInicio").val()!="" && $("#txtFin").val()!="" && $("#cliente").val()!=null){
            loadTable();
        }
    });
    $("#txtInicio").on("change", function(){
        if($('#todos_cli').is(':checked') == true && $("#txtInicio").val()!="" && $("#txtFin").val()!=""){
            loadTable();
        }
    });
    $("#txtFin").on("change", function(){
        if($('#todos_cli').is(':checked') == true && $("#txtInicio").val()!="" && $("#txtFin").val()!=""){
            loadTable();
        }
    });
    $("#exportar_excel").on("click", function (){
      exportar();
    });
    $("#todos_cli").on("click",function(){
        if($('#todos_cli').is(':checked') == true && $("#txtInicio").val()!="" && $("#txtFin").val()!=""){
            loadTable();
        }
    });
}); 

function loadTable(){
    //var cliente=$('#cliente option:selected').val();
    var cliente = $('#todos_cli').is(':checked') == true ? 0 : $('#cliente option:selected').val();
    var fechai=$('#txtInicio').val();
    var fechaf=$('#txtFin').val();
    var idm=$('#id_mercado option:selected').val();

    table = $('#table_edos').DataTable({
        destroy:true,
        "ajax": {
            url: base_url+"Reportes/reporteEstadoCuenta",
            type: "post",
            "data": {
                "cliente": cliente,
                "fechai": fechai,
                "fechaf": fechaf,
                "mercado": idm
            },
            error: function() {
                //$("#tabla_consumibles").css("display", "none");
            }
        },
        "columns": [
            {"data": 'id_venta'},
            {"data": 'reg'},
            {"data": 'Nom'},
            //{"data": 'monto_total'},
            /*{"data": null,
                "render": function (data,type,row) {
                    //var monto_total=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto_total);  
                    //return monto_total;
                    var suma=sumaVenta(row.id_venta);
                    console.log("suma: "+suma);
                    return suma;
                }
            },*/
            //{"data": 'cantidad'},
            {"data": null,
                "render": function (data,type,row) {
                    var total_pagos=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.total_pagos);  
                    return total_pagos;
                }
            },
            /*{"data": null,
                "render": function (data,type,row) {
                    var rw=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.monto_total-row.cantidad);  
                    return rw;
                }
            },*/
        ],
        order: [
            [0, "desc"]
        ]
        
    });
}

function sumaVenta(id){
    //var cliente=$('#cliente option:selected').val();
    var cliente = $('#todos_cli').is(':checked') == true ? 0 : $('#cliente option:selected').val();
    var fechai=$('#txtInicio').val();
    var fechaf=$('#txtFin').val();
    var resultado; 
    $.ajax({ 
        type:'POST', 
        url: base_url+'Reportes/reporteSumaVenta', 
        "data": {
            "cliente": cliente,
            "fechai": fechai,
            "fechaf": fechaf,
            "id": id
        },
        success:function(data){ 
            resultado=data; 
            //console.log("resultado: "+resultado);
            return resultado; 
        } 
    }); 
}

function exportar(){
  //var cli=$('#cliente option:selected').val();
  var cli = $('#todos_cli').is(':checked') == true ? 0 : $('#cliente option:selected').val();
  var fi=$("#txtInicio").val();
  var ff=$("#txtFin").val();
  window.open(base_url+"Reportes/generarReporteEdos/"+cli+"/"+fi+"/"+ff+"", "_blank");
}