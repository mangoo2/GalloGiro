var table;
var base_url = $('#base_url').val();
$(document).ready(function () {

  $('#id_cliente').select2({
    width: '95%',
    minimumInputLength: 2,
    minimumResultsForSearch: 10,
    placeholder: 'Buscar un Cliente',
    ajax: {
      url: base_url+'Reportes/searchclientes',
      dataType: "json",
      data: function(params) {
        var query = {
          search: params.term,
          type: 'public'
        }
        return query;
      },
      processResults: function(data) {
        //var productos=data;
        var itemscli = [];
        //console.log(data);
        data.forEach(function(element) {
            itemscli.push({
                id: element.ClientesId,
                text: element.Nom,
            });
        });
        return {
            results: itemscli
        };
      },
    }
  });

  $("#fechai, #fechaf, #id_sucursal").on("change",function(){
    if($("#fechai").val()!="" && $("#fechaf").val()!="")
      load();
  });
  $("#exportar_excel").on("click",function(){
    if($("#fechai").val()!="" && $("#fechaf").val()!="")
      window.open(base_url+"Reportes/exportVentaProds/"+$("#fechai").val()+"/"+$("#fechaf").val()+"/"+$("#id_sucursal option:selected").val(), '_blank');
  });

  $("#fechaip, #fechafp, #id_sucursal_pp, #id_cliente").on("change",function(){
    if($("#fechaip").val()!="" && $("#fechafp").val()!="" && $("#id_cliente option:selected").val()!=undefined)
      loadPeds();
  });
  $("#exportar_excel_pp").on("click",function(){
    if($("#fechaip").val()!="" && $("#fechafp").val()!="" && $("#id_cliente option:selected").val()!=undefined)
      window.open(base_url+"Reportes/exportPedidoProds/"+$("#fechaip").val()+"/"+$("#fechafp").val()+"/"+$("#id_sucursal_pp option:selected").val()+"/"+$("#id_cliente option:selected").val(), '_blank');
  });

});

function load() {
  table=$('#table_prodsv').DataTable({
    "bProcessing": true,
    "serverSide": true,
    destroy: true,
    "order": [[ 0, "desc" ]],
    "ajax": {
      "url": base_url+"Reportes/datatable_reporteprodsv",
      type: "post",
      data: { fechai:$("#fechai").val(), fechaf:$("#fechaf").val(), id_sucursal:$("#id_sucursal option:selected").val() },
    },
    "columns": [
      //{"data": "id_venta"},
      {"data": null,
        "render" : function ( url, type, row) {
            var msj='';
            msj+=row.categoria+" "+row.marca;
            return msj;
        }
      },
      //{"data": "cantidad"},
      {"data": "totalc",
        "render" : function ( url, type, row) {
            var msj='';
            msj=parseFloat(row.totalc).toFixed(2);
            return msj;
        }
      },
      {"data": "precio",
        "render" : function ( url, type, row) {
            var msj='';
            msj+=Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.precio);
            return msj;
        }
      },
      {"data": "total_prods",
        "render" : function ( url, type, row) {
            var msj='';
            msj+=Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.total_prods);
            return msj;
        }
      },
      //{"data": "metodo"}
      /*{"data": null,
        "render" : function ( url, type, row) {
          var msj='';
          if(row.metodo=="1")
            msj="Efectivo";
          else if(row.metodo=="2")
            msj="Tarjeta de Crédito";
          else if(row.metodo=="3")
            msj="Tarjeta de Dédito";
          else if(row.metodo=="4")
            msj="A crédito";

          return msj;
        }
      },*/
      /*{"data": null,
        "render" : function ( url, type, row) {
            var msj='';
            if(row.bodega=="1")
              msj="Matriz";
            else if(row.bodega=="2")
              msj="La Fragua";
            else if(row.bodega=="3")
              msj="16 de Sep.";
            else if(row.bodega=="4")
              msj="Acocota";
            else if(row.bodega=="3")
              msj="Morillotla";
            else if(row.bodega=="4")
              msj="Sonata";

            return msj;
        }
      },*/

    ],
    footerCallback: function (row, data, start, end, display) {
      var api = this.api();

      // Remove the formatting to get integer data for summation
      var intVal = function (i) {
          return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
      };

      // Total over all pages
      totalcat = api
        .column(3)
        .data()
        .reduce(function (a, b) {
            return intVal(a) + intVal(b);
        }, 0);

      // Total over this page
      pageTotalp = api
        .column(3, { page: 'current' })
        .data()
        .reduce(function (a, b) {
            return intVal(a) + intVal(b);
        }, 0);


      // Total over all pages
      /*totalprod = api
        .column(2)
        .data()
        .reduce(function (c, d) {
            return intVal(c) + intVal(d);
        }, 0);

      // Total over this page
      pageTotalp = api
        .column(2, { page: 'current' })
        .data()
        .reduce(function (c, d) {
            return intVal(c) + intVal(d);
        }, 0);*/

      // Update footer
      //total_fin = totalcat*totalprod;
      totalcat = parseFloat(totalcat).toFixed(2);
      $(api.column(3).footer()).html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalcat));

    },
    //language: languageTables
  });
}

function loadPeds() {
  table=$('#table_prod_ped').DataTable({
    "bProcessing": true,
    "serverSide": true,
    destroy: true,
    "order": [[ 0, "desc" ]],
    "ajax": {
      "url": base_url+"Reportes/datatable_reporteprodsPed",
      type: "post",
      data: { fechai:$("#fechaip").val(), fechaf:$("#fechafp").val(),id_sucursal:$("#id_sucursal_pp option:selected").val(),id_cliente:$("#id_cliente option:selected").val() },
    },
    "columns": [
      //{"data": "id_venta"},
      {"data": null,
        "render" : function ( url, type, row) {
            var msj='';
            msj+=row.categoria+" "+row.marca;
            return msj;
        }
      },
      //{"data": "cantidad"},
      {"data": "totalc",
        "render" : function ( url, type, row) {
            var msj='';
            msj=parseFloat(row.totalc).toFixed(2);
            return msj;
        }
      },
      {"data": "precio",
        "render" : function ( url, type, row) {
            var msj='';
            msj+=Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.precio);
            return msj;
        }
      },
      {"data": "total_prods",
        "render" : function ( url, type, row) {
            var msj='';
            msj+=Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.total_prods);
            return msj;
        }
      },
      {"data": "Nom"},
    ],
    footerCallback: function (row, data, start, end, display) {
      var api = this.api();

      // Remove the formatting to get integer data for summation
      var intVal = function (i) {
          return typeof i === 'string' ? i.replace(/[\$,]/g, '') * 1 : typeof i === 'number' ? i : 0;
      };

      // Total over all pages
      totalcat = api
        .column(3)
        .data()
        .reduce(function (a, b) {
            return intVal(a) + intVal(b);
        }, 0);

      // Total over this page
      pageTotalp = api
        .column(3, { page: 'current' })
        .data()
        .reduce(function (a, b) {
            return intVal(a) + intVal(b);
        }, 0);

      totalcat = parseFloat(totalcat).toFixed(2);
      $(api.column(3).footer()).html(Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(totalcat));

    },
    //language: languageTables
  });
}